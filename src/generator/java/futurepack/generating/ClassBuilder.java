package futurepack.generating;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

public class ClassBuilder
{
	final String packageName;
	final String name;
	
	Set<String> imports;
	Collection<Consumer<Writer>> methods;
	
	public ClassBuilder(String packageName, String name)
	{
		this.packageName = packageName;
		this.name = name;
		imports = new HashSet<>();
		methods = new ArrayList<>();
	}
	
	public void build(Writer writer) throws IOException
	{
		writer.write("package ");
		writer.write(packageName);
		writer.write(";\r\n\r\n");
		
		for(String s : imports.parallelStream().sorted().toList())
		{
			writer.append("import ").append(s).append(";\r\n");
		}
		writer.write("\npublic class " + name +"\r\n");
		writer.write("{\r\n");
		
		genBody(writer);
		
		writer.write("}\r\n");
	}
	
	private void genBody(Writer writer) throws IOException
	{
		methods.forEach(c -> c.accept(writer));
	}
	
	public File getFile(File base)
	{
		String path = packageName.replace(".", File.separator);
		File f =  new File(base, path + File.separator + name + ".java");
		System.out.println(f);
		return f;
	}
}