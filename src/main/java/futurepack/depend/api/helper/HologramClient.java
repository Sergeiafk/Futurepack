package futurepack.depend.api.helper;

import futurepack.api.Constants;
import net.minecraft.client.Minecraft;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.item.Item;
import net.minecraftforge.registries.ForgeRegistries;

//@ TODO: OnlyIn(Dist.CLIENT)
public class HologramClient
{
	private static final Item holo = ForgeRegistries.ITEMS.getValue(new ResourceLocation(Constants.MOD_ID, "hologram_controler"));
	
	//TODO: TileEntity einbauen, sodass wirklich jeder Block gerendert werden kann.
	
//	protected static void renderHologram(BlockEntity t)
//	{
//		boolean debug = isHologramDebug();
//		ITileHologramAble holo = (ITileHologramAble) t;
//		
//		t.getLevel().getProfiler().push("renderHologramOld");
//		
//		if(debug)
//		{
//			GlStateManager._enableBlend();
//			GL11.glBlendFunc(GL11.GL_ONE_MINUS_DST_COLOR, GL11.GL_ONE);
//			GlStateManager._disableCull();
//		}
//		else
//		{
//			GlStateManager._blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
//		}
//		
//		//FIXME HelperRenderBlocks.renderBlockSlow(holo.getHologram(), t.getPos(), t.getWorld());
//		
//		if(debug)
//		{
//			GlStateManager._disableBlend();
//			GlStateManager._enableCull();
//		}
//		
//		t.getLevel().getProfiler().pop();
//	}
	
//	private static Map<BlockEntity, CashedModel> cashedModels = new WeakHashMap<BlockEntity, CashedModel>();
//	private static CashedModelInvalidator invalidator;
	
//	protected static void renderHologramFAST(BlockEntity tile, double x, double y, double z, BufferBuilder buf)
//	{
//		tile.getLevel().getProfiler().push("renderHologramTESR");
//		if(invalidator == null)
//		{
//			invalidator = new CashedModelInvalidator(tile.getLevel(), cashedModels);
//		}
//		else if(tile.getLevel() != invalidator.getWorld())
//		{
//			invalidator.clear();
//			invalidator = new CashedModelInvalidator(tile.getLevel(), cashedModels);
//		}		
//		ITileHologramAble holo = (ITileHologramAble) tile;
//		if(holo.hasHologram())
//		{
//			tile.getLevel().getProfiler().push("setupTempWorld");
//			CashedModel model = cashedModels.get(tile);
//			BlockPos pos = tile.getBlockPos();	
//			if(model==null)
//			{
//				model = new CashedModel(pos, holo.getHologram(), tile.getLevel()); //create model if absend
//				cashedModels.put(tile, model);
//			}
//			else if(model.getBlockState() != holo.getHologram())
//			{
//				model.clear();
//				model = new CashedModel(pos, holo.getHologram(), tile.getLevel()); // refresh after changes
//				cashedModels.put(tile, model);
//			}
//			
//			tile.getLevel().getProfiler().popPush("rendering");
//			model.build((float)x, (float)y, (float)z, buf);
//			tile.getLevel().getProfiler().pop();
//		}
//		tile.getLevel().getProfiler().pop();
//	}
	
	
	
	//GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE_MINUS_DST_ALPHA); sirgt f�r blend nur f�r hintergrund
	//GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE_MINUS_DST_COLOR); invertierung
	//GL11.glBlendFunc(GL11.GL_ONE_MINUS_DST_ALPHA, GL11.GL_ONE_MINUS_SRC_COLOR); invertierung + hintergrund ist dr�ber
	//GL11.glBlendFunc(GL11.GL_ONE_MINUS_DST_COLOR, GL11.GL_ONE_MINUS_SRC_COLOR); invertriung nur im fordergrund + bei schwarz normal
	//GL11.glBlendFunc(GL11.GL_ONE_MINUS_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_COLOR); invertriung nur im fordergrund
	//GL11.glBlendFunc(GL11.GL_ONE_MINUS_SRC_ALPHA, GL11.GL_ONE_MINUS_DST_ALPHA); hintergrund + texture ist schwarz
	//GL11.glBlendFunc(GL11.GL_ONE_MINUS_SRC_ALPHA, GL11.GL_ONE_MINUS_DST_COLOR); invertriert, aber dunkler
	protected static boolean isHologramDebug()
	{
		@SuppressWarnings("resource")
		LocalPlayer sp = Minecraft.getInstance().player;
		if(sp==null)
		{
			return false;
		}
		
		if(sp.getItemInHand(InteractionHand.MAIN_HAND) != null)
		{
			if(sp.getItemInHand(InteractionHand.MAIN_HAND).getItem() == holo)
				return true;
		}
		if(sp.getItemInHand(InteractionHand.OFF_HAND) != null)
		{
			if(sp.getItemInHand(InteractionHand.OFF_HAND).getItem() == holo)
				return true;
		}
		return false;
	}
	
//	protected static boolean hasFastRenderer(ITileHologramAble holo)
//	{
//		return holo.hasHologram() && !isHologramDebug();
//	}
}
