package futurepack.depend.api.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.pathfinder.Path;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.ITeleporter;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class HelperEntities
{
	private static HelperEntities Instance = new HelperEntities();

	private boolean itemSpawn = true;

	public HelperEntities()
	{
		MinecraftForge.EVENT_BUS.register(this);
	}

	@SubscribeEvent
	public void onEntitySpawn(EntityJoinWorldEvent event)
	{
		if(!itemSpawn)
		{
			if(event.getEntity() instanceof ItemEntity)
			{
				event.setCanceled(true);
			}
		}
		if(remover.size()>0)
		{
			if(event.getEntity() instanceof ItemEntity item)
			{
				if(remover.stream().filter(p -> p.test(item)).findAny().isPresent())
				{
					event.setCanceled(true);
				}
			}

		}

	}

	public static void disableItemSpawn()
	{
		Instance.itemSpawn = false;
	}

	public static void enableItemSpawn()
	{
		Instance.itemSpawn = true;
	}

	private static final List<Predicate<ItemEntity>> remover = Collections.synchronizedList(new ArrayList<>(5));

	public static void addItemRemover(Predicate<ItemEntity> stopDrops)
	{
		remover.add(stopDrops);
	}

	public static void removeItemRemover(Predicate<ItemEntity> stopDrops)
	{
		remover.remove(stopDrops);
	}

	public static Entity transportToDimension(Entity e, ServerLevel server)
	{
		return e.changeDimension(server, new ITeleporter()
		{
			@Override
			public Entity placeEntity(Entity porting, ServerLevel currentWorld, ServerLevel destWorld, float yaw, Function<Boolean, Entity> repositionEntity)
			{
				porting.level.getProfiler().popPush("reloading");
				Entity entity = porting.getType().create(server);
				if (entity != null)
				{
					entity.restoreFrom(porting);
					entity.moveTo(porting.getX(), porting.getY(), porting.getZ(), porting.getYRot(), porting.getXRot());
					entity.setDeltaMovement(porting.getDeltaMovement());
					server.addDuringTeleport(entity);
				}
				return entity;
			}
		});
	}

	public static ServerPlayer transportPlayerToDimnsion(ServerPlayer pl, ServerLevel w)
	{
		pl.teleportTo(w, pl.getX(), pl.getY(), pl.getZ(), pl.getViewYRot(0), pl.getViewXRot(0));
		return pl;
	}

	public static boolean navigateEntity(Mob c, BlockPos target, boolean ignorePath)
	{
		if(ignorePath || c.getNavigation().isDone())
		{
			Level world = c.getCommandSenderWorld();
			Path p = c.getNavigation().createPath(target, 1);
			if(p!=null)
			{
				c.getNavigation().moveTo(p, 1.0F);
				return true;
			}
			else
			{
				int range = 10;
				int posX = (int) (c.getX() + world.random.nextInt(range) - world.random.nextInt(range));
				int posZ = (int) (c.getZ() + world.random.nextInt(range) - world.random.nextInt(range));
				int posY = (int) c.getY();

				while(world.isEmptyBlock(new BlockPos(posX,posY,posZ)))
				{
					posY--;
					if(posY<=0)
						return false;
				}

				while(!world.isEmptyBlock(new BlockPos(posX,posY,posZ)) && posY < 256)
				{
					posY++;
					if(posY >= 256)
						return false;
				}


				double disSqC = target.distToCenterSqr(c.position());
				double disSq = (target.getX()-posX)*(target.getX()-posX) + (target.getY()-posY)*(target.getY()-posY) +(target.getZ()-posZ)*(target.getZ()-posZ);
				if(disSq < disSqC || world.random.nextInt(10)==0)
				{
					p = c.getNavigation().createPath(new BlockPos(posX, posY, posZ), 1); //20 is the max away distance
					if(p!=null)
					{
						c.getNavigation().moveTo(p, 0.7F);
						((ServerLevel)world).addParticle(ParticleTypes.FIREWORK, true, c.getX(), c.getY(), c.getZ(), 0, 0.1, 0);
						return true;
					}
				}
			}
		}
		return false;
	}


}
