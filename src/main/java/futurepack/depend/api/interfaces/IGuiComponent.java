package futurepack.depend.api.interfaces;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.gui.escanner.GuiScannerBase;
import net.minecraft.client.gui.screens.Screen;

public interface IGuiComponent
{
	
	public void init(int maxWidth, Screen gui);
	
	
	public void setAdditionHeight(int additionHight);
	/**
	 * @return the second param overgiven by the setAdditionHieght, just pass it back.
	 */
	public int getAdditionHeight();
	
	public int getWidth();
	
	public int getHeight();
	
	/**
	 * Render the Maion things
	 * @param blitOffset TODO
	 */
	public void render(PoseStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, GuiScannerBase gui);
	
	/**
	 * render Hover texts
	 * @param matrixStack TODO
	 * @param blitOffset TODO
	 */
	public void postRendering(PoseStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, boolean hover, GuiScannerBase gui);
	
	public void onClicked(int x, int y, int mouseButton, double mouseX, double mouseY, GuiScannerBase gui);
}
