package futurepack.depend.api.interfaces;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtIo;
import net.minecraft.network.FriendlyByteBuf;

public interface IGuiSyncronisedContainer 
{
	public void writeToBuffer(FriendlyByteBuf buf);
	
	public void readFromBuffer(FriendlyByteBuf buf);
	
	public static void writeNBT(FriendlyByteBuf buf, CompoundTag nbt)
	{
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		try 
		{
			NbtIo.writeCompressed(nbt, bytes);
			buf.writeByteArray(bytes.toByteArray());
			bytes.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static CompoundTag readNBT(FriendlyByteBuf buf)
	{
		ByteArrayInputStream bytes = new ByteArrayInputStream(buf.readByteArray());
		try
		{
			CompoundTag nbt = NbtIo.readCompressed(bytes);
			return nbt;
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
