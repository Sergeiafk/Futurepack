package futurepack.client.render.block;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import futurepack.api.Constants;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.resources.ResourceLocation;

public class ModelDeepCoreMiner extends SegmentedBlockModel
{

	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(Constants.MOD_ID, "deep_core_miner"), "main");
	private final ModelPart root;
	
    public ModelPart Linse, Linse_glow;
    public ModelPart Fokusmechanik;

    public ModelDeepCoreMiner(ModelPart root) 
    {
    	super(RenderType::entityCutout);
    	
		this.root = root.getChild("root");
		Linse = this.root.getChild("Linse");
		Linse_glow = root.getChild("root_glow").getChild("Linse_glow");
		Fokusmechanik = this.root.getChild("Fokusmechanik");
	}

	public static LayerDefinition createBodyLayer() 
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition root = partdefinition.addOrReplaceChild("root", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, -16.0F));
		PartDefinition root_glow = partdefinition.addOrReplaceChild("root_glow", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, -16.0F));

		PartDefinition Arm  = root.addOrReplaceChild("Arm", CubeListBuilder.create().texOffs(69, 39).addBox(-3.0F, -19.0F, -4.0F, 6.0F, 22.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -22.0F, 33.0F, 0.7436F, 0.0F, 0.0F));

		PartDefinition Kopf = root.addOrReplaceChild("Kopf", CubeListBuilder.create().texOffs(104, 28).addBox(-8.0F, 0.0F, -8.0F, 16.0F, 16.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -48.0F, 16.0F));

		PartDefinition Transformer = root.addOrReplaceChild("Transformer", CubeListBuilder.create().texOffs(0, 25).addBox(-8.0F, -24.0F, 8.0F, 16.0F, 24.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		PartDefinition Bodenplatte = root.addOrReplaceChild("Bodenplatte", CubeListBuilder.create().texOffs(106, 67).addBox(-9.0F, -3.0F, -24.0F, 18.0F, 3.0F, 32.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		PartDefinition Bohrloch = root.addOrReplaceChild("Bohrloch", CubeListBuilder.create().texOffs(78, 0).addBox(-8.0F, -4.0F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		PartDefinition Photonengenerator = root.addOrReplaceChild("Photonengenerator", CubeListBuilder.create().texOffs(148, 0).addBox(-5.0F, -32.0F, -5.0F, 10.0F, 8.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		PartDefinition Linse = root.addOrReplaceChild("Linse", CubeListBuilder.create().texOffs(67, 25).addBox(-4.0F, -22.0F, -4.0F, 8.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));
		PartDefinition Linse_glow = root_glow.addOrReplaceChild("Linse_glow", CubeListBuilder.create().texOffs(188, 0).addBox(-4.0F, -22.0F, -4.0F, 8.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));
		
		PartDefinition Fokusmechanik = root.addOrReplaceChild("Fokusmechanik", CubeListBuilder.create().texOffs(172, 28).addBox(-3.0F, -18.0F, -3.0F, 6.0F, 16.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -20.0F, 16.0F));

		PartDefinition Sicherheitsgitter = root.addOrReplaceChild("Sicherheitsgitter", CubeListBuilder.create().texOffs(86, 106).addBox(-8.0F, -47.0F, -24.0F, 16.0F, 44.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		PartDefinition Verbindung4 = root.addOrReplaceChild("Verbindung4", CubeListBuilder.create().texOffs(65, 73).addBox(7.0F, -47.0F, 14.0F, 2.0F, 44.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 16.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition Verbindung3 = root.addOrReplaceChild("Verbindung3", CubeListBuilder.create().texOffs(65, 73).addBox(7.0F, -47.0F, -18.0F, 2.0F, 44.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		PartDefinition Verbindung2 = root.addOrReplaceChild("Verbindung2", CubeListBuilder.create().texOffs(65, 73).addBox(7.0F, -47.0F, -1.0F, 2.0F, 44.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 16.0F, 0.0F, -3.1416F, 0.0F));

		PartDefinition Verbindung1 = root.addOrReplaceChild("Verbindung1", CubeListBuilder.create().texOffs(65, 73).addBox(7.0F, -47.0F, -2.0F, 2.0F, 44.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		PartDefinition Lagereinheit3 = root.addOrReplaceChild("Lagereinheit3", CubeListBuilder.create().texOffs(0, 67).addBox(-7.0F, -48.0F, -23.0F, 14.0F, 14.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		PartDefinition Lagereinheit2 = root.addOrReplaceChild("Lagereinheit2", CubeListBuilder.create().texOffs(0, 67).addBox(-7.0F, -33.0F, -23.0F, 14.0F, 14.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		PartDefinition Lagereinheit1 = root.addOrReplaceChild("Lagereinheit1", CubeListBuilder.create().texOffs(0, 67).addBox(-7.0F, -18.0F, -23.0F, 14.0F, 14.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		return LayerDefinition.create(meshdefinition, 256, 256);
	}

    
    public void setLaserPosition(float y)
    {
    	y*=16F;
    	Linse.y = y;
    	Linse_glow.y = y;
    	Fokusmechanik.y = y - 20F;
    }
    
    @Override
    public ModelPart[] getParts() 
    {
    	return new ModelPart[] {root};
    }
    
    @Override
    public void renderToBuffer(PoseStack matrixStackIn, VertexConsumer bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) 
    {
    	super.renderToBuffer(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, 1F, 1F, 1F, alpha);
    	Linse_glow.render(matrixStackIn, bufferIn, 0xF000F0, packedOverlayIn, red, green, blue, alpha); //emmissiv rendering
    }
}
