package futurepack.client.render.block;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.blaze3d.vertex.BufferUploader;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.blaze3d.vertex.VertexFormat;
import com.mojang.datafixers.util.Pair;

import futurepack.api.Constants;
import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.FacingUtil;
import futurepack.api.capabilities.CapabilityLogistic;
import futurepack.api.capabilities.ILogisticInterface;
import futurepack.common.FPLog;
import futurepack.common.item.tools.ItemLogisticEditor;
import futurepack.common.item.tools.ToolItems;
import futurepack.depend.api.helper.HelperRendering;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.HitResult.Type;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.common.util.LazyOptional;


public class RenderLogistic
{
	static ResourceLocation logistic = new ResourceLocation(Constants.MOD_ID, "textures/model/overlay_logistic.png");
	
	private static Tesselator tesselator = new Tesselator(20480);
	private static Runnable renderCall = null;
	
	private static void renderLogistics(ILogisticInterface log, double posX, double posY, double posZ, EnumLogisticType mode, Direction face, boolean hovered, BlockEntity tile, BufferBuilder tes, PoseStack matrixStack)
	{			
		matrixStack.pushPose();
		RenderType.cutout().setupRenderState();
		
		bindTexture(logistic);
        
		RenderSystem.getModelViewMatrix().load(matrixStack.last().pose());
		
		if(hovered)
		{
			Minecraft.getInstance().level.getProfiler().push("drawHovered");
			double min = 0.02;
			BufferBuilder buffer = Tesselator.getInstance().getBuilder();
			buffer.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.BLOCK);
			renderFace(face, posX-min, posY-min, posZ-min, 1+min*2, 1+min*2, 1+min*2, mode.color, buffer);

			Tesselator.getInstance().end();
			
			GL11.glDepthMask(false);
			EnumLogisticIO io = log.getMode(mode);
			if(io!=EnumLogisticIO.NONE)
			{
				buffer.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.BLOCK);
				bindTexture(io.getTexture());
				min = 0.04;
				renderFace(face, posX-min, posY-min, posZ-min, 1+min*2, 1+min*2, 1+min*2, 0xFFFFFFFF, buffer);
				
				Tesselator.getInstance().end();
			}
			GL11.glDepthMask(true);
			
			Minecraft.getInstance().level.getProfiler().pop();
		}
		else
		{
			VoxelShape sh = tile.getLevel().getBlockState(tile.getBlockPos()).getBlockSupportShape(tile.getLevel(), tile.getBlockPos());
			AABB bb;
			if(sh.isEmpty())
				bb = new AABB(0,0,0, 1,1,1);
			else
				bb = sh.bounds();
//				bb = bb.offset(-tile.getPos().getX(),-tile.getPos().getY(),-tile.getPos().getZ());								
			double min = 0.02;
			renderFace(face, posX-min +bb.minX, posY-min +bb.minY, posZ-min + bb.minZ, (bb.maxX-bb.minX)+min*2, (bb.maxY-bb.minY)+min*2, (bb.maxZ-bb.minZ)+min*2, mode.color, tes);
		}
		
		RenderType.cutout().clearRenderState();				
		matrixStack.popPose();
	}
	
	private static void renderFace(Direction face, double posX, double posY, double posZ, double sizeX, double sizeY, double sizeZ, int color, BufferBuilder tes)
	{
		Minecraft.getInstance().level.getProfiler().push("renderFace");
		
		int x0 = Float.floatToIntBits((float) posX);
		int x1 = Float.floatToIntBits((float)(posX+sizeX));
		int y0 = Float.floatToIntBits((float) posY);
		int y1 = Float.floatToIntBits((float)(posY+sizeY));
		int z0 = Float.floatToIntBits((float) posZ);
		int z1 = Float.floatToIntBits((float)(posZ+sizeZ));
		int u0 = Float.floatToIntBits(0F);
		int u1 = Float.floatToIntBits(1F);
		int v0 = Float.floatToIntBits(0F);
		int v1 = Float.floatToIntBits(1F);
		
		int[] vertexData = null;
		int ligthMap = 0xF000FF;
		int normal = 0x7F0000;
		
		if(face== Direction.UP)
		{		
			vertexData = new int[] {
					x0,y1,z1,color,u0,v0,ligthMap,normal,
					x1,y1,z1,color,u1,v0,ligthMap,normal,
					x1,y1,z0,color,u1,v1,ligthMap,normal,
					x0,y1,z0,color,u0,v1,ligthMap,normal
			};
			
		}
		else if(face== Direction.DOWN)
		{
			vertexData = new int[]{
					x0,y0,z0,color,u1,v0,ligthMap,normal,
					x1,y0,z0,color,u0,v0,ligthMap,normal,
					x1,y0,z1,color,u0,v1,ligthMap,normal,
					x0,y0,z1,color,u1,v1,ligthMap,normal
			};
		}
		else if(face== Direction.NORTH)
		{
			vertexData = new int[]{
					x0,y1,z0,color,u1,v0,ligthMap,normal,
					x1,y1,z0,color,u0,v0,ligthMap,normal,
					x1,y0,z0,color,u0,v1,ligthMap,normal,
					x0,y0,z0,color,u1,v1,ligthMap,normal
			};
		}
		else if(face== Direction.SOUTH)
		{
			vertexData = new int[]{
					x1,y1,z1,color,u1,v0,ligthMap,normal,
					x0,y1,z1,color,u0,v0,ligthMap,normal,
					x0,y0,z1,color,u0,v1,ligthMap,normal,
					x1,y0,z1,color,u1,v1,ligthMap,normal
			};
		}
		else if(face== Direction.WEST)
		{
			vertexData = new int[]{
					x0,y1,z1,color,u1,v0,ligthMap,normal,
					x0,y1,z0,color,u0,v0,ligthMap,normal,
					x0,y0,z0,color,u0,v1,ligthMap,normal,
					x0,y0,z1,color,u1,v1,ligthMap,normal
			};
		}
		else if(face== Direction.EAST)
		{
			vertexData = new int[] {
					x1,y1,z0,color,u1,v0,ligthMap,normal,
					x1,y1,z1,color,u0,v0,ligthMap,normal,
					x1,y0,z1,color,u0,v1,ligthMap,normal,
					x1,y0,z0,color,u1,v1,ligthMap,normal
			};
		}
		else
		{
			FPLog.logger.fatal("Unkown Direction " + (face==null?"null":face.toString()));
		}
		
		if(vertexData!=null)
		{
			ByteBuffer buffer = ByteBuffer.allocate(vertexData.length * 4);
			buffer.order(ByteOrder.nativeOrder());
			buffer.asIntBuffer().put(vertexData);
			tes.putBulkData(buffer);
		}
		Minecraft.getInstance().level.getProfiler().pop();
	}

	public static void onItemHeld(ItemStack heldItem, float partialTicks, PoseStack matrixStack)
	{
		matrixStack.pushPose();
		RenderSystem.enableBlend();
		RenderSystem.defaultBlendFunc();
		RenderSystem.getModelViewStack().pushPose();
		
//		Matrix4f mat = matrixStack.last().pose();
//		FloatBuffer matF = MemoryTracker.create(16 * 4).asFloatBuffer();
//		mat.store(matF);
//		GL11.glLoadMatrixf(matF);
//		GlStateManager.disableDepthTest();
		
		Player pl = Minecraft.getInstance().player;
		ClientLevel cl = Minecraft.getInstance().level;
		
		float apartialTicks = 1F -partialTicks;
		double pX,pY,pZ;
		pX = (pl.getX() * partialTicks + apartialTicks * pl.xo);
		pY = (pl.getY() * partialTicks + apartialTicks * pl.yo) + pl.getEyeHeight();
		pZ = (pl.getZ() * partialTicks + apartialTicks * pl.zo);
		
		cl.getProfiler().push("renderLogisticsOverlay");
		if(pl != null && Minecraft.getInstance().screen==null)
		{
			Minecraft mc = Minecraft.getInstance();
			boolean mainH = !pl.getItemInHand(InteractionHand.MAIN_HAND).isEmpty() && pl.getItemInHand(InteractionHand.MAIN_HAND).getItem() == ToolItems.logisticEditor;
			boolean offH = !pl.getItemInHand(InteractionHand.OFF_HAND).isEmpty() && pl.getItemInHand(InteractionHand.OFF_HAND).getItem() == ToolItems.logisticEditor;
			
			if(mainH || offH)
			{
				EnumLogisticType mode = ItemLogisticEditor.getModeFromItem(pl.getItemInHand(mainH ? InteractionHand.MAIN_HAND : InteractionHand.OFF_HAND));
				
				BlockHitResult res = null;
				if(mc.hitResult.getType() == Type.BLOCK)
				{
					res = (BlockHitResult) mc.hitResult;
				}
				
				HelperRendering.disableLighting();
//				OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 200.0F, 200.0F);
				setLightmapDisabled(true);
				
				if(renderCall == null)
				{
					BufferBuilder tes = tesselator.getBuilder();
					tes.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.BLOCK);
						
					
					int r = 5;
					ChunkPos p = new ChunkPos(new BlockPos(pl.position()));
					for(int dx=-r;dx<=r;dx++)
					{
						for(int dz=-r;dz<=r;dz++)
						{
							int chunkX = p.x + dx;
							int chunkZ = p.z + dz;
							
							for(BlockEntity tile : cl.getChunk(chunkX, chunkZ).getBlockEntities().values())
							{
								double posX = tile.getBlockPos().getX();
								double posY = tile.getBlockPos().getY();
								double posZ = tile.getBlockPos().getZ();
									
								render(tile, posX, posY, posZ, partialTicks, mode, res, tes, matrixStack);
							}
						}	
					}
					
					tes.end();
					renderCall = createStaticRenderCall(tes);
					
				}
				if(renderCall != null)
				{					
					matrixStack.pushPose();
					matrixStack.translate(-pX, -pY, -pZ);
										
					RenderType.cutout().setupRenderState();
	         
					bindTexture(logistic);
	       
					RenderSystem.getModelViewMatrix().load(matrixStack.last().pose());
					
					drawBufferWithoutReset();
					
					RenderType.cutout().clearRenderState();
					
					matrixStack.popPose();
					if(System.currentTimeMillis() % 1000 / 10 == 0)
					{
						renderCall = null;
					}
				}
				if(res != null)
				{
					BlockEntity tile = cl.getBlockEntity(res.getBlockPos());
					if(tile != null)
					{
						double posX = (tile.getBlockPos().getX() - pX);
						double posY = (tile.getBlockPos().getY() - pY);
						double posZ = (tile.getBlockPos().getZ() - pZ);
						
						render(tile, posX, posY, posZ, partialTicks, mode, res, null, matrixStack);
					}
				}
				HelperRendering.enableLighting();
				setLightmapDisabled(false);
			}
		}
		cl.getProfiler().pop();
		RenderSystem.disableBlend();
		
		RenderSystem.getModelViewStack().popPose();
		RenderSystem.applyModelViewMatrix();
		
		matrixStack.popPose();
	}

	private static void setLightmapDisabled(boolean b) 
	{
		if(b)
		{
			Minecraft.getInstance().gameRenderer.lightTexture().turnOffLightLayer();
		}
		else
		{
			Minecraft.getInstance().gameRenderer.lightTexture().turnOnLightLayer();
		}
		
	}

	private static void bindTexture(ResourceLocation path) 
	{
        RenderSystem.enableTexture();
        TextureManager texturemanager = Minecraft.getInstance().getTextureManager();
        texturemanager.getTexture(path).setFilter(false, true);
        RenderSystem.setShaderTexture(0, path);
	}

	private static void render(BlockEntity tile, double posX, double posY, double posZ, float partialTicks, EnumLogisticType mode, BlockHitResult res, BufferBuilder tes, PoseStack matrixStack)
	{
		final boolean hover = res==null ? false : tile.getBlockPos().equals(res.getBlockPos());
		
		for(Direction facing : FacingUtil.VALUES)
		{
			LazyOptional<ILogisticInterface> opt = tile.getCapability(CapabilityLogistic.cap_LOGISTIC, facing);
			opt.ifPresent(log -> 
			{
				if(log.isTypeSupported(mode))
				{
					renderLogistics(log, posX, posY, posZ, mode, facing, hover, tile, tes, matrixStack);
				}
			});
		}
	}
	
	public static Runnable createStaticRenderCall(BufferBuilder bufferBuilderIn)//copied from WorldVertexBufferUploader#draw
	{
		//now BufferUploader#end
		
		final Pair<BufferBuilder.DrawState, ByteBuffer> pair = bufferBuilderIn.popNextBuffer();
		BufferBuilder.DrawState bufferbuilder$drawstate = pair.getFirst();
		ByteBuffer bufferIn = pair.getSecond();
		
		return () -> 
		{
			BufferUploader._end(bufferIn, bufferbuilder$drawstate.mode(), bufferbuilder$drawstate.format(), bufferbuilder$drawstate.vertexCount(), bufferbuilder$drawstate.indexType(), bufferbuilder$drawstate.indexCount(), bufferbuilder$drawstate.sequentialIndex());
		};
	}
	
	public static void drawBufferWithoutReset() 
	{
		if(renderCall!=null)
		{
			renderCall.run();
		}
	}

	public static void refreshRenderng() 
	{
		renderCall = null;
	}

}
