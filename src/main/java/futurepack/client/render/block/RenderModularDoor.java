package futurepack.client.render.block;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.block.misc.BlockModularDoor;
import futurepack.common.block.misc.TileEntityModularDoor;
import futurepack.depend.api.helper.HelperRenderBlocks;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.core.Direction;
import net.minecraft.core.Direction.AxisDirection;
import net.minecraft.world.level.block.DirectionalBlock;
import net.minecraft.world.level.block.state.BlockState;

public class RenderModularDoor implements BlockEntityRenderer<TileEntityModularDoor>
{

	public RenderModularDoor(BlockEntityRendererProvider.Context rendererDispatcherIn)
	{
//		super(rendererDispatcherIn);
	}

	public static boolean isRendering = false;


	@Override
	public void render(TileEntityModularDoor te, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int combinedLightIn, int combinedOverlayIn)
	{
		te.getLevel().getProfiler().push("renderModularDoor");
		if(te.canRender())
		{
			BlockState orginal = te.getLevel().getBlockState(te.getBlockPos());
			BlockState state = orginal.setValue(BlockModularDoor.COMPLETE, true).setValue(BlockModularDoor.OPEN, false);

			matrixStackIn.pushPose();
			GlStateManager._blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

			Direction face = orginal.getValue(DirectionalBlock.FACING);
			Direction.Axis axis = face.getAxis();
			float m = te.getPrevSize() /16F  + (te.getSize() - te.getPrevSize())/16F * partialTicks;
			float x=0F,y=0F,z=0F;
			if(face.getAxisDirection()==AxisDirection.NEGATIVE)
			{
				switch (axis)
				{
				case X:
					x+=(1-m);
					break;
				case Y:
					y+=(1-m);
					break;
				case Z:
					z+=(1-m);
					break;
				}
			}
			matrixStackIn.translate(x, y, z);
			float sx=1F,sy=1F,sz=1F;
			switch (axis)
			{
			case X:
				sx*=m;
				break;
			case Y:
				sy*=m;
				break;
			case Z:
				sz*=m;
				break;
			}
			matrixStackIn.scale(sx, sy, sz);

			isRendering=true;
			HelperRenderBlocks.renderBlockSlow(state, te.getBlockPos(), te.getLevel(), matrixStackIn, bufferIn, false);
			isRendering=false;

			matrixStackIn.popPose();
		}
		te.getLevel().getProfiler().pop();
	}


}
