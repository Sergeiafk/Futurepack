package futurepack.client.render.block;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Vector3f;

import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.misc.TileEntityNeonEngine;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.core.Direction;

public class RenderNeonEngine implements BlockEntityRenderer<TileEntityNeonEngine>
{
	public RenderNeonEngine(BlockEntityRendererProvider.Context rendererDispatcherIn) 
	{
//		super(rendererDispatcherIn);
		model = new ModelNeonEngine2(rendererDispatcherIn.bakeLayer(ModelNeonEngine2.LAYER_LOCATION));
	}

	ModelNeonEngine2 model;

	
	@Override
	public void render(TileEntityNeonEngine tile, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int combinedLightIn, int combinedOverlayIn) 
	{
		tile.getLevel().getProfiler().push("renderNeonEngine");
		
		matrixStackIn.pushPose();
		matrixStackIn.translate(0.5, 0.5, 0.5);
		
		float d = 1F/16F;
		
		Direction meta = tile.getBlockState().getValue(BlockRotateableTile.FACING);
		
		
		if(meta== Direction.DOWN)
			matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(-90));
		else if(meta== Direction.UP)
			matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(90F));
		else
		{
			matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(180F));

//			matrixStack.mulPose(Vector3f.YP.rotationDegrees(90F*meta.getHorizontalIndex()));
			matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(90F*meta.get2DDataValue()));
		}
		
		VertexConsumer builder = bufferIn.getBuffer(model.renderType(tile.heatState.getTexture()));
		
		model.setOut( (System.currentTimeMillis()+tile.hashCode())%1000 * tile.heatState.getSpeed() );
		model.renderToBuffer(matrixStackIn, builder, combinedLightIn, combinedOverlayIn, 1F, 1F, 1F, 1F);
		
		matrixStackIn.popPose();
		tile.getLevel().getProfiler().pop();
	}

}
