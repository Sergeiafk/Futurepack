package futurepack.client.render.block;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Vector3f;

import futurepack.api.Constants;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.modification.TileEntityRadar;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;

public class RenderRadar implements BlockEntityRenderer<TileEntityRadar>
{
	public RenderRadar(BlockEntityRendererProvider.Context rendererDispatcherIn)
	{
//		super(rendererDispatcherIn);
		model = new ModelRadar(rendererDispatcherIn.bakeLayer(ModelRadar.LAYER_LOCATION));
	}

	ModelRadar model;
	ResourceLocation texture = new ResourceLocation(Constants.MOD_ID, "textures/blocks/misc/radar.png");
	
	@Override
	public void render(TileEntityRadar tile, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int combinedLightIn, int combinedOverlayIn)
	{
		tile.getLevel().getProfiler().push("renderRadar");
		
		matrixStackIn.pushPose();
		matrixStackIn.translate(0.5, 1.5, 0.5);
		matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(180F));
		
		Direction direction = tile.getBlockState().getValue(BlockRotateableTile.FACING);

		if(direction == Direction.DOWN) {
			matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(-180F));
			matrixStackIn.translate(0,-2,0);
		}
		else if(direction == Direction.NORTH) {
			matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(-90F));
			matrixStackIn.translate(0,-1,1);
		}
		else if(direction == Direction.EAST) {
			matrixStackIn.mulPose(Vector3f.ZP.rotationDegrees(90F));
			matrixStackIn.translate(1,-1,0);
		}
		else if(direction == Direction.SOUTH) {
			matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(90F));
			matrixStackIn.translate(0,-1,-1);
		}
		else if(direction == Direction.WEST) {
			matrixStackIn.mulPose(Vector3f.ZP.rotationDegrees(-90F));
			matrixStackIn.translate(-1,-1,0);
		}
		
		VertexConsumer builder = bufferIn.getBuffer(model.renderType(texture));

		tile.updateRotation(partialTicks);

		model.rotateSpiegel(tile.rotation);

		model.renderToBuffer(matrixStackIn, builder, combinedLightIn, combinedOverlayIn, 1F, 1F, 1F, 1F);
		
		matrixStackIn.popPose();
		tile.getLevel().getProfiler().pop();
	}

}
