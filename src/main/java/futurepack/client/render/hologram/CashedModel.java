//package futurepack.client.render.hologram;
//
//import java.nio.ByteBuffer;
//import java.nio.ByteOrder;
//
//import com.mojang.blaze3d.vertex.BufferBuilder;
//import com.mojang.blaze3d.vertex.DefaultVertexFormat;
//import com.mojang.blaze3d.vertex.PoseStack;
//import com.mojang.blaze3d.vertex.VertexFormat;
//
//import net.minecraft.client.renderer.MultiBufferSource;
//import net.minecraft.core.BlockPos;
//import net.minecraft.world.level.Level;
//import net.minecraft.world.level.block.state.BlockState;
//
//public class CashedModel
//{
//	private static final BufferBuilder buffer = new BufferBuilder(4*7*40);
//	
//	private final long UPDATE_TIME = 2000L;
//	
//	private int light;
//	
//	private Level world;
//	private BlockPos pos;
//	private BlockState state;
//	
//	private ByteBuffer data;
//	private int elements;
//	
//	private long creationTime = 0L;
//	
//	private boolean old = true;
//	
//	public CashedModel(BlockPos pos, BlockState state, Level world)
//	{
//		this.world = world;
//		this.pos = pos;
//		this.state = state;
//		
//		//FIXME create();
//	}
//	
//	private void create(PoseStack matrixStack, MultiBufferSource bufferTypeIn)
//	{				
//		old = false;
//		
//		light = world.getRawBrightness(pos, 0);
//		//Tessellator tes = Tessellator.getInstance();
//		
//		buffer.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.BLOCK); ////Blocks: 3F-pos, 4UB-color, 2F-UV, 2S-UV 
//		
//		//FIXME buffer.setTranslation(-pos.getX(), -pos.getY(), -pos.getZ());
//		TemporaryWorld temp = new TemporaryWorld(world, pos, state);
////		HelperRenderBlocks.renderBlockTESR(state, pos, temp, buffer);
//		
//		//FIXME HelperRenderBlocks.renderBlockTESR(state, matrixStackIn, bufferTypeIn, light, combinedOverlayIn);
//		
//		//FIXME elements = buffer.getVertexCount();
//		buffer.end();
//		byte[] bytes = new byte[elements * DefaultVertexFormat.BLOCK.getVertexSize()];
//		//FIXME buffer.getByteBuffer().get(bytes);
//		buffer.clear(); //so this wont get rendered...
//		
//		data = ByteBuffer.wrap(bytes).asReadOnlyBuffer();
//		data.order(ByteOrder.LITTLE_ENDIAN);
//		creationTime = System.currentTimeMillis();
//	}
//	
//	public void build(float x, float y, float z, BufferBuilder buf)
//	{
//		if(old)//some random so no big lag
//		{
//			//FIXME create();
//		}
//		if(buf.getVertexFormat() == DefaultVertexFormat.BLOCK && elements > 0)
//		{		
//			ByteBuffer vertex = translate(x, y, z);
//			vertex.order(ByteOrder.BIG_ENDIAN);
////			HelperRenderBlocks.insertRenderData(buf, vertex, elements);
//		}
//	}
//
//	private ByteBuffer translate(float x, float y, float z)//data buffer wird sehr oft kopiert, optimierungs bedarf...
//	{
//		int size = DefaultVertexFormat.BLOCK.getVertexSize();
//		int length = size * elements;
//		byte[] bytes = new byte[length];
//		this.data.position(0);
//		this.data.get(bytes); 
//		ByteBuffer data = ByteBuffer.wrap(bytes);
//		data.order(ByteOrder.LITTLE_ENDIAN);
//		for(int i=0;i<elements;i++)
//		{
//			data.position(i * size);
//			float ox, oy, oz;
//			ox = data.getFloat();
//			oy = data.getFloat();
//			oz = data.getFloat();
//				
//			data.position(i * size);
//
//			data.putFloat(ox+x);
//			data.putFloat(oy+y);
//			data.putFloat(oz+z);
//		}	
//		data.position(0);
//		return data;
//	}
//
//	public BlockPos getPos()
//	{
//		return pos;
//	}
//
//	public BlockState getBlockState()
//	{
//		return state;
//	}
//	
//	public int getLight()
//	{
//		return light;
//	}
//
//	public void clear()
//	{
//		pos=null;
//		state = null;
//		world = null;
//		data.clear();
//		data = null;
//		light = -1;
//		elements = -1;
//	}
//
//	public void markDirty()
//	{
//		old = true;
//	}
//}
