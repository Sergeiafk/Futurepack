package futurepack.client.render.hologram;

import java.util.HashMap;

import futurepack.api.interfaces.tilentity.ITileHologramAble;
import net.minecraft.core.BlockPos;
import net.minecraft.util.profiling.ProfilerFiller;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class TemporaryWorld extends DelegatedWorldReader
{
	ProfilerFiller prof;
	
	public final HashMap<BlockPos, BlockState> overwriteState;
	public final HashMap<BlockPos, BlockEntity> overwriteTiles;
	
	LevelReader original;
	boolean debug = true;
	
	long update;
	
	public TemporaryWorld(LevelReader ori, BlockPos p, BlockState f, BlockEntity tile)
	{
		super(ori);
		original = ori;
		overwriteState = new HashMap<>();
		overwriteTiles = new HashMap<>();
		overwriteState.put(p, f);
		overwriteTiles.put(p, tile);
		debug = false;
		update = System.currentTimeMillis();
		
		if(ori instanceof Level)
		{
			prof = ((Level) ori).getProfiler();
		}
	}
	
	public TemporaryWorld(Level ori, BlockPos p, BlockState f)
	{
		this(ori, p, f, f.hasBlockEntity() ?  ((EntityBlock)f.getBlock()).newBlockEntity(p, f) : null);
		debug = true;
	}
	
	@Override
	public BlockEntity getBlockEntity(BlockPos pos)
	{
		startSection("w.getTileEntity");
		BlockEntity tile;
		if(overwriteTiles.containsKey(pos))
		{
			tile =  overwriteTiles.get(pos);
		}
		else
		{
			tile = original.getBlockEntity(pos);
		}
		endSection();
		return tile;
	}

	@Override
	public BlockState getBlockState(BlockPos pos)
	{
		startSection("w.getBlockState");
		BlockState state = overwriteState.getOrDefault(pos, null);
		if(state==null)
		{
			if(getBlockEntity(pos) !=null)
			{
				BlockEntity tile = getBlockEntity(pos);
				if(tile instanceof ITileHologramAble)
				{
					ITileHologramAble holo = (ITileHologramAble) getBlockEntity(pos);
					if(holo.hasHologram())
					{
						state =  holo.getHologram();
					}
				}		
			}
			if(state==null)
			{
				state = original.getBlockState(pos);
			}
		}
		endSection();
		return state;
	}


	public void checkOld()
	{
		if(System.currentTimeMillis() - update > 1000)
		{
			update = System.currentTimeMillis();
			overwriteState.clear();
			overwriteTiles.clear();
		}
	}
	
	private void startSection(String name)
	{
		if(prof!=null)
			prof.push(name);
	}
	
	private void endSection()
	{
		if(prof!=null)
			prof.pop();
	}
}
