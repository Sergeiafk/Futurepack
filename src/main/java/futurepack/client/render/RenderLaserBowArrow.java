package futurepack.client.render;

import com.mojang.math.Vector4f;

import futurepack.api.Constants;
import futurepack.common.entity.throwable.EntityLaser;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;

public class RenderLaserBowArrow extends RenderThrowable3DBase<EntityLaser>
{

	public RenderLaserBowArrow(EntityRendererProvider.Context renderManager)
	{
		super(renderManager);
	}

	@Override
    public ResourceLocation getTextureLocation(EntityLaser entity)
	{
		return new ResourceLocation(Constants.MOD_ID,"textures/model/lsgeschoss.png");
	}

	private final Vector4f side = new Vector4f(0, 0F, 7F/16F, 1F);
	
	@Override
	public Vector4f getUVForSides(EntityLaser entity) 
	{
		return side;
	}

	private final Vector4f front = new Vector4f(8F/16F, 0F, 15f/16F , 7f/16F);
	
	@Override
	public Vector4f getUVForFront(EntityLaser entity) 
	{
		return front;
	}

	@Override
	public float getWidth(EntityLaser entity) 
	{
		return 0.0625F * 3.5F;
	}

	@Override
	public float getDepth(EntityLaser entity) 
	{
		return 0.1875F;
	}
}

