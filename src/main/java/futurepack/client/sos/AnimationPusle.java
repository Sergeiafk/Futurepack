package futurepack.client.sos;

import java.util.Random;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Vector3f;

import futurepack.api.Constants;
import futurepack.common.block.inventory.TileEntityScannerBlock;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.resources.ResourceLocation;

public class AnimationPusle extends AnimationPicture
{
	private final int parts;
	private final ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/os/fragez.png");
	private Random r;
	public AnimationPusle(int time, TileEntityScannerBlock data)
	{
		super(new ResourceLocation(Constants.MOD_ID, "textures/os/asp_fehlt.png"), time);
		parts = data.getProperty(4);
		r = new Random();
		//ticks = System.currentTimeMillis() + 10 * 1000;
	}

	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY)
	{
		r.setSeed(hashCode());
		int w = (int) (width * 0.8);
		int h = (int) (height * 0.8);
		int x = xPos + (width)/2;
		int y = yPos + (height)/2;
		
		RenderSystem.setShaderTexture(0, res);
		for(int i=0;i<parts;i++)
		{
			
			matrixStack.pushPose();
			int ws = w / 4;
			int hs = h / 4;
			matrixStack.translate(x, y, 0);
			matrixStack.mulPose(Vector3f.ZP.rotationDegrees(r.nextFloat()*360-90F));
			GuiComponent.blit(matrixStack, -ws*2, -hs*2, ws, hs, 0, 0, 1, 1, 1, 1);
			matrixStack.popPose();
		}
		super.render(matrixStack, mouseX, mouseY);
	}
}
