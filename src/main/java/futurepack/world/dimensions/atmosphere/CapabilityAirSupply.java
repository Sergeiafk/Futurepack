package futurepack.world.dimensions.atmosphere;

import futurepack.api.interfaces.IAirSupply;

public class CapabilityAirSupply implements IAirSupply
{
//	public static class Storage implements IStorage<IAirSupply>
//	{
//
//		@Override
//		public Tag writeNBT(Capability<IAirSupply> capability, IAirSupply instance, Direction side)
//		{
//			return IntTag.valueOf(instance.getAir());
//		}
//
//		@Override
//		public void readNBT(Capability<IAirSupply> capability, IAirSupply instance, Direction side, Tag nbt)
//		{
//			if(nbt instanceof IntTag)
//			{
//				int air = ((IntTag)nbt).getAsInt();
//				instance.addAir(air-instance.getAir());
//			}
//		}
//	}
	
	private int air = 300;

	@Override
	public int getAir()
	{
		return air;
	}

	@Override
	public void addAir(int amount)
	{
		air+=amount;
	}

	@Override
	public void reduceAir(int amount)
	{
		air-=amount;
	} 
	
	@Override
	public int getMaxAir()
	{
		return 300;
	}
	
}
