package futurepack.world.dimensions;

import futurepack.api.Constants;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.Level;

//@Mod.EventBusSubscriber(modid = Constants.MOD_ID, bus = Bus.FORGE)
public class Dimensions
{
	public static final ResourceLocation BELOW_BEDROCK_ID = new ResourceLocation(Constants.MOD_ID, "below_bedrock");
	public static final ResourceLocation MENELAUS_ID = new ResourceLocation(Constants.MOD_ID, "menelaus");
	public static final ResourceLocation TYROS_ID = new ResourceLocation(Constants.MOD_ID, "tyros");
	public static final ResourceLocation ENTROS_ID = new ResourceLocation(Constants.MOD_ID, "entros");
	public static final ResourceLocation LYRARA_ID = new ResourceLocation(Constants.MOD_ID, "lyrara");
	public static final ResourceLocation ENVIA_ID = new ResourceLocation(Constants.MOD_ID, "envia");
	public static final ResourceLocation ASTEROID_BELT_ID = new ResourceLocation(Constants.MOD_ID, "asteroid_belt");
	public static final ResourceKey<Level> BELOW_BEDROCK = ResourceKey.create(Registry.DIMENSION_REGISTRY, BELOW_BEDROCK_ID);

//	public static DimensionType BELOW_BEDROCK, MENELAUS, TYROS, ENTROS, ENVIA;
//
//	/**
//	 * Called from {@link FPRegistry}
//	 * @param event
//	 */
//	public static void register(RegistryEvent.Register<ModDimension> event)
//	{
//		IForgeRegistry<ModDimension> r = event.getRegistry();
//
//		r.register(new ModDimensionBase(DimensionBelowBedrock::new).setRegistryName(BELOW_BEDROCK_ID));
//		r.register(new ModDimensionBase(DimensionMenelaus::new).setRegistryName(MENELAUS_ID));
//		r.register(new ModDimensionBase(DimensionTyros::new).setRegistryName(TYROS_ID));
//		r.register(new ModDimensionBase(DimensionEnvia::new).setRegistryName(ENVIA_ID));
//	}
//
//	@SubscribeEvent
//	public static void register(RegisterDimensionsEvent event)
//	{
//		BELOW_BEDROCK = registerDimensionInternal(BELOW_BEDROCK_ID);
//		MENELAUS = registerDimensionInternal(MENELAUS_ID);
//		TYROS = registerDimensionInternal(TYROS_ID);
//		ENTROS = registerDimensionInternal(ENTROS_ID);
//		ENVIA = registerDimensionInternal(ENVIA_ID);
//	}
//
//	public static DimensionType registerDimensionInternal(ResourceLocation res)
//	{
//		DimensionType t = DimensionType.byName(res);
//		if(t==null)
//		{
//			ModDimension mod = ForgeRegistries.MOD_DIMENSIONS.getValue(res);
//			if(mod!=null)
//			{
//				boolean skyLight = res.equals(MENELAUS_ID) || res.equals(TYROS_ID);
//				t =  DimensionManager.registerDimension(res, mod, null, skyLight);
//			}
//		}
//		if(t!=null && t.getRegistryName()==null)
//		{
//			t.setRegistryName(res);
//		}
//		return t;
//	}
//
//	public static class ModDimensionBase extends ModDimension
//	{
//		private final BiFunction<World, DimensionType, ? extends Dimension> fac;
//
//		public ModDimensionBase(BiFunction<World, DimensionType, ? extends Dimension> factory)
//		{
//			this.fac = factory;
//		}
//
//		@Override
//		public BiFunction<World, DimensionType, ? extends Dimension> getFactory()
//		{
//			return fac;
//		}
//
//	}
}
