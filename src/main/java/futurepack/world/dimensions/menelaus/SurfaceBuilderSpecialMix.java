//package futurepack.world.dimensions.menelaus;
//
//import java.util.Random;
//
//import com.mojang.serialization.Codec;
//
//import net.minecraft.world.level.biome.Biome;
//import net.minecraft.world.level.block.state.BlockState;
//import net.minecraft.world.level.chunk.ChunkAccess;
//import net.minecraft.world.level.levelgen.surfacebuilders.SurfaceBuilder;
//import net.minecraft.world.level.levelgen.surfacebuilders.SurfaceBuilderBaseConfiguration;
//
//public class SurfaceBuilderSpecialMix extends SurfaceBuilder<SurfaceBuilderBaseConfiguration>
//{
//	private final float specialChance;
//	private final SurfaceBuilderBaseConfiguration specialConfig;
//	
//	
//	public SurfaceBuilderSpecialMix(Codec<SurfaceBuilderBaseConfiguration> serializer, SurfaceBuilderBaseConfiguration specialConfig, float specialChance) 
//	{
//		super(serializer);
//		this.specialChance = specialChance;
//		this.specialConfig = specialConfig;
//	}
//	
//	@Override
//	public void apply(Random random, ChunkAccess chunkIn, Biome biomeIn, int x, int z, int startHeight, double noise, BlockState defaultBlock, BlockState defaultFluid, int seaLevel, int pMinSurfaceLevel, long seed, SurfaceBuilderBaseConfiguration config)
//	{
//		if (noise > specialChance) {
//			SurfaceBuilder.DEFAULT.apply(random, chunkIn, biomeIn, x, z, startHeight, noise, defaultBlock, defaultFluid, seaLevel, pMinSurfaceLevel, seed, specialConfig);
//		} else {
//			SurfaceBuilder.DEFAULT.apply(random, chunkIn, biomeIn, x, z, startHeight, noise, defaultBlock, defaultFluid, seaLevel, pMinSurfaceLevel, seed, config);
//		}
//	}
//
//}
