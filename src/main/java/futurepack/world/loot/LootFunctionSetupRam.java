package futurepack.world.loot;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import futurepack.common.FPLootFunctions;
import futurepack.common.item.ItemRam;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.Serializer;
import net.minecraft.world.level.storage.loot.functions.LootItemFunction;
import net.minecraft.world.level.storage.loot.functions.LootItemFunctionType;
import net.minecraft.world.level.storage.loot.providers.number.NumberProvider;

public class LootFunctionSetupRam implements LootItemFunction
{
	private final Boolean isToasted;
    private final NumberProvider ram;
    private final String[] ramid;

    public LootFunctionSetupRam(Boolean toastedIn, NumberProvider ramRangeIn, String...ramidIn)
    {
        isToasted = toastedIn;
        ram = ramRangeIn;
        ramid = ramidIn;
    }

    @Override
	public ItemStack apply(ItemStack stack, LootContext context)
    {
    	CompoundTag innerNBT = new CompoundTag();
    	
//    	if(ram.getMax() > 10 || ram.getMin() < 0)
//    		FPLog.logger.warn("Ram power is out of normal range!");
    		
    	innerNBT.putFloat("ram", ram.getInt(context));
    	
    	if(!isToasted)
    	{
    		if(ItemRam.getRam(stack)!=null)
    		{
    			stack.setTag(innerNBT);
    		}
    		else
    			throw new IllegalStateException("Only rams are working! but got " + stack);
    	}
    	else
    	{
    		CompoundTag outerNBT = new CompoundTag();
    		outerNBT.put("tag", innerNBT);
    		
    		if(ramid!=null)
    		{
	    		if(ramid.length >= 2)
	    		{
	        		outerNBT.putString("id", ramid[context.getRandom().nextInt(ramid.length)]);
	    		}
	    		else if(ramid.length == 1)
	    		{
	    			outerNBT.putString("id", ramid[0]);
	    		}
    		}
    		outerNBT.putBoolean("toasted", true);
    		outerNBT.putInt("Count", 1);
    		stack.setTag(outerNBT);
//    		stack.setItemDamage(ItemSpaceship.toasted_ram);
    	}

        return stack;
    }

    public static class Storage implements Serializer<LootFunctionSetupRam>
    {
    	
            @Override
			public void serialize(JsonObject object, LootFunctionSetupRam functionClazz, JsonSerializationContext serializationContext)
            {
            	object.add("toasted", serializationContext.serialize(functionClazz.isToasted));
            	object.add("ram", serializationContext.serialize(functionClazz.ram));
            	if(functionClazz.isToasted)
            	{
            		object.add("ramid", serializationContext.serialize(functionClazz.ramid));
            	}
            }

            @Override
			public LootFunctionSetupRam deserialize(JsonObject object, JsonDeserializationContext deserializationContext)
            {
            	Boolean toasted = GsonHelper.getAsObject(object, "toasted", deserializationContext, Boolean.class);
            	NumberProvider ram = GsonHelper.getAsObject(object, "ram", deserializationContext, NumberProvider.class);
            	String[] ramid = null;
            	if(toasted)
            	{
            		ramid = GsonHelper.getAsObject(object, "ramid", deserializationContext, String[].class);
            	}
            	
                return new LootFunctionSetupRam(toasted, ram, ramid);
            }
    }

	@Override
	public LootItemFunctionType getType() 
	{
		return FPLootFunctions.SETUP_RAM.get();
	}
    
}
