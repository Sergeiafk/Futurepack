package futurepack.world.gen.feature;

import java.util.Random;

import futurepack.common.FPConfig;
import futurepack.common.block.misc.MiscBlocks;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;

public class BedrockRiftFeature extends Feature<NoneFeatureConfiguration> 
{

	public BedrockRiftFeature() 
	{
		super(NoneFeatureConfiguration.CODEC);
	}

	@Override
	public boolean place(FeaturePlaceContext<NoneFeatureConfiguration> c) 
	{
		Random rand = c.random();
		BlockPos pos = c.origin();
		WorldGenLevel world = c.level();
		
		for (int i = 0; i < FPConfig.WORLDGEN.bedrockRift.get() * 2; i++)
		{
			if(rand.nextFloat() < 0.5f)
				continue;
			
			BlockState state = MiscBlocks.bedrock_rift.defaultBlockState();
			pos = new BlockPos(pos.getX()+2 + rand.nextInt(12), FPConfig.WORLDGEN.bedrockRiftHeight.get(), pos.getZ() + rand.nextInt(12)+2);
				  
			if(!world.isEmptyBlock(pos))
			{
				for(int x = -2; x <= 2; x++)
				{
					for(int z = -2; z <= 2; z++)
					{
						if((Math.abs(x) >= 2 || Math.abs(z) >= 2) && rand.nextFloat() < 0.66f)
							continue;
						
						for(int y = 0; y < 4; y++)
						{
							BlockPos p = pos.offset(x,y,z);
								
							if(y == 0)
							{
							   	if(x==0 && z==0)
							   		world.setBlock(p, state, 2);   
							   	else if(world.getBlockState(p).getBlock() != Blocks.BEDROCK)
							   		world.setBlock(p, Blocks.BEDROCK.defaultBlockState(), 2);		
							}
							else
							{
								if(world.getBlockState(p).getBlock() == Blocks.BEDROCK)
									world.setBlock(p, Blocks.OBSIDIAN.defaultBlockState(), 2);  
							}
						}
					}
				}
			}
		}
		
		return true;
	}

}
