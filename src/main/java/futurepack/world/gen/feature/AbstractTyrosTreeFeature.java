package futurepack.world.gen.feature;

import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import com.mojang.serialization.Codec;

import futurepack.api.FacingUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.core.BlockPos.MutableBlockPos;
import net.minecraft.core.Direction;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.level.LevelSimulatedRW;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.TreeFeature;
import net.minecraft.world.level.levelgen.feature.configurations.TreeConfiguration;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProviderType;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.material.Material;

public abstract class AbstractTyrosTreeFeature<T extends TreeConfiguration> extends Feature<T> 
{

//	public final BlockState LOG;
//	public final BlockState LEAVES;
//	public final BaseTreeFeatureConfig config;
    
	public AbstractTyrosTreeFeature(Codec<T> serialiser)
	{
		super(serialiser);
//		this.LOG = log;
//		this.LEAVES = leaves;
//		
//		if(!LOG.has(BlockStateProperties.AXIS))
//		{
//			throw new IllegalArgumentException("Log need to have axis property");
//		}
//		this.config = config;
	}

	protected void genLeaves(LevelSimulatedRW w, BlockPos pos, int leaveGen, BoundingBox box, TreeConfiguration config, Random rand, Set<BlockPos> changedLeaves)
	{
		float r = (float) (leaveGen + 0.5);
		BlockPos.MutableBlockPos xyz = new BlockPos.MutableBlockPos();
		
		for(int x=(int) -r;x<r;x++)
		{
			for(int y=0;y<r;y++)
			{
				for(int z=(int) -r;z<r;z++)
				{
					if(x*x + y*y + z*z <= r*r)
					{
						xyz.set(pos).move(x,y,z);
						if (TreeFeature.validTreePos(w, xyz))
						{
//							this.setBlockState(w, xyz, LEAVES);
						    setLeaf(w, rand, xyz, changedLeaves, box, config.foliageProvider);
							box.encapsulate(new BoundingBox(xyz));
						}
					}
				}
			}
		}
	}
	
	protected void connectBough(Set<BlockPos> changedLogs, LevelSimulatedRW w, BlockPos startB, BlockPos end, int leaveGen, Random rand, BoundingBox box, TreeConfiguration config)
	{
		int failCounter = 0;
		AtomicInteger offset = new AtomicInteger(0);
		BlockPos.MutableBlockPos start = new BlockPos.MutableBlockPos().set(startB);
		
		BlockStateProvider wrappedConfig = new BlockStateProvider()
		{			
			@Override
			public BlockState getState(Random r, BlockPos pos) 
			{
				return config.trunkProvider.getState(r, pos).setValue(BlockStateProperties.AXIS, Direction.from3DDataValue(offset.get()).getAxis());
			}

			@Override
			protected BlockStateProviderType<?> type() 
			{
				return null;
			}
        };
		
		while(!start.equals(end))
		{
			if(rand.nextInt(20)==0 || start.distSqr(end) > start.relative(Direction.from3DDataValue(offset.get())).distSqr(end))
			{
				start.move(Direction.from3DDataValue(offset.get()));
				if(w.isStateAtPosition(start, this::canReplace))
				{
//					BlockState state = LOG.with(BlockStateProperties.AXIS, Direction.byIndex(offset).getAxis());
//					setLogState(changedBlocks, w, start, state, box);
					setLog(w, rand, start, changedLogs, box, wrappedConfig);
				}
				else
				{
					failCounter++;
					if(failCounter>600000)
					{
						System.out.println("TyrosTreeGen is failing");
						failCounter = 0;
						break;
					}
				}
			}
			else
			{
				offset.set( (offset.get()+1) % FacingUtil.VALUES.length);
			}
		}
	}
	
	
	public boolean canReplace(BlockState state)
	{
		return state.isAir() || state.is(BlockTags.LEAVES) || state.getMaterial() == Material.WATER || state.is(BlockTags.LOGS);
	}
	
	/**
	 * Generates an tree arm from the start position to the end. The arm is bent
 	 */
	protected void genBough(Set<BlockPos> changedLogs, Set<BlockPos> changedLeaves, LevelSimulatedRW w, BlockPos start, BlockPos end, int leaveGen, Random r, boolean leaves, BoundingBox box, TreeConfiguration config)
	{
		
		int sx = end.getX() - start.getX();
		int sy = end.getY() - start.getY();
		int sz = end.getZ() - start.getZ();
		
		int distance = (int) Math.sqrt(sx*sx + sz*sz) + 1;

		float a = (float)-sy / (float)(distance*distance);
		float x_quod = sx / (float)distance;
		float z_quod = sz / (float)distance;
		BlockPos last = start;
		for(int i=0;i<=distance;i++)
		{
			int y = Math.round( a * (i-distance)*(i-distance) + sy);
			int x = Math.round(x_quod * i);
			int z = Math.round(z_quod * i);
			
			BlockPos xyz = start.offset(x,y,z);
			connectBough(changedLogs, w, last, xyz, leaveGen, r, box, config);
			if(leaves)
				genLeaves(w, xyz, leaveGen, box, config, r, changedLeaves);
			last = xyz;
		}
	}
	
	/**
	 * generates the Tree stem
	 * @return the high of the Tree
	 */
	protected void genTreeStemm(Set<BlockPos> changedLog, LevelSimulatedRW worldIn, BlockPos start, int h, BoundingBox box, TreeConfiguration config, Random rand)
	{
		BlockPos.MutableBlockPos xyz = new BlockPos.MutableBlockPos();
		for(int i=0;i<h;i++)
		{
			double r = getRadius(i, h);
			for(int x=(int) -r;x<r;x++)
			{
				for(int z=(int) -r;z<r;z++)
				{
					if(x*x + z*z < r*r)
					{
						xyz.set(start).move(x,i,z);
//						setLogState(changedBlocks, worldIn, xyz, LOG, box);
						setLog(worldIn, rand, xyz, changedLog, box, config.trunkProvider);
					}
				}
			}
		}
	}
	
	protected abstract double getRadius(int i, int h);
	
	protected void setLog(LevelSimulatedRW w, Random rand, MutableBlockPos start, Set<BlockPos> changedLogs, BoundingBox box, BlockStateProvider wrappedConfig)
	{
		w.setBlock(start, wrappedConfig.getState(rand, start), 3);
		if(changedLogs!=null)
			changedLogs.add(start.immutable());
	}

	protected void setLeaf(LevelSimulatedRW w, Random rand, MutableBlockPos xyz, Set<BlockPos> changedLeaves, BoundingBox box, BlockStateProvider config)
	{
		w.setBlock(xyz, config.getState(rand, xyz), 3);
		if(changedLeaves!=null)
			changedLeaves.add(xyz.immutable());
	}

}
