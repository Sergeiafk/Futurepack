package futurepack.world.gen.feature;

import java.util.Random;

import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.tags.FluidTags;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.HugeMushroomBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;

public class BigMushroomFeature extends Feature<BigMushroomFeatureConfig>
{	
	public BigMushroomFeature(Codec<BigMushroomFeatureConfig> serialiser)
	{
		super(serialiser);
	}

	static int[][] pos = new int[89][];
	static
	{
		//pos[block] = {x,y,z, meta}
		pos[0]= new int[]{-4, 2,-2, 1};
		pos[1]= new int[]{-4, 2,-1, 4};
		pos[2]= new int[]{-4, 2, 0, 4};
		pos[3]= new int[]{-4, 2, 1, 4};
		pos[4]= new int[]{-4, 2, 2, 7};
		pos[5]= new int[]{-4, 3, 0, 11};
		pos[6]= new int[]{-3, 2,-3, 1};
		pos[7]= new int[]{-3, 2,-2, 0};
		pos[8]= new int[]{-3, 2, 2, 0};
		pos[9]= new int[]{-3, 2, 3, 7};
		pos[10]=new int[]{-3, 3,-2, 1};
		pos[11]=new int[]{-3, 3,-1, 4};
		pos[12]=new int[]{-3, 3, 0, 0};
		pos[13]=new int[]{-3, 3, 1, 4};
		pos[14]=new int[]{-3, 3, 2, 7};
		pos[15]=new int[]{-3, 4, 0, 11};
		pos[16]=new int[]{-2, 2,-4, 1};
		pos[17]=new int[]{-2, 2,-3, 0};
		pos[18]=new int[]{-2, 2, 3, 0};
		pos[19]=new int[]{-2, 2, 4, 7};
		pos[20]=new int[]{-2, 3,-3, 1};
		pos[21]=new int[]{-2, 3,-2, 0};
		pos[22]=new int[]{-2, 3, 2, 0};
		pos[23]=new int[]{-2, 3, 3, 7};
		pos[24]=new int[]{-2, 4,-2, 1};
		pos[25]=new int[]{-2, 4,-1, 4};
		pos[26]=new int[]{-2, 4, 0, 5};
		pos[27]=new int[]{-2, 4, 1, 4};
		pos[28]=new int[]{-2, 4, 2, 7};
		pos[29]=new int[]{-1, 2,-4, 2};
		pos[30]=new int[]{-1, 2, 4, 8};
		pos[31]=new int[]{-1, 3,-3, 2};
		pos[32]=new int[]{-1, 3, 3, 8};
		pos[33]=new int[]{-1, 4,-2, 2};
		pos[34]=new int[]{-1, 4,-1, 5};
		pos[35]=new int[]{-1, 4, 0, 5};
		pos[36]=new int[]{-1, 4, 1, 5};
		pos[37]=new int[]{-1, 4, 2, 8};
		pos[38]=new int[]{ 0, 2,-4, 2};
		pos[39]=new int[]{ 0, 2, 4, 8};
		pos[40]=new int[]{ 0, 3,-4, 11};
		pos[41]=new int[]{ 0, 3,-3, 0};
		pos[42]=new int[]{ 0, 3, 3, 0};
		pos[43]=new int[]{ 0, 3, 4, 11};
		pos[44]=new int[]{ 0, 4,-3, 11};
		pos[45]=new int[]{ 0, 4,-2, 5};
		pos[46]=new int[]{ 0, 4,-1, 5};
		pos[47]=new int[]{ 0, 4, 0, 5};
		pos[48]=new int[]{ 0, 4, 1, 5};
		pos[49]=new int[]{ 0, 4, 2, 5};
		pos[50]=new int[]{ 0, 4, 3, 11};
		pos[51]=new int[]{ 1, 2,-4, 2};
		pos[52]=new int[]{ 1, 2, 4, 8};
		pos[53]=new int[]{ 1, 3,-3, 2};
		pos[54]=new int[]{ 1, 3, 3, 8};
		pos[55]=new int[]{ 1, 4,-2, 2};
		pos[56]=new int[]{ 1, 4,-1, 5};
		pos[57]=new int[]{ 1, 4, 0, 5};
		pos[58]=new int[]{ 1, 4, 1, 5};
		pos[59]=new int[]{ 1, 4, 2, 8};
		pos[60]=new int[]{ 2, 2,-4, 3};
		pos[61]=new int[]{ 2, 2,-3, 0};
		pos[62]=new int[]{ 2, 2, 3, 0};
		pos[63]=new int[]{ 2, 2, 4, 9};
		pos[64]=new int[]{ 2, 3,-3, 3};
		pos[65]=new int[]{ 2, 3,-2, 0};
		pos[66]=new int[]{ 2, 3, 2, 0};
		pos[67]=new int[]{ 2, 3, 3, 9};
		pos[68]=new int[]{ 2, 4,-2, 3};
		pos[69]=new int[]{ 2, 4,-1, 6};
		pos[70]=new int[]{ 2, 4, 0, 5};
		pos[71]=new int[]{ 2, 4, 1, 6};
		pos[72]=new int[]{ 2, 4, 2, 9};
		pos[73]=new int[]{ 3, 2,-3, 3};
		pos[74]=new int[]{ 3, 2,-2, 0};
		pos[75]=new int[]{ 3, 2, 2, 0};
		pos[76]=new int[]{ 3, 2, 3, 9};
		pos[77]=new int[]{ 3, 3,-2, 3};
		pos[78]=new int[]{ 3, 3,-1, 6};
		pos[79]=new int[]{ 3, 3, 0, 0};
		pos[80]=new int[]{ 3, 3, 1, 6};
		pos[81]=new int[]{ 3, 3, 2, 9};
		pos[82]=new int[]{ 3, 4, 0, 11};
		pos[83]=new int[]{ 4, 2,-2, 3};
		pos[84]=new int[]{ 4, 2,-1, 6};
		pos[85]=new int[]{ 4, 2, 0, 6};
		pos[86]=new int[]{ 4, 2, 1, 6};
		pos[87]=new int[]{ 4, 2, 2, 9};
		pos[88]=new int[]{ 4, 3, 0, 11};
	}
	
	private BlockState getCapBlock(int meta, BlockState cap)
	{
		final BooleanProperty UP = HugeMushroomBlock.UP;
		final BooleanProperty DOWN = HugeMushroomBlock.DOWN;
		final BooleanProperty WEST = HugeMushroomBlock.WEST;
		final BooleanProperty NORTH = HugeMushroomBlock.NORTH;
		final BooleanProperty SOUTH = HugeMushroomBlock.SOUTH;
		final BooleanProperty EAST = HugeMushroomBlock.EAST;
		switch (meta)
		{
		case 0:
			return cap.setValue(DOWN, false).setValue(UP, false).setValue(NORTH, false).setValue(SOUTH, false).setValue(WEST, false).setValue(EAST, false);
		case 1:
			return cap.setValue(DOWN, false).setValue(UP, true).setValue(NORTH, true).setValue(SOUTH, false).setValue(WEST, true).setValue(EAST, false);
		case 2:
			return cap.setValue(DOWN, false).setValue(UP, true).setValue(NORTH, true).setValue(SOUTH, false).setValue(WEST, false).setValue(EAST, false);
		case 3:
			return cap.setValue(DOWN, false).setValue(UP, true).setValue(NORTH, true).setValue(SOUTH, false).setValue(WEST, true).setValue(EAST, true);
		case 4:
			return cap.setValue(DOWN, false).setValue(UP, true).setValue(NORTH, false).setValue(SOUTH, false).setValue(WEST, true).setValue(EAST, false);
		case 5:
			return cap.setValue(DOWN, false).setValue(UP, true).setValue(NORTH, false).setValue(SOUTH, false).setValue(WEST, false).setValue(EAST, false);
		case 6:
			return cap.setValue(DOWN, false).setValue(UP, true).setValue(NORTH, false).setValue(SOUTH, false).setValue(WEST, false).setValue(EAST, true);
		case 7:
			return cap.setValue(DOWN, false).setValue(UP, true).setValue(NORTH, false).setValue(SOUTH, true).setValue(WEST, true).setValue(EAST, false);
		case 8:
			return cap.setValue(DOWN, false).setValue(UP, true).setValue(NORTH, false).setValue(SOUTH, true).setValue(WEST, false).setValue(EAST, false);
		case 9:
			return cap.setValue(DOWN, false).setValue(UP, true).setValue(NORTH, false).setValue(SOUTH, true).setValue(WEST, false).setValue(EAST, true);
		case 11:
			return cap.setValue(DOWN, true).setValue(UP, true).setValue(NORTH, true).setValue(SOUTH, true).setValue(WEST, true).setValue(EAST, true);
		case 10:	
		default:
			return cap;
		}
	}

	@Override
	public boolean place(FeaturePlaceContext<BigMushroomFeatureConfig> context) 
	{
		Random r = context.random();
		WorldGenLevel w = context.level();
		BlockPos jkl = context.origin();
//		int h = 5 + r.nextInt(5);
		int h = context.config().getHeight(r);
		int x=0;
		int z=0;
		//for(int x=-1;x<2;x++)
		{
			//for(int z=-1;z<2;z++)
			{
				int i=-1;
				while(isAir(w, jkl.offset(x,i,z)))
				{
					setBlock(w, jkl.offset(x,i,z), context.config().stem);
					i--;
					if(jkl.getY()+i==0)
						break;
				}
				if(w.isStateAtPosition(jkl.offset(x,i,z), s -> s.getFluidState().is(FluidTags.WATER)))
				{
					setBlock(w, jkl.offset(x,i,z), context.config().stem);
					setBlock(w, jkl.offset(x+1,i,z), context.config().stem);
					setBlock(w, jkl.offset(x-1,i,z), context.config().stem);
					setBlock(w, jkl.offset(x,i,z-1), context.config().stem);
					setBlock(w, jkl.offset(x,i,z+1), context.config().stem);
				}
				
				for(i=0;i<h;i++)
				{	
					BlockPos p = jkl.offset(x,i,z).immutable();
					if(w instanceof LevelAccessor)
					{
//						System.out.println("From " + ((IWorld)w).getBlockState(p) + " at " + p);
					}
					setBlock(w, p, context.config().stem);
					if(w instanceof LevelAccessor)
					{
//						System.out.println("To " + ((IWorld)w).getBlockState(p) + " at " + p);
					}
//					System.out.println(p);
//					changedLogs.add(p);
//					box.expandTo(new MutableBoundingBox(p, p));
				}
			}
		}
		for(int i=0;i<pos.length;i++)
		{
			x = pos[i][0];
			int y = pos[i][1];
			z = pos[i][2];
			BlockPos bp = jkl.offset(x,y+h-4,z);
//			changedLeaves.add(bp);
//			box.expandTo(new MutableBoundingBox(bp, bp));
			if(isAir(w, bp))
				w.setBlock(bp, getCapBlock(pos[i][3], context.config().cap), 2);
		}
		
		return true;
	}
}
