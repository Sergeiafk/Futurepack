package futurepack.world.gen.feature;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;

public class MycelFeatureConfig implements FeatureConfiguration
{
	
	public static final Codec<MycelFeatureConfig> CODEC = RecordCodecBuilder.create((builder) -> {
	      return builder.group(BlockState.CODEC.fieldOf("state").forGetter((config) -> {
	         return config.mycel;
	      })).apply(builder, MycelFeatureConfig::new);
	   });
	
	public final BlockState mycel;
	
	public MycelFeatureConfig(BlockState mycel)
	{
		super();
		this.mycel = mycel;
	}

	public BlockState getMycel()
	{
		return mycel;
	}
}
