package futurepack.world.gen;

import java.util.Random;
import java.util.stream.Stream;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.WorldgenRandom;
import net.minecraft.world.level.levelgen.placement.PlacementContext;
import net.minecraft.world.level.levelgen.placement.PlacementModifier;
import net.minecraft.world.level.levelgen.placement.PlacementModifierType;

public class GroupPlacementAtSurface extends PlacementModifier
{
	public final float areaProbability, placeProbability;
	public final int areaSize;
	
	public static final Codec<GroupPlacementAtSurface> CODEC = RecordCodecBuilder.create((builder) -> {
	      return builder.group(
	    		  Codec.FLOAT.fieldOf("p_area").forGetter(c -> c.areaProbability),
	    		  Codec.INT.fieldOf("area_size").forGetter(c -> c.areaSize),
	    		  Codec.FLOAT.fieldOf("p_place").forGetter(c -> c.placeProbability)
	    		  ).apply(builder, GroupPlacementAtSurface::new);
		   });
		
	public GroupPlacementAtSurface(float areaProbability, int areaSize, float placeProbability) 
	{
		super();
		this.areaProbability = areaProbability;
		this.areaSize = areaSize;
		this.placeProbability = placeProbability;
	}
	
	@Override
	public Stream<BlockPos> getPositions(PlacementContext helper, Random random, BlockPos pos) 
	{
		int x = pos.getX() / areaSize;
		int z = pos.getZ() / areaSize;
        Random r = WorldgenRandom.seedSlimeChunk(x, z, helper.getLevel().getSeed(), 324657867L);
        
        if(r.nextFloat() < areaProbability)
        {
        	if(random.nextFloat() < placeProbability)
        	{
	        	int k = random.nextInt(16);
	            int l = random.nextInt(16);
	            return Stream.of(helper.getLevel().getHeightmapPos(Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, pos.offset(k, 0, l)));
        	}
        }
        
		return Stream.empty();
	}

	@Override
	public PlacementModifierType<?> type() 
	{
		return FPPlacementModifiers.GROUP_AT_SURFACE.get();
	}

}
