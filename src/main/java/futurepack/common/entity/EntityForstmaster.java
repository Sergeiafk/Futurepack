package futurepack.common.entity;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.stream.StreamSupport;

import com.google.common.base.Predicate;

import futurepack.api.FacingUtil;
import futurepack.common.FPEntitys;
import futurepack.common.FuturepackMain;
import futurepack.common.FuturepackTags;
import futurepack.common.block.inventory.TileEntityDroneStation;
import futurepack.common.block.misc.BlockSaplingHolder;
import futurepack.common.block.misc.TileEntityFallingTree;
import futurepack.common.block.misc.TileEntitySaplingHolder;
import futurepack.common.item.tools.ToolItems;
import futurepack.depend.api.helper.HelperInventory;
import futurepack.depend.api.helper.HelperInventory.SlotContent;
import futurepack.depend.api.helper.HelperMagnetism;
import futurepack.world.dimensions.TreeUtils;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;

public class EntityForstmaster extends EntityDrone implements Consumer<TileEntityFallingTree>
{
	private static final EntityDataAccessor<Byte> state = SynchedEntityData.defineId(EntityForstmaster.class, EntityDataSerializers.BYTE);
	private static final EntityDataAccessor<BlockPos> currentTarget = SynchedEntityData.defineId(EntityForstmaster.class, EntityDataSerializers.BLOCK_POS);
	
	private static final int magnetRange = 10;
	
	public static enum EnumState
	{
		STANDBY(0.08F, false),
		FELLING(1.2F, false),
		COLLECTING(0.8F, true),
		MOVING(0.5F, true),
		REFILLING(0.5F, true),
		;
		
		private float energyUse;
		private boolean moving;
		
		private EnumState(float neUse, boolean moving)
		{
			energyUse = neUse;
			this.moving = moving;
		}
		
		private EnumState setMoving()
		{
			moving = true;
			return this;
		}
		
		public byte ID()
		{
			return (byte) ordinal();
		}
		
		public float getEnergieUse()
		{
			return energyUse;
		}
		

		public static EnumState getState(Byte b)
		{
			return values()[b];
		}
		
		public boolean isMoving()
		{
			return moving;
		}
	}
	
	private Navigator navi;
	private ArrayList<WeakReference<Entity>> items;
	private HashMap<BlockPos, ItemStack> holderMap;
	private ItemStack transport =ItemStack.EMPTY;
	
	public EntityForstmaster(Level w)
	{
		this(FPEntitys.FORESTMASTER, w);
	}
	
	public EntityForstmaster(EntityType<EntityForstmaster> type, Level w)
	{
		super(type, w, 100);
		navi = new Navigator(this);
	}

	public EntityForstmaster(Level w, BlockPos pos)
	{
		this(w);
		setPos(pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5);
		setInventoryPos(pos);
	}

	@Override
	public AABB getBoundingBoxForCulling() 
	{
		return super.getBoundingBox();
	}
	
	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();
		this.entityData.define(state, EnumState.STANDBY.ID());//pogress
		this.entityData.define(currentTarget, new BlockPos(0,0,0));
	}
	
	
	@Override
	protected MovementEmission getMovementEmission()
	{
		return MovementEmission.NONE;
	}
	
	@Override
	public boolean isPickable() 
	{
		return !isAlive()==false;
	}
	
	@Override
	public boolean hurt(DamageSource ds, float par2) 
	{
		if(ds.getDirectEntity() != null)
		{
			if(!ds.getDirectEntity().hurt(ds, par2))
			{
				return ds.getDirectEntity().hurt(FuturepackMain.NEON_DAMAGE, par2);
			}
			return true;
		}
		return super.hurt(ds, par2);
	}
		
	@Override
	public InteractionResult interact(Player pl, InteractionHand hand)
	{
		ItemStack it = pl.getItemInHand(hand);
		if(it != null && it.getItem() == ToolItems.scrench && !level.isClientSide)
		{
			discard();
			ItemEntity ei = new ItemEntity(level,getX(),getY(),getZ(), new ItemStack(ToolItems.forstmasterbox));
			level.addFreshEntity(ei);
			return InteractionResult.SUCCESS;
		}
		return InteractionResult.PASS;
	}
	
	public int scanningProgress = 0;
	private BlockPos log = null;
	private int magnetShutdown = 0;
	
	@Override
	public void baseTick() 
	{
		super.baseTick();
		if(level.isClientSide)
		{
			if(getTarget()==null && !navi.isFinished())
			{
				navi.clear();
			}
			else if(getTarget()!=null)
			{
				navi.clear();
				navi.addTarget(getTarget());
			}
		}
		else
		{
			setTarget(navi.getTarget());
		}
		
		if(getState()!=EnumState.STANDBY)
		{
			
			if(getState().isMoving())
			{
				if(getState()==EnumState.COLLECTING)
				{
					//world.addParticle(ParticleTypes.FIREWORKS_SPARK, posX, posY+0.6, posZ, 0, 0, 0);
					HelperMagnetism.doMagnetism(level, blockPosition(), magnetRange, 0.2F);			
					collectItems();
				}
				
				if(!navi.isFinished())
				{
					if(items!=null)
					{
						clear(items);
						if(items.isEmpty())
						{
							if(magnetShutdown-- <= 0)
							{
								items=null;
								BlockPos pos = navi.getFinish();
								navi.clear();
								navi.addTarget(pos);
								setState(EnumState.MOVING);		
							}
						}	
						else
						{
							magnetShutdown = 40;
						}
					}
					
					destroyLeavesAround();
					navi.move(0.2F);
					
					if(getState()==EnumState.MOVING)
					{
						if(log!=null && level.isEmptyBlock(log)) //check if log got already removed
						{
							log = null;
							navi.nextTarget();
							return;
						}
						
						if(horizontalCollision && items==null) 
						{
							//try felling 
							Direction face = Direction.fromYRot(getYRot());
							BlockPos pos = this.blockPosition().relative(face, 1);
							BlockState state = level.getBlockState(pos);
							Predicate<BlockState> pred = TreeUtils.getLogPredicate();
							if(pred.apply(state))
							{
								log = pos;
								setState(EnumState.FELLING);
							}
							else
							{
								
							}
						}
						else if(items!=null)
						{
							clear(items);
							if(items.isEmpty())
							{
								items=null;
							}			
						}
						else if(verticalCollision && items!=null)
						{
							System.out.println("EntityForstmaster.baseTick() got stuck!");
						}
					}
				}
				else if(!level.isClientSide && getState()==EnumState.MOVING)
				{
					setState(EnumState.FELLING);
				}
				else if(!level.isClientSide && getState()==EnumState.COLLECTING)
				{
					if(items!=null)
					{
						clear(items);
						if(!items.isEmpty())
						{
							Entity e = items.get(0).get();
							if(e.isAlive())
							{
								BlockPos pp = e.blockPosition();
								if(pp.equals(new BlockPos(0,0,0)))
								{
									e.discard();
									items.remove(0);
								}
								else
								{
									Direction val = Direction.from3DDataValue(random.nextInt(6));
									navi.addTarget(pp.relative(val));
								}
							}
							
							return;
						}
						else
						{
							items = null;
						}				
					}
					else
					{
						setState(EnumState.STANDBY);		
					}
				}
				else if(!level.isClientSide && getState()==EnumState.REFILLING)
				{
					if(!transport.isEmpty())
					{
						BlockPos pos = this.blockPosition().below();
						BlockEntity tile = level.getBlockEntity(pos);
						IItemHandler handler = HelperInventory.getHandler(tile, Direction.UP);
						ItemStack left = ItemStack.EMPTY;
						if(handler!=null)//put sapling in sapling holder
						{
							left = ItemHandlerHelper.insertItem(handler, transport, false);
							transport=ItemStack.EMPTY;
						}
						else
						{
							left = transport;
							transport=ItemStack.EMPTY;;
						}
						
						if(!left.isEmpty()) //throw sapling away 'cause it does not fit
						{
							ItemEntity item = new ItemEntity(level, getX(), getY(), getZ(), left);
							level.addFreshEntity(item);
						}
					}
					else if(this.getDistanceSqToCenter(getInventoryPos())>2) //move back to station
					{
						navi.addTarget(getInventoryPos());//TODO: go back to claime center
					}
					else //fill in next sapling for sapling holder
					{
						if(!getNeededItem())
						{
							setState(EnumState.STANDBY);
						}
					}
				}
			}
			else if(getState()==EnumState.FELLING && !level.isClientSide)
			{
				if(log==null)
				{
					setState(EnumState.STANDBY);
					return;
				}
				
				addMiningProgress();
				if(getMiningProgress()>20)
				{									
//					navi.calcRotaion(this.getPositionVec().subtract(new Vector3d(log)));   	
					Direction face = Direction.fromYRot(getYRot());
//					BlockPos pos = this.getPosition().offset(face, -1);
//					setPositionAndUpdate(pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5);	
					resetMiningProgress();
					if(this.getDistanceSqToCenter(log)<5)
					{		
						BlockPos pos = TreeUtils.getFarWoodInHeight(level, log);
						if(pos.equals(log))
						{
							TreeUtils.selectTree(level, log, face).listeners.add(this);
							setState(EnumState.STANDBY);
						
							scanningProgress = -20;
						}
						else
						{
							level.destroyBlock(pos, true);
						}
					}
					else
					{
						setState(EnumState.MOVING);
						Direction val = Direction.from3DDataValue(random.nextInt(6));
						navi.addTarget(log.relative(val));
					}
				}
				
			}
		}
		else
		{
			scanningProgress++;
			
			if(scanningProgress > 60)
			{
				scanningProgress = 0;
				scanForNearbyWood();
				
				BlockPos inv = getInventoryPos();//TODO: go back to claime center
				if(getState()==EnumState.STANDBY && this.getDistanceSqToCenter(inv)>2)
				{
					if(StreamSupport.stream(level.getBlockCollisions(null, new AABB(inv)).spliterator(), false).filter(v -> !v.isEmpty()).findAny().isPresent())//getCollisionShapes
					{
						level.destroyBlock(inv, true);
					}
					navi.addTarget(inv);
					setState(EnumState.MOVING);
				}
				
				if(!level.isClientSide && getState() == EnumState.STANDBY)
				{
					findSaplingHolder();
				}
			}				
		}
	}
	
	private double getDistanceSqToCenter(BlockPos inventoryPos) 
	{
		return distanceToSqr(inventoryPos.getX()+0.5, inventoryPos.getY()+0.5, inventoryPos.getZ()+0.5);
	}

	@Override
	public void tick()
	{			
		if(getPower()<getMaxPower())
		{
			tryCharge();
		}
		if(consumePower())
		{
			super.tick();
		}
		
		if(level.isClientSide)
		{	
			if (getState()== EnumState.STANDBY && getPower()>1)
			{
				if(tickCount%12==0)
					level.addParticle(ParticleTypes.HEART, getX(), getY()+0.6, getZ(), 0, 0, 0);
			}
		}
	}	
	
	private void scanForNearbyWood()
	{
		Predicate<BlockState> isLog = TreeUtils.getLogPredicate();
		Predicate<BlockState> isShroom = TreeUtils.getMushroomPredicate();
		log = null;
		BlockPos start = this.blockPosition();
		Y:
		for(int y=-10;y<10;y++)//TODO: scan whole claime but in parts, if nothing was found setDone(true) and move to the next claime, alias the center as claime should update
		{
			for(int x=-10;x<10;x++)
			{
				for(int z=-10;z<10;z++)
				{
					BlockPos pos = start.offset(x,y,z);
					
					if(!isInArea(pos))
						continue;
					
					BlockState state = level.getBlockState(pos);
					if(isShroom.apply(state))
					{
						while(isShroom.apply(state))
						{
							pos = pos.below();
							state = level.getBlockState(pos);
						}
						log = pos.above();
						break Y;
					}
					else if(isLog.apply(state))
					{
						while(isLog.apply(state))
						{
							pos = pos.below();
							state = level.getBlockState(pos);
						}
						log = pos.above();
						break Y;
					}
				}
			}
		}
		
		if(log!=null)
		{
			double dis = 200D;
			BlockPos min = log;
			for(Direction face : FacingUtil.HORIZONTAL)
			{
				double d = this.getDistanceSqToCenter(log.relative(face));
				if(d<dis)
				{
					dis = d;
					min = log.relative(face);
				}
			}
			navi.addTarget(min);
			setState(EnumState.MOVING);
		}
	}
	
	private void collectItems()
	{
		if(level.isClientSide)
			return;
		
		AABB bb = getBoundingBox().inflate(1, 1, 1);
		List<ItemEntity> item = level.getEntitiesOfClass(ItemEntity.class, bb, new Predicate<ItemEntity>() //getEntitiesWithinAABBExcludingEntity
		{	
			@Override
				public boolean apply(ItemEntity e)
				{
					if(e.isAlive()==false)
						return false;					
					
					return true;
				}
		});	
		if(item.isEmpty())
			return;
		
		final ArrayList<SlotContent> items = new ArrayList<SlotContent>(item.size());
		for(ItemEntity e : item)
		{
			if(e.isAlive() && !e.getItem().isEmpty())//there was an itemstack with an empty item...
			{
				e.setPos(EntityForstmaster.this.getX(), EntityForstmaster.this.getY(), EntityForstmaster.this.getZ());
				items.add(new SlotContent(null, 0, e.getItem(), e));
			}
		}
		
		ArrayList<SlotContent> done = (ArrayList<SlotContent>) HelperInventory.insertItems(getDroneStattion(), getSide(), items);
		for(SlotContent slot : done)
		{
			slot.remove();
		}
	}
	
	private void destroyLeavesAround()
	{
		BlockPos start = this.blockPosition().offset(1,1,1);
		BlockPos end = this.blockPosition().offset(-1,-1,-1);
		Predicate<BlockState> isLeave = TreeUtils.getLeavesPredicate();
		for(BlockPos pos : BlockPos.betweenClosed(start, end))
		{
			BlockState state = level.getBlockState(pos);
			if(isLeave.apply(state))
			{
				level.destroyBlock(pos, true);
			}
		}
	}
	
	private boolean findSaplingHolder()
	{
		holderMap = new HashMap<BlockPos, ItemStack>();
		BlockPos start = this.blockPosition();
		for(int y=-10;y<10;y++)
		{
			for(int x=-10;x<10;x++)
			{
				for(int z=-10;z<10;z++)
				{
					BlockPos pos = start.offset(x,y,z);
					BlockState state = level.getBlockState(pos);
					if(state.is(FuturepackTags.SAPLING_HOLDER) && level.isEmptyBlock(pos.above()))
					{
						TileEntitySaplingHolder holder = (TileEntitySaplingHolder) level.getBlockEntity(pos);
						
						LazyOptional<IItemHandler> opt = holder.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, Direction.UP);
						opt.ifPresent(handler -> {
							if(handler.getStackInSlot(0).isEmpty())
							{
								if(holder.getFilter()!=null)
									holderMap.put(pos, holder.getFilter());				
							}
						});
						
					}
				}
			}
		}
		if(!holderMap.isEmpty())
		{
			setState(EnumState.REFILLING);
			return true;
		}
		else
		{
			holderMap = null;
			return false;
		}
	}
	
	private boolean getNeededItem()
	{
		if(holderMap==null || holderMap.isEmpty())
		{
			setState(EnumState.STANDBY);
		}
		else
		{
			TileEntityDroneStation tile = getDroneStattion();
			IItemHandler handler = HelperInventory.getHandler(tile, getSide());
			if(handler==null)
				return false;
			
			Iterator<Entry<BlockPos, ItemStack>> iter = holderMap.entrySet().iterator();
			while(iter.hasNext())
			{
				Entry<BlockPos, ItemStack> e = iter.next();
				TileEntitySaplingHolder holder = (TileEntitySaplingHolder) level.getBlockEntity(e.getKey());
				if(holder==null)
				{
					iter.remove();
					continue;
				}
				LazyOptional<IItemHandler> holderInv = holder.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, Direction.UP);
				if(holderInv.map(h -> !h.getStackInSlot(0).isEmpty()).orElse(true))
				{
					iter.remove();
					continue;
				}
						
				
				for(int i=0;i<handler.getSlots();i++)
				{
					ItemStack it = handler.getStackInSlot(i);
					if(it.isEmpty())
						continue;
					boolean extract = false;
					
					if(e.getValue()==null)
					{
						if(BlockSaplingHolder.isSapling(it))
						{
							extract=true;
						}
					}
					else
					{
						if(HelperInventory.areItemsEqualNoSize(it, e.getValue()))
						{
							extract=true;
						}
					}
					
					if(extract)
					{
						transport = handler.extractItem(i, 1, false);
						if(!transport.isEmpty())
						{
							iter.remove();
							navi.addTarget(e.getKey().above());
							return true;
						}
					}
				}	
			}		
		}
		return false;
	}
	
	@Override
	protected float getEnergieUse()
	{
		return getState().getEnergieUse();
	}
	
	public EnumState getState()
	{
		return EnumState.getState(entityData.get(state));
	}

	private void setState(EnumState state)
	{
		entityData.set(EntityForstmaster.state, state.ID());
	}
	
	/**
	 * This sets the next Target
	 * @param pos the Position this Entity is trying to reach next.
	 */
	private void setTarget(BlockPos pos)
	{
		if(pos==null)
			pos = new BlockPos(0,0,0);
		entityData.set(EntityForstmaster.currentTarget, pos);
	}
	
	/**
	 * THis is for client syncronisation, becasue the pathfinding runs only on the server
	 * @return the Position this Entity is trying to reach next.
	 */
	private BlockPos getTarget()
	{
		if(!level.isClientSide)
		{
			throw new IllegalAccessError("Dont access this from Server side, use the Naviagtor on the Server.");
		}
		BlockPos pos = entityData.get(EntityForstmaster.currentTarget);
		if(pos.getX()==0 && pos.getY()==0 && pos.getZ()==0)
			pos = null;
		return pos;
	}
	
	@Override
	public void accept(TileEntityFallingTree input)
	{
		
		this.setState(EnumState.COLLECTING);
		this.items = input.spawned;
		
		
		if(!items.isEmpty())
		{
			navi.addTarget(new BlockPos(items.get(items.size()-1).get().blockPosition()));
		}
	}
	
//	/**
//	 * p1 = { 0, 0, 0}
//	 * p2 = {10, 0, 1}
//	 * 
//	 * @param pos1
//	 * @param pos2
//	 * @return
//	 */
//	private List<BlockPos> line(BlockPos pos1, BlockPos pos2)
//	{
//		int dx = pos2.getX() -pos1.getX();
//		int dz = pos2.getZ() -pos1.getZ();
//		
//		int max = Math.max(dx, dz);
//		ArrayList<BlockPos> poss = new ArrayList<BlockPos>(max);
//		
//		for(int i=0;i<max;i++)
//		{
//			int x = (int) (((double)i/(double)max) * (double)dx);
//			int z = (int) (((double)i/(double)max) * (double)dz);
//			BlockPos pos = pos1.add(x,0,z);
//			
//			while(!worldObj.isAirBlock(pos))
//			{
//				pos = pos.up();
//			}
//			if(worldObj instanceof WorldServer)
//			{
//				WorldServer serv = (WorldServer) worldObj;
//				serv.addParticle(ParticleTypes.NOTE, true, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, 1, 0D, 0D, 0D, 1D);
//			}
//			poss.add(pos);
//		}
//		
//		return poss;
//	}
	
	private void clear(ArrayList<WeakReference<Entity>> list)
	{
		Iterator<WeakReference<Entity>> iter = list.iterator();
		while(iter.hasNext())
		{
			Entity ref = iter.next().get();
			if(ref==null || ref.isAlive()==false)
			{
				iter.remove();
			}
		}
		list=null;
	}
	
//	//@ TODO: OnlyIn(Dist.CLIENT)
//	public int currentLayer;
//	
//	@Override
//	public boolean shouldRenderInPass(int pass)
//	{
//		currentLayer = pass;
//		return pass == 0 || pass == 1;
//	}
//	
	@Override
	public boolean startRiding(Entity entityIn)
	{
		return false;
	}

	@Override
	protected float getMiningProgressModifier() 
	{
		return 1;
	}
}
