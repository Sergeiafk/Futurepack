package futurepack.common.entity.throwable;

import futurepack.common.FPEntitys;
import net.minecraft.core.BlockPos;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.projectile.ThrowableProjectile;
import net.minecraft.world.level.Explosion;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.HitResult.Type;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.network.NetworkHooks;

public class EntityLaser extends ThrowableProjectile
{
	private static EntityDataAccessor<Boolean> critical = SynchedEntityData.defineId(EntityLaser.class, EntityDataSerializers.BOOLEAN);
	private static EntityDataAccessor<Byte> explPower = SynchedEntityData.defineId(EntityLaser.class, EntityDataSerializers.BYTE);

	private float power;
	
	public EntityLaser(Level w)
	{
		super(FPEntitys.LASER, w);
	}
	
	public EntityLaser(EntityType<EntityLaser> type, Level w)
	{
		super(type, w);
	}
	
	@Override
	protected void defineSynchedData() 
	{
		this.entityData.define(critical, false);
		this.entityData.define(explPower, (byte)0);
	}

	public void setIsCritical(boolean b)
    {
		 this.entityData.set(critical, b);
    }

    public boolean getIsCritical()
    {
    	return this.entityData.get(critical);
    }
	
    public void setExplosionPower(byte b)
    {
    	this.entityData.set(explPower, b);
    }
    
    private byte getExplosionPower()
    {
    	return this.entityData.get(explPower);
    }
    
	public EntityLaser(Level w, LivingEntity base, float power)
	{
		super(FPEntitys.LASER, base, w);
//		this.getMotion().x *= 2;
//		this.getMotion().y *= 2;
//		this.getMotion().z *= 2;
		
		this.power = power;
	}

//	@Override
//	public void setThrowableHeading(double x, double y, double z, float velocity, float inaccuracy)
//    {
//        x *= (double)velocity;
//        y *= (double)velocity;
//        z *= (double)velocity;
//        this.getMotion().x = x;
//        this.getMotion().y = y;
//        this.getMotion().z = z;
//    }
	
	@Override
	protected void onHit(HitResult mov)
	{
		if(level.isClientSide)
			return;
			
		if(mov.getType() == Type.BLOCK)
		{
			BlockPos pos = new BlockPos(mov.getLocation());
			BlockState b = level.getBlockState(pos);
			if(!b.isSolidRender(level, pos))
			{
				return;
			}
		}
		else if(mov.getType() == Type.ENTITY)
		{
			if(((EntityHitResult)mov).getEntity() == getOwner() && tickCount<20)
				return;
			
			((EntityHitResult)mov).getEntity().hurt(DamageSource.mobAttack((LivingEntity) getOwner()), 10 * power);
			((EntityHitResult)mov).getEntity().setSecondsOnFire(1);
		}
		float p = getIsCritical()?0.2F : 0F;
		p += getExplosionPower();
	
		if(p>0)
			level.explode(getOwner(), mov.getLocation().x, mov.getLocation().y, mov.getLocation().z, p, false, Explosion.BlockInteraction.NONE);
		
		discard();
	}

	@Override
	public void tick() 
	{
		Vec3 mot = this.getDeltaMovement();
		super.tick();		
		this.setDeltaMovement(mot);

	}
	
	@Override
	public Packet<?> getAddEntityPacket() 
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}
