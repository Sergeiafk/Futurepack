package futurepack.common.entity.living;

import java.util.Random;

import javax.annotation.Nullable;

import futurepack.common.FPEntitys;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.animal.Sheep;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;

public class EntityWolba extends Sheep
{
	public EntityWolba(EntityType<EntityWolba> type, Level w)
	{
		super(type, w);
	}
	
	public EntityWolba(Level w)
	{
		this(FPEntitys.WOLBA, w);
	}
	
	@Override
	public EntityType<?> getType() 
	{
		return FPEntitys.WOLBA;
	}
	
	@Override
	public EntityWolba getBreedOffspring(ServerLevel world, AgeableMob p_90011_1_)
	{
		return new EntityWolba(world);
	}
	
	@Nullable
	@Override
    public SpawnGroupData finalizeSpawn(ServerLevelAccessor worldIn, DifficultyInstance difficultyIn, MobSpawnType reason, @Nullable SpawnGroupData spawnDataIn, @Nullable CompoundTag dataTag)
    {
		spawnDataIn =  super.finalizeSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
        this.setColor(EntityWolba.getRandomSheepColor(this.level.random));
        return spawnDataIn;
    }
	
	public static DyeColor getRandomSheepColor(Random random)
    {
        int i = random.nextInt(100);
        return i < 7 ? DyeColor.BROWN : (i < 12 ? DyeColor.GRAY : (i < 17 ? DyeColor.YELLOW : (i < 20 ? DyeColor.BLACK : (random.nextInt(500) == 0 ? DyeColor.LIME : (random.nextInt(500) == 0 ? DyeColor.LIGHT_BLUE : DyeColor.ORANGE)))));
    }
}
