package futurepack.common.entity.living;

import javax.annotation.Nullable;

import futurepack.api.Constants;
import futurepack.common.FPEntitys;
import futurepack.common.entity.ai.AIEatFireflyes;
import futurepack.common.entity.ai.AIFindFireflyes;
import futurepack.common.entity.ai.AIFindWater;
import futurepack.common.entity.ai.AILookAround;
import futurepack.common.entity.ai.AIRandomFly;
import futurepack.common.entity.ai.AIRestOnWater;
import futurepack.common.entity.ai.HeulerMoveHelper;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MoverType;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier.Builder;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.BreedGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;

public class EntityHeuler extends Animal
{

	public EntityHeuler(Level worldIn)
	{
		this(FPEntitys.HEULER, worldIn);
	}
	
	public EntityHeuler(EntityType<EntityHeuler> type, Level worldIn)
	{
		super(type, worldIn);
		this.moveControl = new HeulerMoveHelper(this);
	}
	
	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new BreedGoal(this, 2.0F));
		
		this.goalSelector.addGoal(1, new AIEatFireflyes(this));
		
		this.goalSelector.addGoal(2, new AIRestOnWater(this));
		
		this.goalSelector.addGoal(3, new AIFindFireflyes(this));
		this.goalSelector.addGoal(3, new AIFindWater(this));
		this.goalSelector.addGoal(5, new AIRandomFly(this));
		this.goalSelector.addGoal(7, new AILookAround(this));
		
		this.goalSelector.addGoal(8, new LookAtPlayerGoal(this, Player.class, 8.0F));
		this.goalSelector.addGoal(8, new RandomLookAroundGoal(this));
	}

	public static Builder registerAttributes()
	{
		return Animal.createMobAttributes().add(Attributes.MAX_HEALTH, 5.0).add(Attributes.MOVEMENT_SPEED, 0.20D );
	}

	@Override
	public AgeableMob getBreedOffspring(ServerLevel world, AgeableMob ageable)
	{
		return new EntityHeuler(world);
	}
	
	@Override
	protected void ageBoundaryReached()
	{
	
		super.ageBoundaryReached();
	}
	
	@Override
	public boolean isFood(ItemStack stack)
	{
		return false;
	}
	
	@Override
	protected MovementEmission getMovementEmission()
	{
		return MovementEmission.NONE;
	}
	
	@Override
	public boolean canBeLeashed(Player player)
    {
        return false;
    }

	@Override
	public void aiStep()
	{
		
//		if (!this.onGround && this.getMotion().y < 0.0D)
//		{
//		   this.getMotion().y *= 0.6D;
//		}

		super.aiStep();
	}
	
	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntityDimensions sizeIn) 
	{
		return 0.25F;
	}
	
	/////////////////////////////////////
	// From EntityFLying
	////////////////////////////////////
	
	@Override
	public boolean causeFallDamage(float distance, float damageMultiplier, DamageSource source)
	{
		//FIXME: Ka ob das stimmt
		return false;
	}
	
	@Override
	protected void checkFallDamage(double y, boolean onGroundIn, BlockState state, BlockPos pos)
	{
	}
	

	@Override
	public void travel(Vec3 motion) 
	{
		if (this.isInWater()) 
		{
			this.moveRelative(0.02F, motion);
			this.move(MoverType.SELF, this.getDeltaMovement());
			this.setDeltaMovement(this.getDeltaMovement().scale(0.8F));
		}
		else if (this.isInLava()) 
		{
			this.moveRelative(0.02F, motion);
			this.move(MoverType.SELF, this.getDeltaMovement());
			this.setDeltaMovement(this.getDeltaMovement().scale(0.5D));
		}
		else
		{
			BlockPos ground = new BlockPos(this.getX(), this.getBoundingBox().minY - 1.0D, this.getZ());
			float f = 0.91F;
			if (this.onGround) 
			{
				f = this.level.getBlockState(ground).getBlock().getFriction() * 0.91F;
			}

			float f1 = 0.16277137F / (f * f * f);
			f = 0.91F;
			if (this.onGround) 
			{
				f = this.level.getBlockState(ground).getBlock().getFriction() * 0.91F;
			}

			this.moveRelative(this.onGround ? 0.1F * f1 : 0.02F, motion);
			this.move(MoverType.SELF, this.getDeltaMovement());
			this.setDeltaMovement(this.getDeltaMovement().scale(f));
		}

		this.animationSpeedOld = this.animationSpeed;
		double d1 = this.getX() - this.xo;
		double d0 = this.getZ() - this.zo;
		double f2 = Math.sqrt(d1 * d1 + d0 * d0) * 4.0F;
		if (f2 > 1.0F) 
		{
			f2 = 1.0F;
		}

		this.animationSpeed += (f2 - this.animationSpeed) * 0.4F;
		this.animationPosition += this.animationSpeed;
	}

	@Override
	public boolean onClimbable()
	{
		return false;
	}

	@Override
	@Nullable
    protected ResourceLocation getDefaultLootTable()
    {
        return new ResourceLocation(Constants.MOD_ID, "entities/heuler");
    }
}
