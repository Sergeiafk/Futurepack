package futurepack.common.entity.drones;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;

public interface IWorker 
{
	/**
	 * 
	 * @return Current Enity Position
	 */
	BlockPos getPosition();
	
	/**
	 * Try to move to BlockPos, teleport if walking failed
	 * @param target
	 * @return is moving possible
	 */
	boolean moveOrTeleport(BlockPos target);
	
	
	boolean isFinised();
	
	Level getWorld();

}
