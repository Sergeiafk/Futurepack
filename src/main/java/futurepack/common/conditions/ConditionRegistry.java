package futurepack.common.conditions;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BooleanSupplier;
import java.util.function.Function;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import futurepack.common.FPLog;
import net.minecraft.resources.ResourceLocation;

public class ConditionRegistry
{
	private static Map<ResourceLocation, CachedCondition> map = Collections.synchronizedMap(new HashMap<>());
	private static Map<String, Function<JsonElement, BooleanSupplier>> conditionFactories = new HashMap<>();
	
	public static final BooleanSupplier ALWAYS_TRUE = () -> true;
	public static final String FOLDER_NAME = "fp_conditions";
	
	public static AtomicBoolean LOADING = new AtomicBoolean(false);
	
	//code done: 
	// - recipes: zentrifuge, assembly, crusher, ind neon furn, ind furnc, recylcer[ laser, shredder, time]
	// - researches
	// - planets 
	
	static
	{
		register("or", OrCondition::fromJson);
		register("and", AndCondition::fromJson);
		register("registry", RegistryEntryCondition::fromJson);
		register("tag", TagCondition::fromJson);
		register("mod", ModCondition::fromJson);
	}
	
	public static void register(String name, Function<JsonElement, BooleanSupplier> factory)
	{
		conditionFactories.put(name, factory);
	}
	
	public static void init()
	{
		map.clear();
		LOADING.set(true);
	}
	
	public static void loadingFolderFinishedCallback(String folder)
	{
		if(folder == FOLDER_NAME || FOLDER_NAME.equals(folder))
		{
			LOADING.set(false);
		}
	}
	
	public static void readJson(ResourceLocation path, JsonObject obj)
	{
		map.put(new ResourceLocation(path.getNamespace(), path.getPath().replaceFirst(FOLDER_NAME+"/", "")), cache(load(obj)));
	}
	
	public static BooleanSupplier load(JsonElement elm)
	{
		if(elm.isJsonPrimitive())
		{
			ResourceLocation conditionKey = new ResourceLocation(elm.getAsString());
			if(LOADING.get())
			{
				return cache(() -> {
					BooleanSupplier sup = map.get(conditionKey);
					while(LOADING.get() && (sup = map.get(conditionKey)) == null)
					{
						try
						{
							Thread.sleep(100);
						} catch (InterruptedException e)
						{
							e.printStackTrace();
						}
					}
					if(sup==null)
					{
						FPLog.logger.warn("Unknown Condition %s, return always true condition", conditionKey);
						return true;
					}
					else
					{
						return sup.getAsBoolean();
					}
				});
			}
			else
			{
				BooleanSupplier sup = map.get(conditionKey);
				if(sup==null)
				{
					FPLog.logger.warn("Unknown Condition %s, return always true condition", conditionKey);
					return ALWAYS_TRUE;
				}
				else
				{
					return sup;
				}
			}
		}
		else if(elm.isJsonObject())
		{
			return load(elm.getAsJsonObject());
		}
		else
		{
			throw new IllegalArgumentException(elm.toString());
		}
	}
	
	
	public static BooleanSupplier load(JsonObject jobj)
	{
		assert jobj.has("type");
		assert jobj.has("data");
		
		return getFacotry(jobj.get("type").getAsString()).apply(jobj.get("data"));
	}
	
	private static Function<JsonElement, BooleanSupplier> getFacotry(String type)
	{
		return conditionFactories.computeIfAbsent(type, type_key -> {
			FPLog.logger.error("Condition type %s was nto found, using always true returning dummy instead!", type_key);
			return json -> {
				FPLog.logger.warn("[Conditon] Always true dummy got data " + json);
				return ALWAYS_TRUE;
			};
		});
	}
	
	public static BooleanSupplier[] load(JsonArray jarr)
	{
		BooleanSupplier[] conditions = new BooleanSupplier[jarr.size()];
		for(int i=0;i<jarr.size();i++)
		{
			conditions[i] = load(jarr.get(i).getAsJsonObject());
		}
		return conditions;
	}
	
	public static CachedCondition cache(BooleanSupplier condition)
	{
		return new CachedCondition(condition);
	}
	
	public static boolean checkCondition(JsonObject recipe)
	{
		if(recipe.has("condition"))
		{
			return load(recipe.get("condition")).getAsBoolean();	
		}
		else
		{
			return true;
		}
	}
}
