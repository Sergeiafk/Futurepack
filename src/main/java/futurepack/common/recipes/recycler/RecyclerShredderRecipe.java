package futurepack.common.recipes.recycler;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import futurepack.api.ItemPredicateBase;
import futurepack.api.interfaces.IRecyclerRecipe;
import futurepack.common.item.misc.MiscItems;
import futurepack.common.recipes.EnumRecipeSync;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;

public class RecyclerShredderRecipe implements IRecyclerRecipe 
{
	private ItemStack[] output;
	private float[] probabilities;
	private ItemPredicateBase input;
	private int maxprogress;
		
	@Override
	public ItemStack[] getMaxOutput() 
	{
		return output;
	}
	
	public ArrayList<ItemStack> getWeightedOutput(Random rand) 
	{
		ArrayList<ItemStack> res = new ArrayList<ItemStack>();
		
		for(int s = 0; s < output.length; s++)
		{
			ItemStack o = output[s];
			float rate = s < probabilities.length ? probabilities[s] : 0;
			if(o != null)
			{
				int size = 0;
				for(int i = 0; i < o.getCount(); i++)
				{
					float f = rand.nextFloat();
					if(f <= rate)
					{
						size ++;
					}
				}
				if(size > 0)
				{
					ItemStack it = o.copy();
					it.setCount(size);
					res.add(it);
				}
			}
		}
		
		return res;
	}

	@Override
	public ItemPredicateBase getInput()
	{
		return input;
	}
		
	public int getMaxprogress() 
	{
		return maxprogress;
	}
		
	public RecyclerShredderRecipe(ItemPredicateBase i, ItemStack[] o, int time, float ...prob)
	{
		input = i;
		
		for(int j = 0; j < o.length; j++)
		{
			if(o[j] == null)
				o[j] = ItemStack.EMPTY;
		}
		
		output = o;
		maxprogress = time;
		probabilities = prob;
	}

	public boolean match(ItemStack in) 
	{
		if(in!=null)
		{
			return input.apply(in, false);
		}
		return false;
	}
	
	@Override
	public List<ItemStack> getToolItemStacks() 
	{
		return Collections.singletonList(new ItemStack(MiscItems.shredder));
	}

	@Override
	public float[] getChances() 
	{
		return probabilities;
	}

	@Override
	public String getJeiInfoText() 
	{
		return "";
	}	
		
	public void write(FriendlyByteBuf buf)
	{
		buf.writeVarInt(output.length);
		for(ItemStack o : output)
		{
			buf.writeItem(o);
		}
		buf.writeVarInt(probabilities.length);
		for(float f : probabilities)
		{
			buf.writeFloat(f);
		}
		EnumRecipeSync.writeUnknown(input, buf);
		buf.writeVarInt(maxprogress);
	}
	
	public static RecyclerShredderRecipe read(FriendlyByteBuf buf)
	{
		ItemStack[] out = new ItemStack[buf.readVarInt()];
		for(int i=0;i<out.length;i++)
		{
			out[i] = buf.readItem();
		}
		float[] probs = new float[buf.readVarInt()];
		for(int i=0;i<probs.length;i++)
		{
			probs[i] = buf.readFloat();
		}
		
		return new RecyclerShredderRecipe((ItemPredicateBase) EnumRecipeSync.readUnknown(buf), out, buf.readVarInt(), probs);
	}
}
