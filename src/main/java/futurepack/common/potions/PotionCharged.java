package futurepack.common.potions;

import java.util.List;

import futurepack.api.interfaces.IItemNeon;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectCategory;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;

public class PotionCharged extends MobEffect
{

	protected PotionCharged()
	{
		super(MobEffectCategory.NEUTRAL, 0x049eff);
		//this.registerPotionAttributeModifier(Attributes.movementSpeed, "7107DE5E-7CE8-4030-940E-514C1F160890", -0.15000000596046448D, 2);
	}

	@Override
	public void applyEffectTick(LivingEntity liv, int lvl)
	{
		super.applyEffectTick(liv, lvl);
		
		if(!liv.level.isClientSide && liv.isAlive())
		{
			//charges all items
			for(ItemStack it : liv.getAllSlots())
			{
				if(!it.isEmpty() && it.getItem() instanceof IItemNeon)
				{
					IItemNeon ch = (IItemNeon) it.getItem();
						
					if(ch.isNeonable(it))
					{
						int diff = ch.getMaxNeon(it)-ch.getNeon(it);
						if(diff>0)
						{
							int neon = Math.min(lvl, diff);
							ch.addNeon(it, neon);
						}								
					}		
				}
			}
			
			
			MobEffectInstance potion = liv.getEffect(this);
				
			if(lvl>0)
				return;
			List<LivingEntity> otherLiving = liv.level.getEntitiesOfClass(LivingEntity.class, liv.getBoundingBox().inflate((lvl+1)*3), l -> l!=liv);
			for(LivingEntity other : otherLiving)
			{
				if(liv==other)
					continue;
				
				if(liv.getRandom().nextFloat() < 0.005F + lvl*0.02F)
				{
					if(other.hurt(DamageSource.mobAttack(liv), lvl))
					{
						((ServerLevel)liv.level).playSound(null, other, SoundEvents.CAT_HISS, SoundSource.HOSTILE, 0.5F, 3F);
						((ServerLevel)liv.level).sendParticles(ParticleTypes.ANGRY_VILLAGER.getType(), other.getX(), other.getY()+other.getEyeHeight(), other.getZ(), 2+lvl, 1, 1, 1, 0);
						((ServerLevel)liv.level).sendParticles(ParticleTypes.ANGRY_VILLAGER.getType(), liv.getX(), liv.getY()+liv.getEyeHeight(), liv.getZ(), 2+lvl, 0, 0.1, 0, 0.1);
						if(liv.getRandom().nextFloat() < 0.3+lvl*0.2)
						{
							other.addEffect(new MobEffectInstance(this, potion.getDuration()/2, potion.getAmplifier()), liv);
							
						}
					}
				}
			}
		}
	}
}
