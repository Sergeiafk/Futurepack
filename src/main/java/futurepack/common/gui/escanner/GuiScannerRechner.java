package futurepack.common.gui.escanner;

import org.lwjgl.glfw.GLFW;

import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.gui.screens.Screen;

public class GuiScannerRechner extends GuiScannerBase
{
	//Maximal fl�sche 120 x 142
	// 105 x 140
	
	public GuiScannerRechner(Screen befor)
	{
		super(befor);
	}


	int guiX, guiY;
	
	double storedNum = 0;
	String Num = "0";
	// 1 +; 2 - ; 3 * ; 4 / ;
	int operation = -1;
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
	{	
		//Buttons
		super.render(matrixStack, mouseX, mouseY, partialTicks);
		
		font.draw(matrixStack, Num, guiX + 173 - font.width(Num), guiY + 101, 0x000000);
		String[] s = new String[]{"+","-","*","/"};
		if(operation>0)
		{
			font.draw(matrixStack,  s[operation-1]+storedNum , guiX + 173 - font.width(s[operation-1]+storedNum), guiY + 81, 0x000000);
		}
	}
	
	@Override
	public void init()
	{
		super.init();
		guiX = (width - 187) / 2;
		guiY = (height - 228) / 2;
		int buttonSize = 20;
		//size is 120 x 60
		
		String[][] buttons = new String[][]{{"1","2","3","+","<-"},{"4","5","6","-","CE"},{"7","8","9","*","C"},{".","0","=","/"}};
		
		for(int y=0;y<buttons.length;y++)
		{
			for(int x=0;x<buttons[y].length;x++)
			{
				int bx = guiX + 57  +120/5*x;
				int by = guiY + 115 +96/4*y;
				final int id = y*5+x;
				addRenderableWidget(new GuiTouchScreenButton(bx, by, buttonSize, buttonSize, buttons[y][x], b -> actionPerformed(id)));
			}
		}
	}
	
	protected void actionPerformed(int button)
	{
		switch (button)
		{
			case 4:
				if(Num.length()>0)
				{
					Num = Num.substring(0, Num.length()-1);
				}
				if(Num.length()<=0)
				{
					Num = "0";
				}
				break;
			case 9:
				Num = "0";
				break;
			case 14:
				Num = "0";
				storedNum = 0;
				operation = -1;
				break;
			case 3:
				result();
				storedNum = Double.valueOf(Num);
				operation=1;				
				Num = "0";
				break;
			case 8:
				result();
				storedNum = Double.valueOf(Num);
				operation=2;
				Num = "0";
				break;
			case 13:
				result();
				storedNum = Double.valueOf(Num); 
				operation=3;
				Num = "0";
				break;
			case 18:
				result();
				storedNum = Double.valueOf(Num);
				operation=4;
				Num = "0";
				break;
			case 15:
				if(!Num.contains("."))
				{
					Num += ".";
				}	
				break;
			case 0:
				Num += "1";
				break;
			case 1:
				Num += "2";
				break;
			case 2:
				Num += "3";
				break;
			case 5:
				Num += "4";
				break;
			case 6:
				Num += "5";
				break;
			case 7:
				Num += "6";
				break;
			case 10:
				Num += "7";
				break;
			case 11:
				Num += "8";
				break;
			case 12:
				Num += "9";
				break;
			case 16:
				Num += "0";
				break;
			case 17:
				result();
				break;
		}
		
		removeZero();
		
		if(storedNum==0)
		{
			operation=-1;
		}
		
	}
	
	private void result()
	{
		double result = 0;
		if(Num.startsWith("E"))
		{
			Num = "1" + Num;
		}
		if(Num.isEmpty())
		{
			Num = "0";
		}
		double d = 0;
		try {
			d = Double.valueOf(Num);
		} catch(NumberFormatException e) { }
		
		switch (operation)
		{
		case 1:
			result = storedNum + d;
			break;
		case 2:
			result = storedNum - d;
			break;
		case 3:
			result = storedNum * d;
			break;
		case 4:
			result = storedNum / d;
			break;
		default:
			result = d;
			break;
		}
		storedNum=0;
		operation=-1;
		Num = result + "";
	}
	
	private void removeZero()
	{
		if(Num.startsWith("0"))
		{
			Num = Num.substring(1);
		}
		if(Num.length()>19)
		{
			if(Num.contains("E") && !Num.startsWith("E"))
			{
				int e = Num.lastIndexOf("E");
				int over = Num.length() - 19;
				String s = Num.substring(0, e-over-1);
				s+= Num.substring(e);
				Num = s;
			}
			else
			{
				Num = Num.substring(0,19);
			}
			
		}
	}
	
	@Override
	public boolean charTyped(char typedChar, int keyCode)
	{
		boolean used = true;
		switch (typedChar)
		{
			case '1':
				Num += "1";
				break;
			case '2':
				Num += "2";
				break;
			case '3':
				Num += "3";
				break;
			case '4':
				Num += "4";
				break;
			case '5':
				Num += "5";
				break;
			case '6':
				Num += "6";
				break;
			case '7':
				Num += "7";
				break;
			case '8':
				Num += "8";
				break;
			case '9':
				Num += "9";
				break;
			case '0':
				Num += "0";
				break;
			case '+':
				result();
				storedNum = Double.valueOf(Num);
				operation=1;				
				Num = "0";
				break;
			case '-':
				result();
				storedNum = Double.valueOf(Num);
				operation=2;
				Num = "0";
				break;
			case '*':
				result();
				storedNum = Double.valueOf(Num);
				operation=3;
				Num = "0";
				break;
			case '/':
				result();
				storedNum = Double.valueOf(Num);
				operation=4;
				Num = "0";
				break;
			case '=':
				result();
				break;
			case ',':
				if(!Num.contains("."))
				{
					Num += ".";
				}	
				break;
			case '.':
				if(!Num.contains("."))
				{
					Num += ".";
				}	
				break;
			default:
				used = false;
				break;
		}
		
		boolean used2 = true;
		switch (keyCode)
		{
			case GLFW.GLFW_KEY_ENTER:
				result();
				break;
			case GLFW.GLFW_KEY_BACKSPACE:
				if(Num.length()>0)
				{
					Num = Num.substring(0, Num.length()-1);
				}
				if(Num.length()<=0)
				{
					Num = "0";
				}
				break;
			case GLFW.GLFW_KEY_DELETE:
				Num = "0";
				break;
			default:
				used2 = false;
				break;
		}
		
		removeZero();
		
		if(storedNum==0)
		{
			operation=-1;
		}
		if(used || used2)
			return true;
		
		return super.charTyped(typedChar, keyCode);
	}

	
	@Override
	public int getIconIndex()
	{
		return 3;
	}
}
