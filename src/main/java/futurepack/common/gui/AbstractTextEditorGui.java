package futurepack.common.gui;

import org.lwjgl.glfw.GLFW;

import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.KeyboardHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.events.GuiEventListener;
import net.minecraft.client.gui.screens.Screen;

public abstract class AbstractTextEditorGui implements GuiEventListener
{
	protected int cursorPos;
	protected int cursorLine;
	protected TextArea selected;
	protected boolean focused = false;
	protected boolean selecting = false;
	
	public abstract FormatedTextEditor getEditor();
	
	public abstract int getHoveredLine(double x, double y);
	
	public abstract int getHoveredLinePos(double x, double y);
	
	public abstract boolean isInBounds(double mouseX, double mouseY);
	
	public abstract void render(PoseStack matrixStack, double mouseX, double mouseY, float partialTicks);
	
	public void onPaste(String s)
	{
		if(s==null)
			return;
		
		s = s.replace("\r", "");
		s = s.replace("\t", "  ");
		
		if(!isSelected())
			getEditor().insertString(cursorLine, cursorPos, s);
		else
		{
			TextArea ta = getSelected();
			getEditor().removeLines(ta.beginLine, ta.beginPos, ta.endLine, ta.endPos);
			removeSelected();
			getEditor().insertString(cursorLine, cursorPos, s);
		}
	}
	
	public String getCopy()
	{
		TextArea ta = getSelected();
		if(ta!=null)
		{
			if(ta.beginLine == ta.endLine)
			{
				return getEditor().getRawLine(ta.beginLine).substring(ta.beginPos, ta.endPos);
			}
			else
			{
				int lines = 1 + ta.endLine - ta.beginLine;
				StringBuilder builder = new StringBuilder(100 * lines);
				for(int line = ta.beginLine; line<= ta.endLine;line++)
				{
					if(line==ta.beginLine)
					{
						builder.append(getEditor().getRawLine(line).substring(ta.beginPos));
					}
					else if(line==ta.endLine)
					{
						builder.append(getEditor().getRawLine(line).substring(0, ta.endPos));
					}
					else
					{
						builder.append(getEditor().getRawLine(line));
					}
					
					builder.append('\n');
				}
				return builder.toString();
			}
		}
		else
		{
			return null;
		}
	}
	
	public String getCutout()
	{
		TextArea ta = getSelected();
		if(ta!=null)
		{
			String s = getCopy();
			getEditor().removeLines(ta.beginLine, ta.beginPos, ta.endLine, ta.endPos);
			removeSelected();
			return s;
		}
		else
		{
			return null;
		}
	}
	
	public void selectAll()
	{
		int lines = getEditor().getLines();
		int length = getEditor().getRawLine(lines-1).length();
		
		selected = new TextArea(0, lines-1, 0, length);
	}
	
	public void onType(char c)
	{
		if(!isSelected())
		{
//			if(c == 8)//backspace
//			{
//				if(cursorPos>0)
//					getEditor().removeLines(cursorLine, cursorPos-1, cursorLine, cursorPos);
//			}
//			else if(c == 127)//delete
//			{
//				if(cursorPos+1<getEditor().getRawLine(cursorLine).length() )
//					getEditor().removeLines(cursorLine, cursorPos, cursorLine, cursorPos+1);
//			}
//			else
			{
				try
				{
					getEditor().insertChar(cursorLine, cursorPos, c);
					cursorPos++;
				}
				catch (IndexOutOfBoundsException e)
				{
					e.printStackTrace();
					cursorPos = getEditor().getRawLine(cursorLine).length();
				}
			}
			
		}
		else
		{
			if(c == '\t')
			{
				//alle zeilen einrücken
				TextArea ta = getSelected();
				for(int line = ta.beginLine;line<=ta.endLine;line++)
				{
					String original = getEditor().getRawLine(line);
					getEditor().replaceLines(line, "\t" + original);
				}
			}
			else if(c == ' ')
			{
				//alle zeilen einrücken
				TextArea ta = getSelected();
				for(int line = ta.beginLine;line<=ta.endLine;line++)
				{
					String original = getEditor().getRawLine(line);
					getEditor().replaceLines(line, " " + original);
				}
			}
//			else if(c == 8 || c == 127) //backspace end delete
//			{
//				TextArea ta = getSelected();
//				getEditor().removeLines(ta.beginLine, ta.beginPos, ta.endLine, ta.endPos);
//				removeSelected();
//			}
			else
			{
				TextArea ta = getSelected();
				getEditor().removeLines(ta.beginLine, ta.beginPos, ta.endLine, ta.endPos);
				removeSelected();
				cursorLine = ta.beginLine;
				cursorPos = ta.beginPos;
				getEditor().insertChar(cursorLine, cursorPos, c);
				cursorPos++;
			}
		}
	}
	
	public void onKey(int keyCode)
	{
		if(Screen.isSelectAll(keyCode))
		{
			selectAll();
		}
		else if(Screen.isCopy(keyCode))
		{
			String s = getCopy();
			if(s!=null)
				getKeyboardListener().setClipboard(s);
		}
		else if(Screen.isCut(keyCode))
		{
			String s = getCutout();
			if(s!=null)
				getKeyboardListener().setClipboard(s);
		}
		else if(Screen.isPaste(keyCode))
		{
			onPaste(getKeyboardListener().getClipboard());
		}
		else if(isSelected())
		{
			if(keyCode == GLFW.GLFW_KEY_DELETE || keyCode == GLFW.GLFW_KEY_BACKSPACE)
			{
				TextArea ta = getSelected();
				getEditor().removeLines(ta.beginLine, ta.beginPos, ta.endLine, ta.endPos);
				removeSelected();
			}
		}
		else
		{
			switch (keyCode)
			{
			case GLFW.GLFW_KEY_LEFT:
				cursorPos--;
				if(cursorPos<0)
				{
					cursorLine--;
					if(cursorLine<0)
						cursorLine = 0;
					cursorPos = getEditor().getRawLine(cursorLine).length();
				}
				break;
			case GLFW.GLFW_KEY_UP:	
				cursorLine--;
				if(cursorLine<0)
					cursorLine = 0;
				
				cursorPos = Math.min(cursorPos, getEditor().getRawLine(cursorLine).length());
				break;
			case GLFW.GLFW_KEY_RIGHT:
				cursorPos++;
				if(cursorPos>getEditor().getRawLine(cursorLine).length())
				{
					cursorPos=0;
					cursorLine++;
					if(cursorLine>=getEditor().getLines())
						cursorLine=getEditor().getLines()-1;
				}
				break;
			case GLFW.GLFW_KEY_DOWN:
				cursorLine++;
				if(cursorLine>=getEditor().getLines())
					cursorLine=getEditor().getLines()-1;
				cursorPos = Math.min(cursorPos, getEditor().getRawLine(cursorLine).length());
				break;
			case GLFW.GLFW_KEY_DELETE:
				if(cursorPos+1<getEditor().getRawLine(cursorLine).length() )
				{
					getEditor().removeLines(cursorLine, cursorPos, cursorLine, cursorPos+1);
				}
				else //remove 1st char of next line, alias removes \n and merges lines
				{
					getEditor().removeLines(cursorLine, cursorPos, cursorLine+1, 0);
//					String line1=getEditor().getRawLine(cursorLine);
//					if(getEditor().getLines() > cursorLine+1)
//					{
//						String line2 = getEditor().getRawLine(cursorLine+1);
//						getEditor().replaceLines(cursorLine, line1+line2);
//						getEditor().replaceLines(cursorLine+1, "");
//					}
				}
				getEditor().notifyFormattingChangeFrom(Math.max(0, cursorLine-1));
				break;
			case GLFW.GLFW_KEY_BACKSPACE:
				if(cursorPos>0)
				{
					getEditor().removeLines(cursorLine, cursorPos-1, cursorLine, cursorPos);
					cursorPos--;
				}
				else
				{
					if(cursorLine>0)//remove last char of prevous line
					{
						cursorLine--;
						cursorPos = getEditor().getRawLine(cursorLine).length();
						getEditor().removeLines(cursorLine, Math.max(0, cursorPos-1), cursorLine+1, 0);
						cursorPos--;
					}
				}
				getEditor().notifyFormattingChangeFrom(cursorLine);
				break;
			case GLFW.GLFW_KEY_TAB:
				onType('\t');
				break;
			case GLFW.GLFW_KEY_ENTER:
				onType('\n');
				cursorLine++;
				cursorPos=0;
				getEditor().notifyFormattingChangeFrom(Math.max(0, cursorLine-1));
				break;
			default:
				break;
			}
		}
	}
	
	public boolean isSelected()
	{
		return getSelected() != null;
	}
	
	public TextArea getSelected()
	{
		return this.selected;
	}
	
	public void removeSelected()
	{
		this.selected = null;
	}
	
	public boolean isFocused()
	{
		return focused;
	}
	
	public KeyboardHandler getKeyboardListener()
	{
		return Minecraft.getInstance().keyboardHandler;
	}
	
	public boolean updateFocus(double mouseX, double mouseY)
	{
		focused = isInBounds(mouseX, mouseY);
		return focused;
	}
	
	
	
	@Override
	public boolean charTyped(char input, int p_charTyped_2_) 
	{
		if(isFocused())
		{
			onType(input);
			return true;
		}
		return false;
	}
	
	
	
	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int button) 
	{
		dragging = false;
		if(isFocused())
		{
			selecting = false;
			if(isSelected())
			{
				if(selected.beginLine == selected.endLine && selected.beginPos == selected.endPos)
				{
					selected = null;
				}
			}
			return true;
		}
		return false;
	}
	
	@Override
	public boolean mouseDragged(double mouseX, double mouseY, int button, double deltaX, double deltaY) 
	{
		if(isFocused())
		{
			if(!selecting && !isSelected())
			{
				selecting = true;
				selected = new TextArea(cursorLine, cursorLine, cursorPos, cursorPos);
			}
			else if(selecting && isSelected())
			{
				TextArea ta = getSelected();
				ta.beginLine = cursorLine;
				ta.beginPos = cursorPos;
				ta.endLine = getHoveredLine(mouseX, mouseY);
				ta.endPos = getHoveredLinePos(mouseX, mouseY);
				ta.fixBounds();
				
				String begin = getEditor().getRawLine(ta.beginLine);
				String end = getEditor().getRawLine(ta.endLine);
				
				if(ta.beginPos > begin.length())
				{
					ta.beginPos = begin.length();
				}
				if(ta.endPos > end.length())
				{
					ta.endPos = end.length();
				}
			}
			return true;
		}
		return false;
	}
	
	private boolean dragging = false;
	
	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) 
	{
		if(updateFocus(mouseX, mouseY))
		{
			if(isSelected())
			{
				removeSelected();
			}
			if(button==0)
				dragging = true;
		
			cursorLine = getHoveredLine(mouseX, mouseY);
			cursorPos = getHoveredLinePos(mouseX, mouseY);
			
			return true;
		}
		return false;
	}
	
	@Override
	public boolean isMouseOver(double mouseX, double mouseY) 
	{
		return isInBounds(mouseX, mouseY);
	}
	
	@Override
	public boolean changeFocus(boolean b) 
	{
		focused = b;
		return b;
	}	
	
	@Override
	public boolean keyPressed(int keyCode, int scanCode, int modifiers) 
	{
		if(isFocused())
		{
			onKey(keyCode);
			return true;
		}
		
		return false;
	}
	
	@Override
	public void mouseMoved(double mouseX, double mouseY) 
	{
		if(dragging)
		{
			mouseDragged(mouseX, mouseY, 0, 0, 0);
		}
	}
}
