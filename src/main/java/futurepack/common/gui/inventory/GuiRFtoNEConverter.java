package futurepack.common.gui.inventory;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.block.misc.TileEntityRFtoNEConverter;
import futurepack.common.block.misc.TileEntityRFtoNEConverter.EnumConversation;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperRendering;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Player;

public class GuiRFtoNEConverter extends ActuallyUseableContainerScreen<GuiRFtoNEConverter.ContainerRFNEConverter>
{
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/rf2ne_gui.png");
	
	public GuiRFtoNEConverter(Player pl, TileEntityRFtoNEConverter tile)
	{
		super(new ContainerRFNEConverter(tile), pl.getInventory(), "gui.rf2neconverter");
		imageWidth = 112;
		imageHeight = 86;
	}
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int mouseX, int mouseY)
	{
		super.renderLabels(matrixStack, mouseX, mouseY);
		if(container().tile.isWorking())
		{
			this.drawString(matrixStack, font, container().tile.getConv().getLimit() * 4 + " RF/t", 19, 11, 0xFFFFFFFF);
		}
	}
	
	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		//19 51
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		RenderSystem.setShaderTexture(0, res);
		this.blit(matrixStack, leftPos, topPos, 0, 0, this.imageWidth, this.imageHeight);

		EnumConversation conv = container().tile.getConv();
		this.blit(matrixStack, leftPos+19, topPos+51, conv.xPos, conv.yPos, 41, 21);
		
		if(!container().tile.isWorking())
			this.blit(matrixStack, leftPos+88, topPos+43, 112, 0, 10, 25);
		

	}
	
	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int but) 
	{
		if(but == 0)
		{
			EnumConversation conv = container().tile.getConv();
			
			if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, 88, 43, 98, 68))
			{
				container().working = !container().tile.isWorking();
				FPPacketHandler.syncWithServer(container());
				return true;
			}
			else if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, 7, 39, 39, 73))
			{
				int o = conv.ordinal()-1;
				if(o<0)
					o=0;
				
				if(o!=container().clientid)
				{
					container().clientid = o;
					FPPacketHandler.syncWithServer(container());
					return true;
				}
			}
			else if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, 40, 39, 71, 73))
			{
				int o = conv.ordinal()+1;
				if(o >= EnumConversation.values().length)
					o = EnumConversation.values().length-1;
				
				if(o!=container().clientid)
				{
					container().clientid = o;
					FPPacketHandler.syncWithServer(container());
					return true;
				}
			}
		}
		return super.mouseReleased(mouseX, mouseY, but);
	}

	public ContainerRFNEConverter container()
	{
		return this.getMenu();
	}
	
	public static class ContainerRFNEConverter extends ContainerSyncBase implements IGuiSyncronisedContainer
	{
		protected TileEntityRFtoNEConverter tile;
		private int clientid;
		private boolean working;
		
		public ContainerRFNEConverter(TileEntityRFtoNEConverter tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			working = tile.isWorking();
			clientid = tile.getConv().ordinal();
			
		}
		
		@Override
		public boolean stillValid(Player playerIn)
		{
			return HelperResearch.isUseable(playerIn, tile);
		}

		@Override
		public void writeToBuffer(FriendlyByteBuf buf) 
		{
			buf.writeByte(clientid);
			buf.writeBoolean(working);
		}

		@Override
		public void readFromBuffer(FriendlyByteBuf buf) 
		{
			byte c = buf.readByte();
			working = buf.readBoolean();
			tile.setProperty(TileEntityRFtoNEConverter.PROPERTY_CONV, c);
			tile.setProperty(TileEntityRFtoNEConverter.PROPERTY_WORKING, working?1:0);
		}
		
	}
}
