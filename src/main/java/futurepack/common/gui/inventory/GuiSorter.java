package futurepack.common.gui.inventory;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.block.modification.machines.TileEntitySorter;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.SlotFakeItem;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ClickType;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class GuiSorter extends GuiModificationBase<TileEntitySorter>
{
	public GuiSorter(Player pl, TileEntitySorter tile)
	{
		super(new ContainerSorter(pl.getInventory(), tile), "sorter.png", pl.getInventory());
		this.imageHeight = 181;
		this.ny=14;
	}
	
	@Override
	public TileEntitySorter tile()
	{
		return ((ContainerSorter)getMenu()).tile;
	}

	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY) 
	{
		super.renderBg(matrixStack, partialTicks, mouseX, mouseY);
	}

	
	public static class ContainerSorter extends ContainerSyncBase
	{
		TileEntitySorter tile;
		
		public ContainerSorter(Inventory inv, TileEntitySorter tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			int l;
			int i1;
			
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 3; ++i1)
				{
					this.addSlot(new Slot(tile, i1 + l * 3, 25 + i1 * 18, 15 + l * 18));
				}
			}
			
			for (l = 0; l < 4; ++l)
			{
				for (i1 = 0; i1 < 4; ++i1)
				{
					this.addSlot(new SlotFakeItem(tile, i1 + l * 4 + 9, 98 + i1 * 18, 15 + l * 18));
				}
			}

			
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 99 + l * 18));
				}
			}
			
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(inv, l, 8 + l * 18, 157));
			}
		}

		@Override
		public void clicked(int slotId, int clickedButton, ClickType mode, Player playerIn)
		{
			if(slotId > 0 && mode == ClickType.PICKUP)
			{
				Slot s = getSlot(slotId);
				if(s instanceof SlotFakeItem)
				{
					if(!getCarried().isEmpty())
					{
						ItemStack inMouseStack = getCarried().copy();
						inMouseStack.setCount(1);	
						s.set(inMouseStack);
						//this.detectAndSendChanges();
					}
					else
					{
						s.set(ItemStack.EMPTY);
						//this.detectAndSendChanges();
					}
				}
			}
			super.clicked(slotId, clickedButton, mode, playerIn);
		}
		
		@Override
		public ItemStack quickMoveStack(Player pl, int par2)
		{
			if(!pl.level.isClientSide)
			{
				Slot slot = this.slots.get(par2);
		        if(slot != null && slot.hasItem())
		        {
		        	if(slot.container == tile)
		        	{
		        		if(this.moveItemStackTo(slot.getItem(), 25, slots.size(), false))
		        			slot.container.setItem(slot.getSlotIndex(), ItemStack.EMPTY);
		        	}
		        	else
		        	{
		        		if(this.moveItemStackTo(slot.getItem(), 0, 9, false))
		        			slot.container.setItem(slot.getSlotIndex(), ItemStack.EMPTY);
		        	}
		        }
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}
		
		@Override
		public boolean stillValid(Player var1)
		{
			return true;
		}
		
	}
}
