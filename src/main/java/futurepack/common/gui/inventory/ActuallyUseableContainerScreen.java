package futurepack.common.gui.inventory;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.WeakHashMap;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.interfaces.IContainerWithHints;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.Slot;

public abstract class ActuallyUseableContainerScreen<T extends AbstractContainerMenu> extends AbstractContainerScreen<T> 
{

	protected Map<Slot, List<Component>> hints = new WeakHashMap<>();
	
	public ActuallyUseableContainerScreen(T screenContainer, Inventory inv, String toTranslate) 
	{
		super(screenContainer, inv, new TranslatableComponent(toTranslate.contains("futurepack") ? toTranslate : "futurepack." + toTranslate));
	
		if(screenContainer instanceof IContainerWithHints)
		{
			((IContainerWithHints) screenContainer).addHints(hints);
		}
	}

	@Deprecated
	public ActuallyUseableContainerScreen(T screenContainer, Inventory inv) 
	{
		this(screenContainer, inv, "TODO");
	}
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int x, int y)
	{
		//Removed to remove default text (inventory and gui name) super.drawGuiContainerForegroundLayer(matrixStack, x, y);
	}
	
	@Override
	protected void renderTooltip(PoseStack pPoseStack, int pX, int pY)
	{
		super.renderTooltip(pPoseStack, pX, pY);
		
		if (this.menu.getCarried().isEmpty() && this.hoveredSlot != null && !this.hoveredSlot.hasItem())
		{
			List<Component> tooltip = getEmptySlotInfo(this.hoveredSlot);
			if(tooltip!=null)
				renderTooltip(pPoseStack, tooltip, Optional.empty(), pX, pY);
		}
	}
	
	protected  List<Component> getEmptySlotInfo(Slot slot)
	{
		return hints.getOrDefault(slot, null);
	}
}
