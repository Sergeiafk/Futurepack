package futurepack.common.sync;

import java.util.function.Supplier;

import futurepack.common.entity.living.EntityAlphaJawaul;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerPlayer;
import net.minecraftforge.network.NetworkEvent;


public class MessageRequestJawaulInventory
{
	public MessageRequestJawaulInventory()
	{

	}

	public static MessageRequestJawaulInventory decode(FriendlyByteBuf buf) 
	{
		return new MessageRequestJawaulInventory();
	}
		
	public static void encode(MessageRequestJawaulInventory msg, FriendlyByteBuf buf) 
	{
	}
			
	public static void consume(MessageRequestJawaulInventory message, Supplier<NetworkEvent.Context> ctx) 
	{
		ServerPlayer pl = ctx.get().getSender();
		
		if(pl.isPassenger() && pl.getVehicle() instanceof EntityAlphaJawaul)
		{
			pl.openMenu((EntityAlphaJawaul)pl.getVehicle());
		}
		
		ctx.get().setPacketHandled(true);
	}
	
}
