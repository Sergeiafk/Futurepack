package futurepack.common.block.inventory;

import futurepack.common.block.BlockRotateable;
import futurepack.common.sync.FPGuiHandler;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.BlockHitResult;

public class BlockScanner extends BlockRotateable
{
	public static final BooleanProperty ON = BooleanProperty.create("on");
	
	public BlockScanner(Block.Properties props)
	{
		super(props);
	}

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(ON);
	}
	
	@Override
	public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, LevelAccessor worldIn, BlockPos pos, BlockPos facingPos)
	{
		TileEntityScannerBlock scanner = (TileEntityScannerBlock) worldIn.getBlockEntity(pos);
		return state.setValue(ON, scanner.isOn());
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
//		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.BLOCK_SCANNER.openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		 return new TileEntityScannerBlock(pos, state);
	}
}
