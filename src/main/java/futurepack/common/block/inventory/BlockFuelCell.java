package futurepack.common.block.inventory;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockHoldingTile;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.phys.BlockHitResult;

public class BlockFuelCell extends BlockHoldingTile implements IBlockServerOnlyTickingEntity<TileEntityFuelCell>
{
	public static final EnumProperty<Direction.Axis> AXIS = BlockStateProperties.AXIS;
	
	protected BlockFuelCell(Block.Properties props)
	{
		super(props);
		this.registerDefaultState(this.defaultBlockState().setValue(AXIS, Direction.Axis.Y));
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.FUEL_CELL.openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}

	@Override
	public BlockState rotate(BlockState state, Rotation rot)
	{
		switch(rot) 
		{
		case COUNTERCLOCKWISE_90:
		case CLOCKWISE_90:
			switch(state.getValue(AXIS)) {
			case X:
				return state.setValue(AXIS, Direction.Axis.Z);
			case Z:
				return state.setValue(AXIS, Direction.Axis.X);
			default:
				return state;
			}
		default:
			return state;
		}
	}

	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		builder.add(AXIS);
	}

	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		return this.defaultBlockState().setValue(AXIS, context.getClickedFace().getAxis());
	}

	@Override
	public BlockEntityType<TileEntityFuelCell> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.FUEL_CELL;
	}
}
