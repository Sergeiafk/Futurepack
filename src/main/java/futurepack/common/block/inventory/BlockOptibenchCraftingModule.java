package futurepack.common.block.inventory;

import futurepack.common.block.BlockRotateableTile;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

public class BlockOptibenchCraftingModule extends BlockRotateableTile
{

	public BlockOptibenchCraftingModule(Block.Properties props)
	{
		super(props);
	}

	
	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new TileEntityOptiBenchCraftingModule(pos, state);
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		((TileEntityOptiBenchCraftingModule)w.getBlockEntity(pos)).onNeighborChange();
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.OPTI_BENCH_CRAFTING_MODULE.openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}
	
	@Override
	public void onNeighborChange(BlockState state, LevelReader w, BlockPos pos, BlockPos neighbor) 
	{
		((TileEntityOptiBenchCraftingModule)w.getBlockEntity(pos)).onNeighborChange();
		super.onNeighborChange(state, w, pos, neighbor);
		
	}
	
	@Override
	public void setPlacedBy(Level w, BlockPos pos, BlockState state, LivingEntity liv, ItemStack it) 
	{
		super.setPlacedBy(w, pos, state, liv, it);
		((TileEntityOptiBenchCraftingModule)w.getBlockEntity(pos)).onNeighborChange();
	}
	
}
