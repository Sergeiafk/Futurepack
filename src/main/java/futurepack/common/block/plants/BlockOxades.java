package futurepack.common.block.plants;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.item.FoodItems;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockOxades extends CropBlock implements EntityBlock, IBlockServerOnlyTickingEntity<TileEntityOxades>
{
	public static final IntegerProperty AGE = IntegerProperty.create("age", 0, 11);
	private static VoxelShape[] boxes = new VoxelShape[16];
	
	public BlockOxades(Block.Properties props) 
	{
		super(props);		
	}
	
	@Override
	public IntegerProperty getAgeProperty()
    {
        return AGE;
    }
	
	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		builder.add(getAgeProperty());
	}

	@Override
    public int getMaxAge()
    {
        return 11;
    }
	
	
	
	@Override
	protected ItemLike getBaseSeedId()
    {
        return FoodItems.oxades_seeds;
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext sel)
    {
    	return getSelctionBox(getAge(state));
    }
    
    private static VoxelShape getSelctionBox(int growth)
    {
    	int[] height = new int[]{2,4,5,6,7,9,9,7,9,11,14,15};
    	if(boxes[height[growth]]!=null)
    		return boxes[height[growth]];
    	else
    	{
    		return boxes[height[growth]] = Block.box(1D, 0D, 1D, 15D, height[growth], 15D);
    	}
    }
    
	@Override
	public BlockEntityType<TileEntityOxades> getTileEntityType(BlockState pState) 
	{
		return FPTileEntitys.OXADES;
	}
	
//	@Override
//	public boolean hasBlockEntity(BlockState state)
//	{
//		return true;
//	}
}
