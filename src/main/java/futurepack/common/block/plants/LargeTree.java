package futurepack.common.block.plants;

import java.util.Random;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.grower.AbstractTreeGrower;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.configurations.TreeConfiguration;

public abstract class LargeTree extends AbstractTreeGrower 
{
	final int size;

	public LargeTree(int width)
	{
		size = width;
	}

	@Override
	public boolean growTree(ServerLevel worldIn, ChunkGenerator chunkGenerator, BlockPos pos, BlockState blockUnder, Random rand) 
	{
		for (int x = 0; x > -size; --x) 
		{
			for (int z = 0; z > -size; --z) 
			{
				if (canLargeTreeSpawnAt(blockUnder, worldIn, pos, x, z)) 
				{
					return this.spawnLargeTree(worldIn, pos, blockUnder, rand, x, z);
				}
			}
		}

		return super.growTree(worldIn, chunkGenerator, pos, blockUnder, rand);
	}

	@Nullable
	protected abstract Holder<ConfiguredFeature<TreeConfiguration, ?>> getLargeTreeFeature(Random random);

	public boolean spawnLargeTree(WorldGenLevel worldIn, BlockPos pos, BlockState blockUnder, Random random, int xOffset, int zOffset) 
	{
		ConfiguredFeature<TreeConfiguration, ?> abstracttreefeature = this.getLargeTreeFeature(random).value();
		if (abstracttreefeature == null) 
		{
			return false;
		} else 
		{
			BlockState air = Blocks.AIR.defaultBlockState();
			for (int x = 0; x < size; ++x) 
			{
				for (int z = 0; z < size; ++z) 
				{
					worldIn.setBlock(pos.offset(xOffset+x, 0, zOffset+z), air, 4);
				}
			}
			if (abstracttreefeature.place(worldIn, ((ServerLevel)worldIn).getChunkSource().getGenerator(), random, pos.offset(xOffset , 0, zOffset))) 
			{
				return true;
			}
			else
			{
				for (int x = 0; x < size; ++x) 
				{
					for (int z = 0; z < size; ++z) 
					{
						worldIn.setBlock(pos.offset(xOffset+x, 0, zOffset+z), blockUnder, 4);
					}
				}
				return false;
			}
		}
	}

	public boolean canLargeTreeSpawnAt(BlockState blockUnder, BlockGetter worldIn, BlockPos pos, int xOffset, int zOffset) 
	{
		Block block = blockUnder.getBlock();
		for (int x = 0; x < size; ++x) 
		{
			for (int z = 0; z < size; ++z) 
			{
				if(block != worldIn.getBlockState(pos.offset(xOffset+x, 0, zOffset+z)).getBlock())
				{
					return false;
				}
			}
		}
		return true;
	}

}
