package futurepack.common.block.plants;

import java.util.Random;

import futurepack.world.dimensions.biomes.FPBiomes;
import futurepack.world.gen.FPFeatures;
import net.minecraft.core.Holder;
import net.minecraft.data.worldgen.features.FeatureUtils;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.TreeConfiguration;

public class TyrosTree extends LargeTree
{
	public TyrosTree() 
	{
		super(5);
	}

	private Holder<ConfiguredFeature<TreeConfiguration, ?>> tyros_tree = FeatureUtils.register("tyrps_tree", Feature.TREE, FPBiomes.TYROS_TREE_CONFIG);
	private Holder<ConfiguredFeature<TreeConfiguration, ?>> large_tyros_tree = FeatureUtils.register("large_tyros_tree", FPFeatures.LARGE_TYROS_TREE, FPBiomes.TYROS_TREE_CONFIG);
	
	
	@Override
	protected Holder<ConfiguredFeature<TreeConfiguration, ?>> getConfiguredFeature(Random random, boolean par2) 
	{
		return tyros_tree;
	}

	@Override
	protected Holder<ConfiguredFeature<TreeConfiguration, ?>> getLargeTreeFeature(Random random) 
	{
		return large_tyros_tree;
	}

}
