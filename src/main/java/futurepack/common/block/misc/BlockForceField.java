package futurepack.common.block.misc;

import futurepack.common.block.BlockHoldingTile;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.EntityCollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class BlockForceField extends BlockHoldingTile 
{

	public BlockForceField(Block.Properties props)
	{
		super(props);
//		super(Material.IRON);
//		setCreativeTab(FPMain.tab_deco);
	}

//	@Override
//	public boolean isOpaqueCube(IBlockState state)
//	{
//		return false;
//	}
	
	@Override
	public void onRemove(BlockState state, Level w, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if (state.getBlock() != newState.getBlock()) 
		{
			w.removeBlockEntity(pos);
		}
	}

	@Override
	public void setPlacedBy(Level w, BlockPos pos, BlockState state, LivingEntity liv, ItemStack it)
	{
		TileEntityForceField force = (TileEntityForceField) w.getBlockEntity(pos);
		force.letPlayerPass = true;
	}
	
	@Override
	public RenderShape getRenderShape(BlockState state)
	{
		return RenderShape.ENTITYBLOCK_ANIMATED;
	}
	
	@Override
	@OnlyIn(Dist.CLIENT)
	public float getShadeBrightness(BlockState state, BlockGetter worldIn, BlockPos pos) 
	{
		return 1.0F;
	}
	
	@Override
	public boolean propagatesSkylightDown(BlockState state, BlockGetter reader, BlockPos pos) 
	{
		return true;
	}
	
	@Override
	public VoxelShape getCollisionShape(BlockState state, BlockGetter w, BlockPos pos, CollisionContext context) 
	{
		if(context instanceof EntityCollisionContext && ((EntityCollisionContext)context).getEntity()!=null)
		{
			TileEntityForceField field = (TileEntityForceField) w.getBlockEntity(pos);
			if(field !=null && field.canEntityPass(((EntityCollisionContext)context).getEntity()))
			{			
				return Shapes.empty();
			}
		}
		return super.getCollisionShape(state, w, pos, context);
	}
	
	@Override
	public boolean skipRendering(BlockState state, BlockState adjacentBlockState, Direction side)
	{
		if(adjacentBlockState.getBlock() == this)
			return true;
		
		return super.skipRendering(state, adjacentBlockState, side);
	}

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new TileEntityForceField(pos, state);
	}
}
