package futurepack.common.block.misc;

import futurepack.api.Constants;
import futurepack.common.block.BlockRotateable;
import futurepack.common.gui.escanner.GuiResearchMainOverview;
import net.minecraft.advancements.Advancement;
import net.minecraft.client.Minecraft;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.LecternBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.pathfinder.PathComputationType;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;

public class BlockTectern extends BlockRotateable 
{

	public BlockTectern(Properties properties) 
	{
		super(properties);
	}

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state) 
	{
		return new TileEntityResearchExchange(pos, state);
	}
	
	@Override
	public VoxelShape getOcclusionShape(BlockState state, BlockGetter worldIn, BlockPos pos) 
	{
		return LecternBlock.SHAPE_COMMON;
	}

	@Override
	public boolean useShapeForLightOcclusion(BlockState state) 
	{
		return true;
	}

	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context) 
	{
		return this.defaultBlockState().setValue(HORIZONTAL_FACING, context.getHorizontalDirection().getOpposite());
	}

	@Override
	public VoxelShape getCollisionShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) 
	{
		return LecternBlock.SHAPE_COLLISION;
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context)
	{
		switch(state.getValue(BlockRotateable.HORIZONTAL_FACING)) 
		{
		case NORTH:
			return LecternBlock.SHAPE_NORTH;
		case SOUTH:
			return LecternBlock.SHAPE_SOUTH;
		case EAST:
			return LecternBlock.SHAPE_EAST;
		case WEST:
			return LecternBlock.SHAPE_WEST;
		default:
			return LecternBlock.SHAPE_COMMON;
		}
	}
	
	@Override
	public boolean isPathfindable(BlockState state, BlockGetter worldIn, BlockPos pos, PathComputationType type) 
	{
		return false;
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand handIn, BlockHitResult hit) 
	{
		if(!w.isClientSide && pl instanceof ServerPlayer)
		{
			TileEntityResearchExchange tile = (TileEntityResearchExchange) w.getBlockEntity(pos);
			tile.onInteract(pl);
			ServerLevel sw = (ServerLevel) w;
			Advancement adv = sw.getServer().getAdvancements().getAdvancement(new ResourceLocation(Constants.MOD_ID, "brain_touch"));
			((ServerPlayer)pl).getAdvancements().award(adv, "use_tectern");
		}
		else
		{
			DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable()
			{
				@Override
				public void run() 
				{
					Minecraft.getInstance().setScreen(new GuiResearchMainOverview());
				}
			});
		}
		return InteractionResult.SUCCESS;
	}

}
