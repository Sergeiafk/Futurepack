package futurepack.common.block.misc;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;

import futurepack.common.block.BlockRotateableTile;
import futurepack.common.dim.structures.TeleporterMap;
import futurepack.common.dim.structures.TeleporterMap.TeleporterEntry;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageChekpointSelect;
import futurepack.common.sync.MessageEScanner;
import futurepack.depend.api.helper.HelperBoundingBoxes;
import net.minecraft.ChatFormatting;
import net.minecraft.client.Minecraft;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.ClickEvent;
import net.minecraft.network.chat.ClickEvent.Action;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.network.PacketDistributor;

public class BlockDungeonCheckpoint extends BlockRotateableTile 
{
	private static final Supplier<double[][]> shape = () -> new double[][]{
		{1,0,1,15,4,15},
		{0,4,0,16,7,16}
	};
	public static final VoxelShape UP = HelperBoundingBoxes.createBlockShape(shape.get(), 0, 0);
	private final VoxelShape DOWN = HelperBoundingBoxes.createBlockShape(shape.get(), 180F, 0F);
	private final VoxelShape NORTH = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 0F);
	private final VoxelShape SOUTH = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 180F);
	private final VoxelShape WEST = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 90F);
	private final VoxelShape EAST = HelperBoundingBoxes.createBlockShape(shape.get(), 90F, 270F);
	
	
	public BlockDungeonCheckpoint(Properties props) 
	{
		super(props);
	}

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new TileEntityDungeonCheckpoint(pos, state);
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext sel)
	{
		Direction face = state.getValue(BlockRotateableTile.FACING);
		switch (face)
		{
		case NORTH:
			return NORTH;
		case SOUTH:
			return SOUTH;
		case WEST:	
			return WEST;
		case EAST:
			return EAST;
		case DOWN:
			return DOWN;
		case UP:
		default:
			return UP;
		}
	}
	
	@Override
	public InteractionResult use(BlockState state, Level worldIn, BlockPos pos, Player player, InteractionHand handIn, BlockHitResult hit)
	{
		if(!worldIn.isClientSide)
		{
			onInteraction(worldIn, pos, player);
			
		}
		return InteractionResult.SUCCESS;
	}
	
	@Override
	public void entityInside(BlockState state, Level worldIn, BlockPos pos, Entity entityIn)
	{
		super.entityInside(state, worldIn, pos, entityIn);
		
		if(!worldIn.isClientSide && entityIn instanceof Player)
		{
			//onInteraction(worldIn, pos,  (PlayerEntity) entityIn);
		}
	}
	
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		Direction face = context.getClickedFace();
		return super.getStateForPlacement(context).setValue(FACING, face);
	}
	
	public void onInteraction(Level w, BlockPos pos, Player pl)
	{
		if(!w.isClientSide)
		{
			TeleporterMap tmap = TeleporterMap.getTeleporterMap((ServerLevel) w);
			tmap.addTeleporter(pl, pos);
			
			List<TeleporterEntry> checkpoints = tmap.getTeleporter(pl);
			List<MutableComponent> list = new ArrayList<MutableComponent>(checkpoints.size() + 1);
			
			list.add(new TextComponent("You are at: "));
			list.add(new TextComponent("Teleport to: "));
			
			Style here = Style.EMPTY.withBold(true).withColor(ChatFormatting.DARK_GRAY);

			//Non-destructive on-the-sort creation of a sorting key for compatibility with already existing checkpoints
			checkpoints.sort(Comparator.comparing(a ->
			a.name.substring(a.name.indexOf(167) + 2, a.name.length() - 3) +
			String.format("%03d", Integer.parseInt(a.name.substring(6, a.name.indexOf(' ', 6)))) +//Floor
			a.name.substring(a.name.length() - 3) +//Tech level
			a.name.substring(8, 10)));//Dungeon name color

			for(TeleporterEntry e : checkpoints)
			{
				TextComponent name = new TextComponent(e.name);
				
				if(pos.equals(e.pos))
				{
					name.setStyle(here);
					list.get(0).append(name);
				}
				else
				{
					
					ClickEvent event = new ClickEvent(Action.CHANGE_PAGE, "dungeon_teleport=" + Long.toHexString(e.pos.asLong()));
					Style click = Style.EMPTY.setUnderlined(true).withColor(ChatFormatting.DARK_BLUE).withClickEvent(event);
					name.setStyle(click);
					list.add(name);
				}
			}
			
			MessageEScanner msg = new MessageEScanner(true, list.toArray(new Component[list.size()]));
			FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(() -> (ServerPlayer)pl), msg);
		}
		
	}

	/**
	 * this gets called from ComponentInteractiveText
	 * @param value is the long of the teleporter pos as hex
	 */
	public static void onClientSelectTeleporter(String value, Player player)
	{
		long l = Long.parseUnsignedLong(value, 16);
		MessageChekpointSelect sel = new MessageChekpointSelect(l);
		FPPacketHandler.CHANNEL_FUTUREPACK.sendToServer(sel);
		Minecraft.getInstance().setScreen(null);
	}
}
