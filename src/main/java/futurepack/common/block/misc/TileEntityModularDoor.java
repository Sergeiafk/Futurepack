package futurepack.common.block.misc;

import futurepack.api.interfaces.tilentity.ITileClientTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityModularDoor extends FPTileEntityBase implements ITileClientTickable
{
	private int size = 32, prevSize=32;
	
	//client only
//	private BlockState buffer = null;
	
	public TileEntityModularDoor(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.MODULAR_DOOR, pos, state);
	}
	
	public void open()
	{
		BlockState state = level.getBlockState(worldPosition);
		state = state.setValue(BlockModularDoor.OPEN, true);
		state = state.setValue(BlockModularDoor.COMPLETE, false);
		
		level.setBlock(worldPosition, state, 3);
	}
	
	public void close()
	{
		BlockState state = level.getBlockState(worldPosition);
		state = state.setValue(BlockModularDoor.OPEN, false);
		state = state.setValue(BlockModularDoor.COMPLETE, false);
		
		level.setBlock(worldPosition, state, 3);
	}
	
	public void invisible()
	{
		size = 0;
		setChanged();
	}
	
	public float getSize()
	{
		return size *0.5F;
	}
	
	public float getPrevSize()
	{
		return prevSize *0.5F;
	}

	@Override
	public void tickClient(Level pLevel, BlockPos pPos, BlockState pState)
	{
		{
//			BlockState state = level.getBlockState(worldPosition);
//			if(buffer!=null)
//			{
//				state = buffer;
//				level.setBlockAndUpdate(worldPosition, buffer);
//				buffer = null;
//			}
			if(!pState.getValue(BlockModularDoor.COMPLETE))
			{
				update(pState);
			}
		}
	}
	
	public void update(BlockState state)
	{
		prevSize=size;
		if(state.getValue(BlockModularDoor.OPEN))
		{
			if(size>1)
			{
				size--;
				level.scheduleTick(worldPosition, state.getBlock(), 1);
				return;
			}
		}
		else
		{
			if(size<32)
			{
				size++;
				level.scheduleTick(worldPosition, state.getBlock(), 1);
				return;
			}
		}

		onMovingFinished();
	}
	
	private void onMovingFinished()
	{
		BlockState state = level.getBlockState(worldPosition);
		state = state.setValue(BlockModularDoor.COMPLETE, true);
		level.setBlock(worldPosition, state, 3);
		
		((BlockModularDoor)MiscBlocks.modular_door).onMovingFinished(level, worldPosition, state);
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag compound)
	{
		super.writeDataUnsynced(compound);
		compound.putInt("size", size);
		return compound;
	}
	
	@Override
	public void readDataUnsynced(CompoundTag compound)
	{
		super.readDataUnsynced(compound);
		prevSize = size = compound.getInt("size");
	}
	
//	@Override
//	public boolean shouldRefresh(World w, BlockPos pos, IBlockState oldState, IBlockState newSate)
//	{
//		if(oldState.getBlock()==newSate.getBlock())
//		{
//			if(w.isRemote)
//			{
//				if(oldState.get(BlockModularDoor.OPEN) != newSate.get(BlockModularDoor.OPEN))
//				{
//					if(newSate.get(BlockModularDoor.COMPLETE))
//					{
//						buffer = newSate.with(BlockModularDoor.COMPLETE, false);
//					}
//				}
//			}
//			return false;
//		}
//		
//		return true;
//	}

	public boolean canRender()
	{
		BlockState state = level.getBlockState(worldPosition);
		if(state.getBlock()==Blocks.AIR)
			return false;
		return size>0 && (!state.getValue(BlockModularDoor.COMPLETE) || state.getValue(BlockModularDoor.OPEN));
	}
}
