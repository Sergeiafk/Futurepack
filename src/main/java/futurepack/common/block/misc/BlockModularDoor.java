package futurepack.common.block.misc;

import java.util.Random;

import futurepack.api.FacingUtil;
import futurepack.api.interfaces.IBlockClientOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Direction.AxisDirection;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockModularDoor extends BlockRotateableTile implements IBlockClientOnlyTickingEntity<TileEntityModularDoor>
{
	public static final BooleanProperty OPEN = BooleanProperty.create("open");
	public static final BooleanProperty COMPLETE = BooleanProperty.create("complete");

	public BlockModularDoor(Block.Properties props)
	{
		super(props);
//		super(Material.IRON);
//		this.setDefaultState(this.blockState.getBaseState().withProperty(OPEN, false).withProperty(COMPLETE, true));
//		setCreativeTab(FPMain.tab_deco);
	}

//	@Override
//	public IBlockState getStateFromMeta(int meta)
//	{
//		return this.getDefaultState().with(FACING, EnumFacing.byIndex(meta%6)).withProperty(OPEN, meta>=6).withProperty(COMPLETE, false);
//	}
//
//	@Override
//	public int getMetaFromState(IBlockState state)
//	{
//		return (state.get(OPEN)? 6 : 0) + state.get(FACING).getIndex();
//	}
//
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(OPEN, COMPLETE);
	}

	VoxelShape[][] cache = new VoxelShape[6][32];


	@Override
	public VoxelShape getShape(BlockState state, BlockGetter w, BlockPos pos, CollisionContext sel)
	{
		if(state.getValue(COMPLETE) && !state.getValue(OPEN))
		{
			return Shapes.block();
		}
		if(w.getBlockEntity(pos) instanceof TileEntityModularDoor door)
		{
			if(door!=null)
			{
				if(door.getSize()==0)
				{
					return Shapes.empty();
				}
				else
				{
					int i1 = state.getValue(FACING).get3DDataValue();
					int i2 = (int) (door.getSize() * 2F)-1;
					if(cache[i1][i2]==null)
					{
						float x1=0F,y1=0F,z1=0F,x2=1F,y2=1F,z2=1F;
						Direction face = state.getValue(FACING);
						float h = Math.max(0.01F, 1F - door.getSize()/16F);

						if(face.getAxisDirection()==AxisDirection.POSITIVE)
						{
							x2 -= h * face.getStepX();
							y2 -= h * face.getStepY();
							z2 -= h * face.getStepZ();
						}
						else
						{
							x1 -= h * face.getStepX();
							y1 -= h * face.getStepY();
							z1 -= h * face.getStepZ();
						}
						cache[i1][i2] = Shapes.box(x1,y1,z1,x2,y2,z2);
					}
	//				List<EntityLivingBase> list = source.getEntitiesWithinAABB(EntityLivingBase.class, cache[i1][i2].offset(pos));
	//				if(list.size()>0)
	//				{
	//					EnumFacing face = state.get(FACING);
	//					float x = 0.01F * face.getXOffset();
	//					float y = 0.015F * face.getYOffset();
	//					float z = 0.01F * face.getZOffset();
	//
	//					for(EntityLivingBase base : list)
	//					{
	//						base.getMotion().x += x;
	//						base.getMotion().y += y;
	//						base.getMotion().z += z;
	//					}
	//				}
					return cache[i1][i2];
				}
			}
		}
		return Shapes.block();
	}


	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player playerIn, InteractionHand hand, BlockHitResult hit)
	{
		if(w.isClientSide)
			return InteractionResult.SUCCESS;

		BlockPos pointer = searchForMovingBlock(w, pos, state);
		BlockState pointerVal = w.getBlockState(pointer);

		if(pointerVal.getValue(OPEN))
		{
			startClosing(w, pointer, pointerVal);
			pointerVal = w.getBlockState(pointer);

			BlockPos other = getOpposite(w, pointer, pointerVal);
			if(other!=null)
			{
				BlockState otherVal = w.getBlockState(other);
				startClosing(w, other, otherVal);
			}
		}
		else
		{
			startOpening(w, pointer, pointerVal);
			pointerVal = w.getBlockState(pointer);

			BlockPos other = getOpposite(w, pointer, pointerVal);
			if(other!=null)
			{
				BlockState otherVal = w.getBlockState(other);
				startOpening(w, other, otherVal);
			}
		}
		return InteractionResult.SUCCESS;
	}

	/**
	 * This searches for the block that is actualy moving. (to stop,start it)
	 */
	private BlockPos searchForMovingBlock(Level w, BlockPos pos, BlockState state)
	{
		Direction face = state.getValue(FACING);
		if(state.getValue(OPEN))
		{
			face = face.getOpposite();
		}

		BlockPos pointer = pos;
		BlockState pointerVal = state;

		while(pointerVal.getValue(COMPLETE))
		{
			pointerVal = w.getBlockState(pointer.relative(face));
			if(pointerVal.getBlock()!=this || pointerVal.getValue(FACING)!=state.getValue(FACING))
			{
				pointerVal = w.getBlockState(pointer);
				break;
			}
			pointer = pointer.relative(face);
		}
		return pointer;
	}

	private BlockPos getOpposite(Level w, BlockPos pos, BlockState state)
	{
		Direction face = state.getValue(FACING);
		BlockPos pointer = pos;
		BlockState pointerVal = state;

		while(pointerVal.getBlock() == this && pointerVal.getValue(FACING)==face)
		{
			pointer = pointer.relative(face);
			pointerVal = w.getBlockState(pointer);
		}

		if(pointerVal.getBlock()==this && pointerVal.getValue(FACING)==face.getOpposite())
		{
			return searchForMovingBlock(w, pointer, pointerVal);
		}

		return null;
	}

	@Override
	public void tick(BlockState state, ServerLevel w, BlockPos pos, Random random)
	{
		TileEntityModularDoor door = (TileEntityModularDoor) w.getBlockEntity(pos);
		door.update(state);
	}

	public void onMovingFinished(Level w, BlockPos pos, BlockState state)
	{
		if(state.getValue(OPEN))
		{
			BlockPos face = pos.relative(state.getValue(FACING), -1);
			BlockState holder = w.getBlockState(face);
			if(holder.getBlock()==this)
			{
				startOpening(w, face, holder);
				makeInvisible(w, pos, state);
			}
		}
		else
		{
			BlockPos face = pos.relative(state.getValue(FACING), 1);
			BlockState holder = w.getBlockState(face);
			if(holder.getBlock()==this && holder.getValue(FACING)==state.getValue(FACING))
			{
				startClosing(w, face, holder);
			}
		}
	}

	public void startOpening(Level w, BlockPos pos, BlockState state)
	{
		if(state.getValue(OPEN)==false)
		{
			TileEntityModularDoor door = (TileEntityModularDoor) w.getBlockEntity(pos);
			door.open();
			w.scheduleTick(pos, this, 1);

			tryTriggerNeighbors(w, pos, state, true);
		}
	}

	public void startClosing(Level w, BlockPos pos, BlockState state)
	{
		if(state.getValue(OPEN)==true)
		{
			TileEntityModularDoor door = (TileEntityModularDoor) w.getBlockEntity(pos);
			door.close();
			w.scheduleTick(pos, this, 1);

			tryTriggerNeighbors(w, pos, state, false);
		}
	}

	public void tryTriggerNeighbors(Level w, BlockPos pos, BlockState state, boolean open)
	{
		Direction.Axis axis = state.getValue(FACING).getAxis();
		for(Direction faces : FacingUtil.VALUES)
		{
			if(faces.getAxis()!=axis)
			{
				BlockPos nextPos = pos.relative(faces);
				BlockState other = w.getBlockState(nextPos);
				if(other.getBlock() == this)
				{
					if(other.getValue(OPEN)!=open)
					{
						if(open)
						{
							startOpening(w, nextPos, other);
						}
						else
						{
							startClosing(w, nextPos, other);
						}
					}
				}
			}
		}

	}

	public void makeInvisible(Level w, BlockPos pos, BlockState state)
	{
		TileEntityModularDoor door = (TileEntityModularDoor) w.getBlockEntity(pos);
		door.invisible();
	}

	@Override
	public RenderShape getRenderShape(BlockState state)
	{
		return state.getValue(COMPLETE)&&!state.getValue(OPEN) ? RenderShape.MODEL : RenderShape.INVISIBLE;
	}


	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		Direction face = context.getNearestLookingDirection().getOpposite();
		return super.getStateForPlacement(context).setValue(FACING, face);
	}

	@Override
	public BlockEntityType<TileEntityModularDoor> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.MODULAR_DOOR;
	}
}
