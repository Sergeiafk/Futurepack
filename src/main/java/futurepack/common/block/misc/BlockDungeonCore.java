package futurepack.common.block.misc;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class BlockDungeonCore extends Block implements EntityBlock
{

	public BlockDungeonCore(Block.Properties props)
	{
		super(props);
//		super(Material.IRON);	
//		setCreativeTab(FPMain.tab_deco);
	}
	
	@Override
	public RenderShape getRenderShape(BlockState state)
    {
        return RenderShape.MODEL;
    }
	
	@Override
	public void neighborChanged(BlockState state, Level w, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving)
	{
		if(w.hasNeighborSignal(pos))
		{
			removeDungeonProtection(w, pos);
		}
	}
	
	public static void removeDungeonProtection(Level w, BlockPos pos)
	{
		TileEntityDungeonCore tile = (TileEntityDungeonCore) w.getBlockEntity(pos);
		tile.removeDungeonProtection();
	}
	
	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new TileEntityDungeonCore(pos, state);
	}
}
