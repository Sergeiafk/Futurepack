package futurepack.common.block.misc;

import java.util.List;
import java.util.Random;

import com.google.common.base.Predicates;

import futurepack.common.block.BlockHoldingTile;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperItems;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.protocol.game.ClientboundCustomSoundPacket;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;

public class BlockRsTimer extends BlockHoldingTile
{
	public static final IntegerProperty TIME = IntegerProperty.create("time", 0, 8);
	
	public BlockRsTimer(Block.Properties props)
	{
		super(props);
	}

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(TIME);
	}
	
	private int getTime(BlockState state)
	{
		return state.getValue(TIME);
	}
	
	@Override
	public void tick(BlockState state, ServerLevel w, BlockPos pos, Random random)
	{
		TileEntityRsTimer timer = (TileEntityRsTimer) w.getBlockEntity(pos);
		
		int next, current;
		current = next = getTime(state);	
		next++;
		next %= 9;
		int delay = timer.getDelay(next);

		state = state.setValue(TIME, next);
		w.setBlockAndUpdate(pos, state);
		 
		if(next == 8)
		{
			ClientboundCustomSoundPacket okt = new ClientboundCustomSoundPacket(HelperItems.getRegistryName(SoundEvents.LEVER_CLICK), SoundSource.BLOCKS, new Vec3(pos.getX()+0.5F, pos.getY()+0.5F, pos.getZ()+0.5F), 0.4F, 0.4F);
			
			List<ServerPlayer> clients = w.getEntitiesOfClass(ServerPlayer.class, new AABB(pos.offset(-15, -15, -15), pos.offset(15,15,15)));
			
			clients.stream().map( mp -> mp.connection).filter(Predicates.notNull()).forEach(c -> c.send(okt));
			
		}
		if(!w.getBlockTicks().hasScheduledTick(pos, this))
			w.scheduleTick(pos, this, delay);
	}
	
	@Override
	public void onPlace(BlockState state, Level w, BlockPos pos, BlockState oldState, boolean isMoving)
	{
		super.onPlace(state, w, pos, oldState, isMoving);
		if(oldState.getBlock()!=this)
			w.scheduleTick(pos, this, 0);
		
		if(w.isClientSide)
		{
			throw new RuntimeException();
		}
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.RS_TIMER.openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}
	
	
	
	@Override
	public boolean canConnectRedstone(BlockState state, BlockGetter world, BlockPos pos, Direction side)
	{
		return true;
	}
	
	@Override
	public boolean isSignalSource(BlockState state)
    {
        return getTime(state) == 8;
    }
    
    @Override
    public int getSignal(BlockState state, BlockGetter blockAccess, BlockPos pos, Direction side)
    {
    	return getTime(state) == 8 ? 15 : 0;
    }

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new TileEntityRsTimer(pos, state);
	}
	
	
}
