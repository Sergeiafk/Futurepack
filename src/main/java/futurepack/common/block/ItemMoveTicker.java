package futurepack.common.block;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.google.common.base.Predicate;

import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageItemMove;
import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.network.PacketDistributor;
import net.minecraftforge.network.PacketDistributor.TargetPoint;

public class ItemMoveTicker 
{
	private static ArrayList<WeakReference<ItemEntity>> inprogress = new ArrayList<WeakReference<ItemEntity>>();
	
	private Vec3 goal;
	private Collection<ItemEntity> item;
	private Vec3 motion;
	private Vec3 position;
	private Level w;
	
	public ItemMoveTicker(Level w , Vec3 toGo, Vec3 start)
	{		
		this.w=w;
		goal = toGo;
		position = start;
		findEntities(w, start);
		
		FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.NEAR.with(() -> new TargetPoint(start.x, start.y, start.z, 20D, w.dimension())), new MessageItemMove(w, toGo, start, item));
		
		updateMotion();
		
		MinecraftForge.EVENT_BUS.register(this);
//		System.out.println(w);
	}
	
	public ItemMoveTicker(Level w , Vec3 toGo, Vec3 start, int[] ids)
	{
		this.w=w;
		goal = toGo;
		position = start;
		item = new ArrayList<ItemEntity>(ids.length);
		for(int i=0;i<ids.length;i++)
		{
			ItemEntity e = (ItemEntity) w.getEntity(ids[i]);
			item.add(e);
		}
		updateMotion();
		MinecraftForge.EVENT_BUS.register(this);
//		System.out.println(w);
	}
	
	private void findEntities(Level w, Vec3 pos)
	{
		item = w.getEntitiesOfClass(ItemEntity.class, new AABB(new BlockPos(pos.add(-1,-1,-1)), new BlockPos(pos.add(1,1,1))), new Predicate<ItemEntity>()
		{
			@Override
			public boolean apply(ItemEntity input)
			{
				if(inprogress.isEmpty())
					return true;
				
				Iterator<WeakReference<ItemEntity>> iter = inprogress.iterator();
				while(iter.hasNext())
				{
					WeakReference<ItemEntity> wref = iter.next();
					ItemEntity ei = wref.get();
					if(ei!=null && input==ei)
					{
						return false;
					}
					else
					{
						iter.remove();
					}
				}
				return true;
			}
		});
		for(ItemEntity it : item)
		{
			inprogress.add(new WeakReference<ItemEntity>(it));
		}
	}
	
	@SubscribeEvent
	public void tick(TickEvent.WorldTickEvent event)
	{
//		if(event.world == w)
		{
			//if(event.phase==Phase.START)
			{
				updateMotion();
				position = position.add(motion.x*0.01, motion.y*0.01, motion.z*0.01);
				updatePos();	
				if(position.distanceToSqr(goal)<=1)
				{
					Iterator<WeakReference<ItemEntity>> iter = inprogress.iterator();
					while(iter.hasNext())
					{
						WeakReference<ItemEntity> wref = iter.next();
						ItemEntity ei = wref.get();
						if(ei!=null)
						{
							if(item.remove(ei))
							{
								iter.remove();
							}
						}
						else
						{
							iter.remove();
						}
						item.clear();
					}
				}
			}
//			System.out.println("ItemMoveTicker.tick()");
			if(item.isEmpty())
			{
				MinecraftForge.EVENT_BUS.unregister(this);
			}
			
	//		position = goal;
	//		updatePos();
	//		MinecraftForge.EVENT_BUS.unregister(this);
		}
		
	}
	
	private void updatePos()
	{
		Entity e;
		Iterator<ItemEntity> iter = item.iterator();
		while(iter.hasNext())
		{
			e = iter.next();
			if(e==null)
			{
				iter.remove();
				continue;
			}
			if(e.isAlive()==false)
			{
				iter.remove();
				continue;
			}
			e.setDeltaMovement(motion.scale(0.01));
			
			e.setPos(position.x, position.y, position.z);
		}
		w.addParticle(ParticleTypes.MYCELIUM, position.x, position.y, position.z, motion.x, motion.y, motion.z);
	}
	
	private void updateMotion()
	{
		Vec3 vec1 = goal.subtract(position);
		double length = vec1.length();	
		Vec3 motionBase = vec1.normalize();
		double x = Math.sin(Math.PI * length);
		double y = Math.cos(Math.PI * length);
		motion = motionBase.add(motionBase.z * x, y, -motionBase.x * x);
	}
}
