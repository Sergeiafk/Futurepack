package futurepack.common.block.logistic;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.api.interfaces.IBlockValidator;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPBlockSelector;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.BucketPickup;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.FluidAttributes;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.IFluidBlock;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler.FluidAction;
import net.minecraftforge.fluids.capability.templates.FluidTank;

public class TileEntityFluidIntake extends FPTileEntityBase implements ITileServerTickable
{
	private FluidTank tank;
	private LazyOptional<IFluidHandler> fluidOpt;
	private List<ParentCoords> list;
	private int i = 999;
	
	public TileEntityFluidIntake(BlockEntityType<? extends TileEntityFluidIntake> type, BlockPos pos, BlockState state)
	{
		super(type, pos, state);
		tank = new FluidTank(20 * FluidAttributes.BUCKET_VOLUME);		
	}
	
	public TileEntityFluidIntake(BlockPos pos, BlockState state)
	{
		this(FPTileEntitys.FLUID_INTAKE, pos, state);
	}
	
	@Override
	public void setRemoved() 
	{
		if(fluidOpt!=null)
			fluidOpt.invalidate();
		super.setRemoved();
	}
	
	private boolean isValidBlock(BlockPos pos)
	{
		if(pos==null)
			return false;
		
		BlockState bs = level.getBlockState(pos);
		FluidState fl = bs.getFluidState();
		Block b = bs.getBlock();
		
		if(fl.isSource())
		{
			return true;
		}
		else if(b instanceof IFluidBlock)
		{
			IFluidBlock fb = (IFluidBlock) b;
			
			if(fb.canDrain(level, pos)) 
			{
				return true;
			}
		}
		return false;
	}
	
		
	private boolean collectFluidStack(FluidStack fs)
	{
		if(fs != null)
		{
			if(tank.fill(fs, FluidAction.SIMULATE) == fs.getAmount())
			{
				tank.fill(fs, FluidAction.EXECUTE);
				return true;
			}	
		}
		return false;
	}
	
	private boolean collectFluidBlock(BlockPos target) 
	{
		if(target==null)
			return false;
		
		BlockState bs = level.getBlockState(target);
		FluidState fl = bs.getFluidState();
		Block b = bs.getBlock();
		
		if(fl.isSource())
		{
			FluidStack fs = new FluidStack(fl.getType(), FluidAttributes.BUCKET_VOLUME);
			if(collectFluidStack(fs))
			{
				((BucketPickup)b).pickupBlock(level, target, bs);
				return true;
			}
		}
		else if(b instanceof IFluidBlock)
		{
			IFluidBlock fb = (IFluidBlock) b;
			
			if(fb.canDrain(level, target)) 
			{
				FluidStack fs = fb.drain(level, target, FluidAction.SIMULATE);
				
				if(collectFluidStack(fs))
				{
					fb.drain(level, target, FluidAction.EXECUTE);
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public void tickServer(Level level, BlockPos pPos, BlockState pState)
	{
		//tank.fillInternal(new FluidStack(HelperFluid.OLD_LAVA, 1000), true);
		if(i > 20)
		{
			i = 1;
			
			if(tank.getFluidAmount()+FluidAttributes.BUCKET_VOLUME >= tank.getCapacity())
				return;
				
			FPBlockSelector sel = new FPBlockSelector(level, new FluidSelector(worldPosition));
			sel.selectBlocks(worldPosition);
					
			list = (List<ParentCoords>) (sel.getValidBlocks(null/*validatorFluid*/));
					
			Collections.sort(list, new ComperatorFluidSort(worldPosition));
						
		}
			
		if(list!=null)
		{
			int id = list.size() - i;
			while(id >= 0)
			{
				ParentCoords pc = list.get(id);
					
				if(isValidBlock(pc))
				{
					if(collectFluidBlock(pc))
					{
							
						break;
					}
				}
				list.set(id, null);
				id--;
			}
		}
		
		i++;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY==capability)
		{
			if(fluidOpt!=null)
			{
				return (LazyOptional<T>) fluidOpt;
			}
			else
			{
				fluidOpt = LazyOptional.of(() -> tank);
				fluidOpt.addListener(p -> fluidOpt = null);
				return (LazyOptional<T>) fluidOpt;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		CompoundTag tank = new CompoundTag();
		this.tank.writeToNBT(tank);
		nbt.put("tank", tank);
		return super.writeDataUnsynced(nbt);
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		CompoundTag tank = nbt.getCompound("tank");
		this.tank.readFromNBT(tank);
		super.readDataUnsynced(nbt);
	}

	
	private static class FluidSelector implements IBlockSelector
	{	
		public int maxrange;
		public BlockPos origin;
		public Fluid target;

		public FluidSelector(BlockPos pos) 
		{
			maxrange = 32;
			origin = pos;
			target = null;
		}

		@Override
		public boolean isValidBlock(Level w, BlockPos pos, Material m, boolean dia, ParentCoords parent)
		{
			if(dia)
				return false;
			
			if(origin.equals(pos))
				return true;
			
			if(pos.getY() < origin.getY())
				return false;
			
			if(parent != null && parent.getY() > pos.getY())
				return false;
			
			BlockPos o = new BlockPos(origin.getX(), 0, origin.getZ()); 
			BlockPos p = new BlockPos(pos.getX(),    0, pos.getZ()); 
			
			if(o.distSqr(p) > maxrange * maxrange)
				return false; 
			
			BlockState bs = w.getBlockState(pos);
			FluidState fl = bs.getFluidState();
			
			Block b = bs.getBlock();
			
			if(target == null)
			{
				if(fl.isSource() && b instanceof BucketPickup)
					target = fl.getType();
				else if(b instanceof IFluidBlock)
					target = ((IFluidBlock) b).getFluid();
			}
			
			if(fl.isSource() && b instanceof BucketPickup)
				return target == fl.getType();
			 
			return (b instanceof IFluidBlock && target == ((IFluidBlock) b).getFluid());
		}
		
		@Override
		public boolean canContinue(Level w, BlockPos pos, Material m, boolean dia, ParentCoords parent)
		{
			return true;
		}
	};
	
	public static class ComperatorFluidSort implements Comparator<ParentCoords>
	{
		public final BlockPos origin;
		
        public ComperatorFluidSort(BlockPos pos) 
        {
			origin = pos;
		}

		@Override
        public int compare(ParentCoords o1, ParentCoords o2) 
        {
        	int dy = o2.getY() - o1.getY();
        	
        	if(dy == 0)
        		return (int) (this.origin.distSqr(o1) - this.origin.distSqr(o2));
        	else
        		return -dy;
        }
    };
    
	private static IBlockValidator validatorFluid = new IBlockValidator()
	{
		@Override
		public boolean isValidBlock(Level w, ParentCoords pos)
		{
			BlockState bs = w.getBlockState(pos);
			FluidState fl = bs.getFluidState();
			Block b = bs.getBlock();
			
			if(fl.isSource() && b instanceof BucketPickup)
			{
				return true;
			}
			else if(b instanceof IFluidBlock)
			{
				IFluidBlock fb = (IFluidBlock) b;
				
				if(fb.canDrain(w, pos)) 
				{
					return true;
				}
			}
			return false;
		}
		
	};

}
