package futurepack.common.block.logistic;

import java.util.List;

import javax.annotation.Nullable;

import futurepack.api.capabilities.CapabilitySupport;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.ISupportStorage;
import futurepack.common.FPTileEntitys;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraftforge.common.util.LazyOptional;

public class BlockPipeSupport extends BlockPipeBase<TileEntityPipeSupport> 
{
	public static final BooleanProperty POWERED = BlockStateProperties.POWERED;
	
	protected BlockPipeSupport(Properties props) 
	{
		super(props);
	}

	@Override
	public boolean hasSpecial() 
	{
		return true;
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(POWERED);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, BlockGetter worldIn, List<Component> tooltip, TooltipFlag flagIn) 
	{
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslatableComponent("tooltip.futurepack.block.conduct.support"));
	}
	
	@Override
	protected EnumSide getAdditionalConnections(TileEntityPipeBase pipe, @Nullable BlockEntity otherTile, Direction face) {
		
		if(otherTile != null)
		{
			LazyOptional<ISupportStorage> supOpt = otherTile.getCapability(CapabilitySupport.cap_SUPPORT, face.getOpposite());
			if(supOpt.isPresent())
			{
				ISupportStorage store = supOpt.orElseThrow(NullPointerException::new);
				if(store.getType()!=EnumEnergyMode.NONE)
				{	
					return EnumSide.CABLE;//CABLE because when this is called there is not an inventory to connect to just support
				}	
			}
		}
		
		return EnumSide.OFF;
	}

	@Override
	public BlockEntityType<TileEntityPipeSupport> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.PIPE_SUPPORT;
	}
}
