package futurepack.common.block.logistic;

import java.util.List;

import javax.annotation.Nullable;

import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.common.FPTileEntitys;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.util.LazyOptional;

public class BlockPipeNeon extends BlockPipeBase<TileEntityPipeNeon> 
{

	protected BlockPipeNeon(Properties props) 
	{
		super(props);
	}

	@Override
	public boolean hasSpecial() 
	{
		return true;
	}

	@Override
	public void appendHoverText(ItemStack stack, BlockGetter worldIn, List<Component> tooltip, TooltipFlag flagIn) 
	{
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslatableComponent("tooltip.futurepack.block.conduct.neon", 500));
	}
	
	@Override
	protected EnumSide getAdditionalConnections(TileEntityPipeBase pipe, @Nullable BlockEntity otherTile, Direction face) {
		
		if(otherTile != null)
		{
			LazyOptional<INeonEnergyStorage> neonOpt = otherTile.getCapability(CapabilityNeon.cap_NEON, face.getOpposite());
			if(neonOpt.isPresent())
			{
				INeonEnergyStorage p = neonOpt.orElseThrow(NullPointerException::new);
				if(p.getType()!=EnumEnergyMode.NONE)
				{	
					return EnumSide.CABLE;//CABLE because when this is called there is not an inventory to connect to just neon
				}	
			}
		}
		
		return EnumSide.OFF;
	}

	@Override
	public BlockEntityType<TileEntityPipeNeon> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.PIPE_NEON;
	}
	
}
