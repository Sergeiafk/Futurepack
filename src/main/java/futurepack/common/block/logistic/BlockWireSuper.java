package futurepack.common.block.logistic;

import futurepack.common.FPTileEntitys;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class BlockWireSuper extends BlockWireBase<TileEntityWireSuper> 
{

	public BlockWireSuper(Properties props) 
	{
		super(props);
	}

	@Override
	protected int getMaxNeon() 
	{
		return 1000;
	}

	@Override
	public BlockEntityType<TileEntityWireSuper> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.WIRE_SUPER;
	}

}
