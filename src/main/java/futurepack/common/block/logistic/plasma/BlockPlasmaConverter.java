package futurepack.common.block.logistic.plasma;

import java.util.function.Supplier;

import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperBoundingBoxes;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockPlasmaConverter<T extends BlockEntity & ITileServerTickable> extends BlockPlasmaTransferPipe<T>
{

	private VoxelShape full    = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,0,16,16,16}}, 0, 0);
	
	
	public BlockPlasmaConverter(Properties properties, Supplier<BlockEntityType<T>> sup) 
	{
		super(properties, sup);
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand handIn, BlockHitResult hit) 
	{
		if(HelperResearch.isUseable(pl, state))
		{
			FPGuiHandler.PLASMA_CONVERTER.openGui(pl, pos);
			return InteractionResult.SUCCESS;
		}
		return InteractionResult.PASS;
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) 
	{
		return full;
	}
}
