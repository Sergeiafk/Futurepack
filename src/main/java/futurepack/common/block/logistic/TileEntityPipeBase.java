package futurepack.common.block.logistic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import com.google.common.base.Predicate;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.FacingUtil;
import futurepack.api.ParentCoords;
import futurepack.api.capabilities.CapabilityLogistic;
import futurepack.api.capabilities.ILogisticInterface;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.api.interfaces.IBlockValidator;
import futurepack.api.interfaces.tilentity.ITileClientTickable;
import futurepack.api.interfaces.tilentity.ITileHologramAble;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPBlockSelector;
import futurepack.common.FPConfig;
import futurepack.common.FPSelectorHelper;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageRendering;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperHologram;
import futurepack.depend.api.helper.HelperInventory;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.Connection;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;

public class TileEntityPipeBase extends FPTileEntityBase implements  ITileHologramAble, ITileServerTickable, ITileClientTickable
{
	private final NonNullList<ItemStack> items = NonNullList.withSize(1, ItemStack.EMPTY);
	private List<ItemStack> refind = new ArrayList<>(); 
	private List<ItemPath> todo = new ArrayList<>();
	
	protected boolean[] lock = new boolean[]{false,false,false,false,false,false};
	protected boolean[] ingoreLockedSub = new boolean[]{false,false,false,false,false,false};
	
	public int time = 0;
	
	private BlockState overlay;
	
	public IBlockSelector selector = new IBlockSelector()
	{
		
		@Override
		public boolean isValidBlock(Level w, BlockPos pos, Material m, boolean dia, ParentCoords p)
		{
			if(dia)
				return false;	
			
			BlockEntity t = w.getBlockEntity(pos);
			
			Direction face = Direction.UP;
			if(p !=null)
			{
				TileEntityPipeBase parentPipe = (TileEntityPipeBase) w.getBlockEntity(p);
				int x = p.getX() - pos.getX();
				int y = p.getY() - pos.getY();
				int z = p.getZ() - pos.getZ();
				for(int i=0;i<FacingUtil.VALUES.length;i++)
				{
					Direction dir = FacingUtil.VALUES[i];
					if(dir.getStepX() == x && dir.getStepY()==y && dir.getStepZ()==z)
					{
						if(parentPipe.isSideLocked(dir.getOpposite()))
						{
							return false;
						}
						if(t instanceof TileEntityPipeBase)
						{
							return !((TileEntityPipeBase)t).isSideLocked(dir);
						}
						break;
					}
				}
				face = FacingUtil.getSide(pos, p);
			}
			
			
//			if(t instanceof TileEntityHopper)
//			{
//				return pos.up().equals(p);
//			}
			
			return HelperInventory.getHandler(t, face) !=null;
		}
		
		@Override
		public boolean canContinue(Level w, BlockPos pos, Material m, boolean dia, ParentCoords parent)
		{
			return (w.getBlockEntity(pos) instanceof TileEntityPipeBase);
		}
	};
	
	public IBlockValidator sorter = new IBlockValidator() 
	{	
		@Override
		public boolean isValidBlock(Level w, ParentCoords pos)
		{
			BlockEntity t = w.getBlockEntity(pos);
			if(!(t instanceof TileEntityPipeBase))
			{
				if(pos!=null)
				{
					TileEntityPipeBase pipe = (TileEntityPipeBase) w.getBlockEntity(pos.getParent());
					if(pipe==null)
					{
						return false;
					}
					if(pipe!=TileEntityPipeBase.this && !pipe.getStackWaiting().isEmpty()) //This makes sure the pipe before the container is not blocked by an item
					{
						return false;
					}
				}
				return true;
			}
			return false;
		}
	};
	
	private LazyOptional<IItemHandler>[] itemOpt;
	private LazyOptional<ILogisticInterface>[] logisticOpt;
	
	@SuppressWarnings("unchecked")
	public TileEntityPipeBase(BlockEntityType<? extends TileEntityPipeBase> type, BlockPos pos, BlockState state)
	{
		super(type, pos, state);
		itemOpt = new LazyOptional[6];
		logisticOpt = new LazyOptional[6];
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(facing==null)
			return super.getCapability(capability, facing);
		
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(itemOpt[facing.get3DDataValue()]!= null)
			{
				return (LazyOptional<T>) itemOpt[facing.get3DDataValue()];
			}
			else
			{
				itemOpt[facing.get3DDataValue()] = LazyOptional.of(() -> new ItemHandlerImpl(facing));
				itemOpt[facing.get3DDataValue()].addListener(p -> itemOpt[facing.get3DDataValue()] = null);
				return (LazyOptional<T>) itemOpt[facing.get3DDataValue()];
			}
		}
		if(capability == CapabilityLogistic.cap_LOGISTIC)
		{
			if(logisticOpt[facing.get3DDataValue()]!= null)
			{
				return (LazyOptional<T>) logisticOpt[facing.get3DDataValue()];
			}
			else
			{
				logisticOpt[facing.get3DDataValue()] = LazyOptional.of(() -> new LogisticWrapper(facing));
				logisticOpt[facing.get3DDataValue()].addListener(p -> logisticOpt[facing.get3DDataValue()] = null);
				return (LazyOptional<T>) logisticOpt[facing.get3DDataValue()];
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(itemOpt);
		HelperEnergyTransfer.invalidateCaps(logisticOpt);
		super.setRemoved();
	}
	
	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState pState)
	{
		tick();
	}
	
	@Override
	public void tickClient(Level pLevel, BlockPos pPos, BlockState pState)
	{
		tick();
	}
	
	
	private void tick()
	{
		cashedTargets = null;
		
		tryRefnd();
		
		addItems();
		
		pushItemsInPipe();
		
		if(items.get(0).isEmpty() && !refind.isEmpty())
		{
			tryRefnd();
		}
		
		if(time<=0)
		{
			time=20;
			FPPacketHandler.sendTileEntityPacketToAllClients(this,  new Predicate<ServerPlayer>() //getEntitiesWithinAABBExcludingEntity
			{			
				@Override
				public boolean apply(ServerPlayer mp)
				{			
					boolean[] b =  FPPacketHandler.map.get(mp.getGameProfile().getId());
					if(b==null || b[0])
					{
						return true;
					}
					return false;
				}
			}, 20);
		}
		time--;
	}
	
	
	private void tryRefnd()
	{
		if(!refind.isEmpty())
		{
			compressList();
			
			ItemStack it = refind.get(0);
			if(items.get(0).isEmpty())
			{
				items.set(0, it.copy());
				refind.remove(0);
			}
			else if(items.get(0).sameItem(it) && ItemStack.tagMatches(it, items.get(0)) && items.get(0).getCount() < 64)
			{
				int add = Math.min(it.getCount(), 64 - items.get(0).getCount());
				items.get(0).grow(add);
				it.shrink(add);
				
				if(it.getCount()<=0)
					refind.remove(0);
			}
		}
	}
	
	private void addItems()
	{
		if(!items.get(0).isEmpty() && !level.isClientSide)
		{
			items.set(0, addItems(items.get(0), null, false));
		}	
	}
	
	private ItemStack addItems(ItemStack it, Direction side, boolean simulate)
	{
		if(!it.isEmpty() && !level.isClientSide)
		{			
			List<ParentCoords> list = getValidTargets((ServerLevel) level);
			//Hauptschleife:
			for(ParentCoords c : list)
			{
				Direction face = Direction.UP;
				ParentCoords p = c.getParent();
				
				if(p!=null)
				{
					face = FacingUtil.getSide(c,p);
				}
				
				
				IItemHandler handler = HelperInventory.getHandler(level.getBlockEntity(c), face);
					
				if(side!=null && c.equals(worldPosition.relative(side)))
					continue;	
				
				ItemStack result = ItemHandlerHelper.insertItem(handler, it, true);
				
				if(result==it)
					continue;
					
				
				BlockPos cc;
				if(side==null)
					cc = new BlockPos(worldPosition);
				else
					cc = worldPosition.relative(side);
				
				
				if(result.isEmpty())
				{
					if(!simulate)
					{
						addItemPath(it.copy(), c, cc);
						time=0;
					}				
					it = ItemStack.EMPTY;				
					break;
				}
				else
				{
					ItemStack itemS = it.copy();
					itemS.shrink(result.getCount());
					if(itemS.getCount()>0)
					{
						if(!simulate)
						{
							time=0;
							addItemPath(itemS, c, cc);
						}
						it =  result.copy();
					}
				}
			}
		}	
		return it;
	}
	
	private List<ParentCoords> cashedTargets = null;
	
	private List<ParentCoords> getValidTargets(ServerLevel world)
	{
		if(cashedTargets!=null)
			return cashedTargets;
		
		FPBlockSelector sel = FPSelectorHelper.getSelectorSave(world, worldPosition, selector, true); //tileEntity only available in main thread
		List<ParentCoords> list = (List<ParentCoords>) sel.getValidBlocks(sorter); //yeah this is vague
		
		Collections.shuffle(list);
		Collections.sort(list, new Comparator<ParentCoords>() 
		{
			@Override
			public int compare(ParentCoords arg0, ParentCoords arg1)
			{
				int a0 = arg0.getDepth();
				int a1 = arg1.getDepth();
				
				return a0 - a1;
			}
		});
		cashedTargets = list;
		return list;
	}
	
	private void pushItemsInPipe()
	{
		List<ItemPath> todo = new ArrayList<>(this.todo);
		Hauptschleife:
		for(ItemPath ip : todo)
		{
			if(ip==null || ip.itemInPipe==null || ip.itemInPipe.isEmpty())
			{
				this.todo.remove(ip);
				continue;
			}
			
			ip.next--;
			if(ip.next>0)
			{
				
				continue;
			}
			
			int s = ip.path.size();
			if(s>0)
			{
				BlockPos c = ip.path.get(s-1);
				BlockEntity t = level.getBlockEntity(c);
				if(t!=null && t instanceof TileEntityPipeBase)
				{
					ip.path.remove(s-1);
					ip.next=10;
					ip.from=new BlockPos(worldPosition);
					((TileEntityPipeBase)t).todo.add(ip);
					this.todo.remove(ip);
					continue;
				}
				else
				{
					refind.add(ip.itemInPipe);
					this.todo.remove(ip);
					continue;
				}
			}
			else if(!level.isClientSide)
			{
				BlockPos c = ip.target;
				Direction face = FacingUtil.getSide(c, worldPosition);
				BlockEntity t = level.getBlockEntity(c);
				
				IItemHandler handler = HelperInventory.getHandler(t, face);
				this.todo.remove(ip);				
				if(handler != null)
				{
					ItemStack result = ItemHandlerHelper.insertItem(handler, ip.itemInPipe, false);
					if(!result.isEmpty())
					{
						refind.add(result);
					}
					continue Hauptschleife;
				}
				refind.add(ip.itemInPipe);
				break;
			}
		}
	}
	
	private void compressList()
	{
		List<ItemStack> newList = new ArrayList<ItemStack>();
		Iterator<ItemStack> refind = this.refind.iterator();
		Iterator<ItemStack> list; 
		ItemStack toRefind;
		ItemStack current;
		HauptSchleife:
		while(refind.hasNext())
		{
			toRefind = refind.next();
			list = newList.iterator();
			while(list.hasNext())
			{
				current = list.next();
				if(ItemStack.isSame(toRefind, current) && ItemStack.tagMatches(toRefind, current) && current.getCount()<64)
				{
					current.grow(toRefind.getCount());
					continue HauptSchleife;
				}
			}
			newList.add(toRefind);
		}
		this.refind = newList;
	}
	
	private static long data=0;
	
	@Override
	public void onDataPacket(Connection net, ClientboundBlockEntityDataPacket pkt)
	{
		super.onDataPacket(net, pkt);
		
		if(level.isClientSide)
		{
			if(!FPConfig.CLIENT.renderPipeItems.get())
			{
				if(System.currentTimeMillis()-data>=1000)
				{
					System.out.println("[ERROR] After getting much render Packets, resend RenderConfig");
					FPPacketHandler.CHANNEL_FUTUREPACK.sendToServer(new MessageRendering(FPConfig.CLIENT.renderPipeItems.get(), FPConfig.CLIENT.doEntityEaterSync.get()));
					data = System.currentTimeMillis();
				}
				
			}
		}
	}
	

	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		
		if(!items.get(0).isEmpty())
		{
			CompoundTag tag = new CompoundTag();
			items.get(0).save(tag);
			nbt.put("item", tag);
		}
		
		ListTag l = new ListTag();
		for(int i=0;i<todo.size();i++)
		{
			CompoundTag tag = new CompoundTag();
			todo.get(i).write(tag);
			l.add(tag);
		}
		nbt.put("todo", l);
		
		l = new ListTag();
		for(int i=0;i<refind.size();i++)
		{
			CompoundTag tag = new CompoundTag();
			refind.get(i).save(tag);
			l.add(tag);
		}
		nbt.put("refind", l);
		
		for(int i=0;i<lock.length;i++)
		{
			nbt.putBoolean("locked"+i, lock[i]);
			nbt.putBoolean("lockedSub"+i, ingoreLockedSub[i]);
		}
		
		if(overlay!=null)
		{
			nbt.put("hologram", HelperHologram.toNBT(overlay));
		}
		return nbt;
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		items.set(0, ItemStack.EMPTY);
		todo.clear();
		refind.clear();
		super.readDataUnsynced(nbt);
		if(nbt.contains("item"))
		{
			CompoundTag tag =  nbt.getCompound("item");
			items.set(0, ItemStack.of(tag));
		}
		
		ListTag l = nbt.getList("todo", 10);
		for(int i=0;i<l.size();i++)
		{
			CompoundTag tag = l.getCompound(i);
			todo.add(ItemPath.createFromNBT(tag));
		}
		
		l = nbt.getList("refind", 10);
		for(int i=0;i<l.size();i++)
		{
			CompoundTag tag = l.getCompound(i);
			refind.add(ItemStack.of(tag));
		}
		for(int i=0;i<lock.length;i++)
		{
			lock[i] = nbt.getBoolean("locked"+i);
			ingoreLockedSub[i] = nbt.getBoolean("lockedSub"+i);
		}
		
		if(nbt.contains("hologram"))
		{
			overlay =HelperHologram.fromNBT(nbt.getCompound("hologram"));
		}
		else if(nbt.contains("BlockStateContainer"))
		{
			overlay =HelperHologram.fromNBT(nbt.getCompound("BlockStateContainer"));
		}
		
	}
	

	
	
	public ItemStack getStackWaiting() 
	{
		return items.get(0);
	}

	public static class ItemPath
	{
		public List<BlockPos> path = new ArrayList<BlockPos>();
		public BlockPos target;
		public BlockPos from;
		public ItemStack itemInPipe;
		public int next;
		
		private ItemPath(ItemStack item, ParentCoords path, BlockPos from)
		{
			this(item, path, from, new ArrayList<BlockPos>(path.getDepth()));
			ParentCoords p = path.getParent();
			while(p!=null)
			{
				this.path.add(p);
				p = p.getParent();
			}
		}
		
		/**
		 * 
		 * @param item the transportet Item
		 * @param target The position where this item is heading
		 * @param from The position where the item was insertet from
		 * @param path first entry is the <code>block</code> right next to target; the last entry is the block right next to <code>from</code>
		 */
		private ItemPath(ItemStack item, BlockPos target, BlockPos from, List<BlockPos> path)
		{
			this.itemInPipe = item.copy();
			this.target = target;
			this.path = path;
			this.next = 10;
			this.from = from;
		}
		
		public void write(CompoundTag nbt)
		{
			if(itemInPipe != null)
				itemInPipe.save(nbt);
			nbt.putInt("x", target.getX());
			nbt.putInt("y", target.getY());
			nbt.putInt("z", target.getZ());
			if(from!=null)
			{
				nbt.putInt("x2", from.getX());
				nbt.putInt("y2", from.getY());
				nbt.putInt("z2", from.getZ());
			}
			nbt.putInt("next", next);
			
			int[] xx = new int[path.size()];
			int[] yy = new int[path.size()];
			int[] zz = new int[path.size()];
			for(int i=0;i<path.size();i++)
			{
				xx[i] = path.get(i).getX();
				yy[i] = path.get(i).getY();
				zz[i] = path.get(i).getZ();
			}
			nbt.putIntArray("xx", xx);
			nbt.putIntArray("yy", yy);
			nbt.putIntArray("zz", zz);
		}
		
		public static ItemPath createFromNBT(CompoundTag nbt)
		{
			ItemStack it = ItemStack.of(nbt);
			ParentCoords target = new ParentCoords(new BlockPos(nbt.getInt("x"), nbt.getInt("y"), nbt.getInt("z")), null);
			BlockPos c = null;
			if(nbt.contains("x2") && nbt.contains("y2") && nbt.contains("z2"))
				c = new BlockPos(nbt.getInt("x2"), nbt.getInt("y2"), nbt.getInt("z2"));
			ItemPath tp = new ItemPath(it, target,c);
			int[] xx = nbt.getIntArray("xx");
			int[] yy = nbt.getIntArray("yy");
			int[] zz = nbt.getIntArray("zz");
			for(int i=0;i<Math.min(Math.min(xx.length, yy.length), zz.length); i++)
			{
				tp.path.add(new BlockPos(xx[i], yy[i], zz[i]));
			}
			tp.next = nbt.getInt("next");
			return tp;
		}
	}

	public ArrayList<ItemPath> getItems()
	{
		return new ArrayList<>(todo);
	}
	
	public ArrayList<ItemStack> getRefind()
	{
		return new ArrayList<>(refind);
	}

	@Override
	public AABB getRenderBoundingBox()
	{
		return new AABB(worldPosition.getX(),worldPosition.getY(),worldPosition.getZ(),worldPosition.getX()+1,worldPosition.getY()+1,worldPosition.getZ()+1);
	}


	public void toggelLock(Direction side)
	{
		if(!level.isClientSide)
		{
			lock[side.get3DDataValue()] = !lock[side.get3DDataValue()];
//			if(itemOpt[side.getIndex()]!=null)
//			{
//				itemOpt[side.getIndex()].invalidate();
//				itemOpt[side.getIndex()] = null;
//			}
			setChanged();
		}
	}

	public boolean isSideLocked(Direction side)
	{
		return lock[side.get3DDataValue()];
	}
	
	public void toggelIgnoreLockSub(Direction side)
	{
		if(!level.isClientSide)
		{
			ingoreLockedSub[side.get3DDataValue()] = !ingoreLockedSub[side.get3DDataValue()];
			BlockPos jkl = worldPosition.relative(side);
			BlockEntity t = level.getBlockEntity(jkl);
			if(t instanceof TileEntityPipeBase)
			{
				((TileEntityPipeBase)t).ingoreLockedSub[side.getOpposite().get3DDataValue()] = ingoreLockedSub[side.get3DDataValue()];
				t.setChanged();
			}
			setChanged();
		}
	}

	public boolean isIgnoreLockSub(Direction face)
	{
		int side = face.get3DDataValue();
		return ingoreLockedSub[side];
	}

	



	@Override
	public BlockState getHologram()
	{
		if(overlay!=null && overlay.getBlock() == getBlockState().getBlock())
		{
			setHologram(null);
			return this.getHologram();
		}
		return overlay;
	}


	@Override
	public boolean hasHologram() 
	{
		return overlay != null;
	}


	@Override
	public void setHologram(BlockState state)
	{
		overlay = state;
		
		this.level.setBlock(getBlockPos(), this.level.getBlockState(getBlockPos()).setValue(BlockPipeBase.HOLOGRAM, this.hasHologram()), 3);
	}
	
	@Override
	public void setChanged()
	{
		super.setChanged();
		BlockState state = level.getBlockState(worldPosition);
		level.sendBlockUpdated(worldPosition, state, state, 2);
		//TODO: back if not wrking!  world.HelperChunks.renderUpdate(w, pos);(pos, pos);
	}
	
	public class ItemHandlerImpl implements IItemHandler
	{
		final Direction side;
		
		public ItemHandlerImpl(Direction face)
		{
			side = face;
		}
		
		
		@Override
		public int getSlots()
		{
			return 1;
		}

		@Override
		public ItemStack getStackInSlot(int slot)
		{
			return ItemStack.EMPTY;
		}

		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
		{			
			return addItems(stack, side, simulate);
		}

		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate)
		{
			return ItemStack.EMPTY;
		}

		@Override
		public int getSlotLimit(int slot)
		{
			return 64;
		}
		
		@Override
		public boolean isItemValid(int slot, ItemStack stack) 
		{
			return true;
		}
		
	}
	
//	@Override
//	public boolean hasFastRenderer()
//	{
//		return HelperHologram.hasFastRenderer(this);
//	}

	public ItemPath addItemPath(ItemStack item, ParentCoords path, BlockPos from)
	{
		ItemPath p = new ItemPath(item, path, from);
		this.todo.add(p);
		return p;
	}
	
	public ItemPath addItemPath(ItemStack item, BlockPos target, BlockPos from, List<BlockPos> path)
	{
		ItemPath p = new ItemPath(item,target, from, path);
		this.todo.add(p);
		time = 0;
		return p;
	}
	
	public class LogisticWrapper implements ILogisticInterface
	{
		protected final Direction face;
		
		public LogisticWrapper(Direction face)
		{
			this.face = face;
		}
		

		@Override
		public EnumLogisticIO getMode(EnumLogisticType mode)
		{
			if(mode == EnumLogisticType.ITEMS)
			{
				return (TileEntityPipeBase.this.isSideLocked(face)) ? EnumLogisticIO.NONE : EnumLogisticIO.INOUT;
			}
			return null;
		}

		@Override
		public boolean setMode(EnumLogisticIO log, EnumLogisticType mode)
		{
			if(mode == EnumLogisticType.ITEMS)
			{
				if(log == EnumLogisticIO.INOUT && TileEntityPipeBase.this.isSideLocked(face))
				{
					TileEntityPipeBase.this.toggelLock(face);
					return true;
				}
				else if(log == EnumLogisticIO.NONE && !TileEntityPipeBase.this.isSideLocked(face))
				{
					TileEntityPipeBase.this.toggelLock(face);
					return true;
				}
			}
			return false;
		}

		@Override
		public boolean isTypeSupported(EnumLogisticType type)
		{
			return type == EnumLogisticType.ITEMS;
		}
		
	}

	public void onBlockDestroy(boolean hasCoustomNBTDrop) 
	{
		dropItem(level, getStackWaiting());
		
		for(ItemPath ip : getItems())
		{
			dropItem(level, ip.itemInPipe);
			ip.itemInPipe = null;
		}
		for(ItemStack ip : getRefind())
		{
			dropItem(level, ip);
		}
		
		//Cleanup in case we want to be dropped with nbt
		if(hasCoustomNBTDrop)
		{
			items.get(0).setCount(0);
			refind = new ArrayList<>(); 
			todo = new ArrayList<>();
			lock = new boolean[]{false,false,false,false,false,false};
			ingoreLockedSub = new boolean[]{false,false,false,false,false,false};
			time = 0;
			overlay = null;
		}
	}
		
	protected void dropItem(Level w, ItemStack it)
	{
		if(!w.isClientSide && it!=null)
		{
			ItemEntity item = new ItemEntity(w,worldPosition.getX()+0.5, worldPosition.getY()+0.5, worldPosition.getZ()+0.5, it);
			w.addFreshEntity(item);
		}
	}	
	
}
