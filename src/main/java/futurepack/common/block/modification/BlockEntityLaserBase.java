package futurepack.common.block.modification;

import java.util.function.Supplier;

import futurepack.api.interfaces.IBlockBothSidesTickingEntity;
import futurepack.common.block.BlockHoldingTile;
import futurepack.common.modification.EnumChipType;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockEntityLaserBase extends BlockHoldingTile implements IBlockBothSidesTickingEntity<TileEntityLaserBase<?>>
{
	public static final EnumProperty<Direction> ROTATION_VERTICAL = EnumProperty.create("rotation_vertical", Direction.class, Direction.Plane.VERTICAL);
			
	private final  Supplier<BlockEntityType<? extends TileEntityLaserBase<?>>> type;

	private static final VoxelShape shape_bottom = Shapes.or(Block.box(0, 0, 0, 16, 6, 16), Block.box(4, 0, 4, 12, 20, 12));
	private static final VoxelShape shape_ceiling   = Shapes.or(Block.box(0, 10, 0, 16, 16, 16), Block.box(4, -4, 4, 12, 16, 12));
	
//	static final String names[] = {"eater", "killer", "healer", "rocket"};
	
	protected BlockEntityLaserBase(Block.Properties props, Supplier<BlockEntityType<? extends TileEntityLaserBase<?>>> laser)
	{
		super(props);
		this.type = laser;
//		super(Material.IRON, 3);
//		setCreativeTab(FPMain.tab_maschiens);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext sel)
	{
		if(state.getValue(ROTATION_VERTICAL) == Direction.DOWN)
			return shape_ceiling;
		else if(state.getValue(ROTATION_VERTICAL) == Direction.UP)
			return shape_bottom;
		
		return super.getShape(state, worldIn, pos, sel);
	}
	
	@Override
	public RenderShape getRenderShape(BlockState state)
    {
        return RenderShape.INVISIBLE;
    }
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(ROTATION_VERTICAL);
	}
	

	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		return super.getStateForPlacement(context).setValue(ROTATION_VERTICAL, context.getClickedFace()!= Direction.DOWN ? Direction.UP : Direction.DOWN);
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		TileEntityLaserBase<?> base = (TileEntityLaserBase<?>) w.getBlockEntity(pos);
		if(base.getChipPower(EnumChipType.AI) > 0 && !w.isClientSide)
		{
			if(HelperResearch.canOpen(pl, state) && !w.isClientSide)
			{
				FPGuiHandler.LASER_EDIT.openGui(pl, pos);
				return InteractionResult.SUCCESS;
			}		
		}
		return InteractionResult.SUCCESS;
	}

	@Override
	public BlockEntityType<TileEntityLaserBase<?>> getTileEntityType(BlockState pState)
	{
		return (BlockEntityType<TileEntityLaserBase<?>>) type.get();
	}
}
