package futurepack.common.commands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.common.FPBlockSelector;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;

public class OreSearcher implements IBlockSelector
{
	private final BlockPos pos;
	public int maxrange = 100;
	
	private static final TagKey<Block> ORES = BlockTags.create(new ResourceLocation("forge:ores"));
	
	public OreSearcher(BlockPos start)
	{
		pos = start;
	}

	@Override
	public boolean isValidBlock(Level w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent)
	{
		if(w.isEmptyBlock(pos))
		{
			return true;
		}
		return isValid(w, pos);
	}

	@Override
	public boolean canContinue(Level w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent)
	{
		return (w.isEmptyBlock(pos) || isValid(w, pos) ) && this.pos.distSqr(pos) < maxrange * maxrange && this.pos.getY()>pos.getY();
	}
	
	private boolean isValid(Level w, BlockPos pos)
	{
		BlockState state = w.getBlockState(pos);
		return state.is(ORES);
	}

	private FPBlockSelector sel;
	private HashMap<BlockState, Integer> map;
	private ArrayList<ParentCoords> ores;
	
	public HashMap<BlockState,Integer> search(Level w)
	{
		if(sel == null)
		{
			sel = new FPBlockSelector(w, this);
			sel.selectBlocks(pos);
		}
		
		if(map==null)
		{
			map = new HashMap<BlockState, Integer>();
			Collection<ParentCoords> list = sel.getAllBlocks();
			ores = new ArrayList<>(list.size());
			for(ParentCoords pc: list)
			{
				if(isValid(w, pc))
				{
					ores.add(pc);
					BlockState state = w.getBlockState(pc);
					if(!map.containsKey(state))
					{
						map.put(state, 1);
					}
					else
					{
						map.put(state, map.get(state) +1);
					}
				}	
			}
			ores.trimToSize();
			ores.sort(new Comparator<ParentCoords>()
			{
				@Override
				public int compare(ParentCoords o1, ParentCoords o2)
				{
					return o1.getDepth() - o2.getDepth();
				}
			});
		}
		return map;
	}
	
	public ArrayList<ParentCoords> getOres(Level w)
	{
		if(ores == null)
			search(w);
		
		return ores;
	}

	public BlockPos getStartPos()
	{
		return pos;
	}
}
