package futurepack.common.research;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.concurrent.Callable;

import futurepack.api.interfaces.IScanPart;
import net.minecraft.ChatFormatting;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.ClickEvent;
import net.minecraft.network.chat.ClickEvent.Action;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

public class ScanPartTech implements IScanPart
{

	@Override
	public Callable<Collection<Component>> doBlockMulti(Level w, BlockPos pos, boolean inGUI, BlockHitResult res)
	{
		BlockState bs = w.getBlockState(pos);
		return () -> 
		{
			Set<Research> set = ResearchLoader.getReqiredResearch(new ItemStack(bs.getBlock(), 1));
			if(set != null)
			{
				ArrayList<Component> list = new ArrayList<Component>(set.size());
				
				Style s = Style.EMPTY;
				s.applyFormat(inGUI ? ChatFormatting.DARK_AQUA : ChatFormatting.GRAY);
				s.setUnderlined(true);
				
				for(Research r : set)
				{
					TranslatableComponent tool = new TranslatableComponent(r.getTranslationKey());
					tool.setStyle(s);
					TextComponent msg = new TextComponent((inGUI ? ChatFormatting.DARK_GRAY : ChatFormatting.GRAY) + "Research: ");
					Style ss = Style.EMPTY;
					ss.withClickEvent(new ClickEvent(Action.CHANGE_PAGE, "openresearch="+r.getName()));
					msg.setStyle(ss);
					msg.append(tool);
					list.add(msg);
				}
				return list;
			}
			return null;
		};
	}

	@Override
	public Component doEntity(Level w, LivingEntity e, boolean inGUI)
	{
		return null;
	}

	@Override
	public Component doBlock(Level w, BlockPos pos, boolean inGUI, BlockHitResult res)
	{
		return null;
	}

}
