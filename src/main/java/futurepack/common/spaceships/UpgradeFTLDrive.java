package futurepack.common.spaceships;

import java.util.Collection;
import java.util.function.Predicate;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockValidator;
import futurepack.api.interfaces.ISpaceshipSelector;
import futurepack.api.interfaces.ISpaceshipUpgrade;
import futurepack.api.interfaces.tilentity.ITileBoardComputer;
import futurepack.common.block.multiblock.BlockFTLMulti;
import futurepack.common.block.multiblock.BlockFTLMulti.EnumPart;
import futurepack.common.block.multiblock.MultiblockBlocks;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;

public class UpgradeFTLDrive implements ISpaceshipUpgrade, IBlockValidator
{
	public static final Predicate<Block> ftl_drive = b -> b == MultiblockBlocks.ftl_drive;
	static
	{
		FPSpaceShipSelector.registerPredicate(ftl_drive);
	}
	
	@Override
	public String getTranslationKey()
	{
		return "drive.FTL";
	}

	@Override
	public boolean isUpgradeInstalled(ISpaceshipSelector sel)
	{
		if(sel.getSelector().getStatisticsManager().getBlockCount(MultiblockBlocks.ftl_drive) >= 12)
		{
			Collection<ParentCoords> blocks = sel.getSelector().getValidBlocks(this);
			if(blocks.size() >= 12)
			{
				return true;
			}
		}
		
		return false;
	}

	@Override
	public boolean isBoardComputerValid(ITileBoardComputer tile)
	{
		return tile.isAdvancedBoardComputer();
	}

	@Override
	public boolean isValidBlock(Level w, ParentCoords c)
	{
		BlockState state = w.getBlockState(c);
		return ftl_drive.test(state.getBlock()) && state.getValue(BlockFTLMulti.part) != EnumPart.OFF;
	}



}
