package futurepack.common.item.misc;

import java.util.List;

import futurepack.common.entity.living.EntityAlphaJawaul;
import futurepack.common.entity.living.EntityJawaul;
import futurepack.common.item.tools.ToolItems;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class ItemHambone extends Item
{

	public ItemHambone(Properties properties)
	{
		super(properties);
	}

	@Override
	public InteractionResult interactLivingEntity(ItemStack stack, Player playerIn, LivingEntity target, InteractionHand hand)
	{
		if(target instanceof EntityJawaul)
		{
			EntityAlphaJawaul alpha = EntityAlphaJawaul.createFromJawaul((EntityJawaul)target);
			if(alpha==null)
				return InteractionResult.FAIL;
			else
			{
				target.discard();
				alpha.tame(playerIn);
				alpha.spawnAnim();
				
				ItemStack it = new ItemStack(ToolItems.entity_egger_full);
				it.setTag(new CompoundTag());
				alpha.saveAsPassenger(it.getTag());
				it.setHoverName(alpha.getName());
				ItemEntity item = new ItemEntity(target.getCommandSenderWorld(), target.getX(), target.getY(), target.getZ(), it);
				target.getCommandSenderWorld().addFreshEntity(item);
				return InteractionResult.CONSUME;
			}
		}
		return super.interactLivingEntity(stack, playerIn, target, hand);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn)
	{
		tooltip.add(new TranslatableComponent(this.getDescriptionId()+".tooltip"));
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
	}
}
