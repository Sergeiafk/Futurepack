package futurepack.common.item.misc;

import java.util.List;

import futurepack.common.gui.GuiNotiz;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageResearchResponse;
import net.minecraft.client.Minecraft;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.network.PacketDistributor;

public class ItemSpawnNote extends Item
{

	public ItemSpawnNote(Properties properties) 
	{
		super(properties);
	}

	@Override
	public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) 
	{
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslatableComponent("info.note.read"));
	}
	
	@Override
    public InteractionResultHolder<ItemStack> use(Level w, Player pl, InteractionHand hand)
    {	
		if(w.isClientSide)
		{
			DistExecutor.runWhenOn(Dist.CLIENT, () -> {
				return new Runnable()
				{
					@Override
					public void run() 
					{
						Minecraft.getInstance().setScreen(new GuiNotiz(pl.level, pl, pl.getItemInHand(hand)));
					}
				};
			} );
			
		}	
		else
		{
			FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(() -> (ServerPlayer)pl) , new MessageResearchResponse(w.getServer(), CustomPlayerData.getDataFromPlayer(pl)));
			GuiNotiz.StateSaver.save(pl, hand);
		}
		return InteractionResultHolder.success(pl.getItemInHand(hand));
    }
}
