package futurepack.common.item;

import futurepack.common.FuturepackMain;
import net.minecraft.core.Registry;
import net.minecraft.world.item.Item;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.RegistryObject;

public class DustItems
{
	public static final Item.Properties dusts_64 = new Item.Properties().stacksTo(64).tab(FuturepackMain.tab_resources);
	public static final DeferredRegister<Item> REGISTRY_Item_futurepack = DeferredRegister.create(Registry.ITEM_REGISTRY, "futurepack");
	public static final RegistryObject<Item> dust_aluminium = REGISTRY_Item_futurepack.register("dust_aluminium", () -> new Item(dusts_64)   ) ;
	public static final RegistryObject<Item> dust_bioterium = REGISTRY_Item_futurepack.register("dust_bioterium", () -> new Item(dusts_64)   ) ;
	public static final RegistryObject<Item> dust_copper = REGISTRY_Item_futurepack.register("dust_copper", () -> new Item(dusts_64)   ) ;
	public static final RegistryObject<Item> dust_glowtite = REGISTRY_Item_futurepack.register("dust_glowtite", () -> new Item(dusts_64)   ) ;
	public static final RegistryObject<Item> dust_gold = REGISTRY_Item_futurepack.register("dust_gold", () -> new Item(dusts_64)   ) ;
	public static final RegistryObject<Item> dust_iron = REGISTRY_Item_futurepack.register("dust_iron", () -> new Item(dusts_64)   ) ;
	public static final RegistryObject<Item> dust_magnet = REGISTRY_Item_futurepack.register("dust_magnet", () -> new Item(dusts_64)   ) ;
	public static final RegistryObject<Item> dust_neon = REGISTRY_Item_futurepack.register("dust_neon", () -> new Item(dusts_64)   ) ;
	public static final RegistryObject<Item> dust_obsidian = REGISTRY_Item_futurepack.register("dust_obsidian", () -> new Item(dusts_64)   ) ;
	public static final RegistryObject<Item> dust_quantanium = REGISTRY_Item_futurepack.register("dust_quantanium", () -> new Item(dusts_64)   ) ;
	public static final RegistryObject<Item> dust_retium = REGISTRY_Item_futurepack.register("dust_retium", () -> new Item(dusts_64)   ) ;
	public static final RegistryObject<Item> dust_tin = REGISTRY_Item_futurepack.register("dust_tin", () -> new Item(dusts_64)   ) ;
	public static final RegistryObject<Item> dust_wakurium = REGISTRY_Item_futurepack.register("dust_wakurium", () -> new Item(dusts_64)   ) ;
	public static final RegistryObject<Item> dust_zinc = REGISTRY_Item_futurepack.register("dust_zinc", () -> new Item(dusts_64)   ) ;

	public static void initItems(IEventBus modBus)
	{
		REGISTRY_Item_futurepack.register(modBus);
	}

}
