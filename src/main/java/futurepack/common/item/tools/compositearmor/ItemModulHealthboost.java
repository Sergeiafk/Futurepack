
package futurepack.common.item.tools.compositearmor;

import java.util.UUID;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableMultimap.Builder;
import com.google.common.collect.Multimap;

import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemModulHealthboost extends ItemModulStatChange
{
	private final double hpBoost;
	
	public ItemModulHealthboost(Properties props, double hpBoost)
	{
		super(null, props);
		this.hpBoost = hpBoost;
	}

	@Override
	public void onArmorTick(Level world, Player player, ItemStack it, CompositeArmorInventory inv)
	{
		
	}
	
	@Override
	public Multimap<Attribute, AttributeModifier> getDefaultAttributeModifiers(EquipmentSlot slot)
	{
		if(slot == EquipmentSlot.MAINHAND || slot == EquipmentSlot.OFFHAND)
		{
			return ImmutableMultimap.of();
		}

		Builder<Attribute, AttributeModifier> builder = ImmutableMultimap.builder();
		UUID uuid = getUUID(slot);
		builder.put(Attributes.MAX_HEALTH, new AttributeModifier(uuid, "HP Boost", this.hpBoost, AttributeModifier.Operation.ADDITION));
		return builder.build();
	}

}
