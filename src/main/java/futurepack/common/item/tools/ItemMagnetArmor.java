package futurepack.common.item.tools;

import java.util.List;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemMagnetArmor extends ArmorItem
{

	public ItemMagnetArmor(ArmorMaterial mat, EquipmentSlot slots, Item.Properties builder)
	{
		super(mat, slots, builder);
	}
	
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type)
	{
		if(slot== EquipmentSlot.CHEST||slot== EquipmentSlot.FEET || slot== EquipmentSlot.HEAD)
			return "futurepack:textures/models/armor/magnet_layer_1.png";
		else if(slot== EquipmentSlot.LEGS)
			return "futurepack:textures/models/armor/magnet_layer_2.png";
		else
			return super.getArmorTexture(stack, entity, slot, type);
	}
	
	@Override
	public void onArmorTick(ItemStack it, Level w, Player pl)
	{
		//if(!w.isRemote)
		{
			int range = 0;
			for(ItemStack its : pl.getInventory().armor)
			{
				if(its!=null && its.getItem() instanceof ItemMagnetArmor)
				{
					range++;
				}
			}
			List<ItemEntity> list = w.getEntitiesOfClass(ItemEntity.class, pl.getBoundingBox().inflate(range, range, range));
			for(ItemEntity item : list)
			{
				double d3 = (pl.getX()) - item.getX() ;
				double d4 = (pl.getY()) - item.getY() ;
				double d5 = (pl.getZ()) - item.getZ() ;
				
				double dis = Math.sqrt(pl.distanceToSqr(item.getX(), item.getY(), item.getZ()));
				
				d3 = d3/dis ;
				d4 = d4/dis ;
				d5 = d5/dis ;
				
				//FIXME: Is this nessesary? item.setAgeToCreativeDespawnTime();
				if(w.isClientSide)
				{	
					item.setDeltaMovement(d3, d4, d5);
				}
				else
				{
					item.setPos(pl.getX(), pl.getY(), pl.getZ());
				}
			}
		}
	}
}
