package futurepack.common.item.tools;

import java.util.List;

import futurepack.api.interfaces.tilentity.ITileHologramAble;
import futurepack.common.FPLog;
import futurepack.depend.api.helper.HelperHologram;
import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class ItemHologramControler extends Item
{
	
	public ItemHologramControler(Item.Properties props)
	{
		super(props);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, Level w, List<Component> l, TooltipFlag advanced)
	{
		l.add(new TranslatableComponent(getDescriptionId()+".tooltip.sneak_click"));
		l.add(new TranslatableComponent(getDescriptionId()+".tooltip.normal_click"));
//		l.add("sneak-click to select Block");
//		l.add("normal-click to set Hologram");
		super.appendHoverText(stack, w, l, advanced);
	}
	
	@Override
	public InteractionResult useOn(UseOnContext context)
	{
		Player pl = context.getPlayer();
		Level w = context.getLevel();
		ItemStack it = context.getItemInHand();
		BlockPos pos = context.getClickedPos();
		
		if(!w.isClientSide)
		{
			BlockState state = w.getBlockState(pos);
//			state = state.getActualState(w, pos);
			
			if(pl.isShiftKeyDown())
			{
				if(state.getRenderShape()!= RenderShape.MODEL)
				{
					pl.sendMessage(new TranslatableComponent("hologram.error.select.tileentity"), Util.NIL_UUID);
					return InteractionResult.FAIL;
				}
				HelperHologram.saveInItem(it, state);
				if(!it.hasTag())
				{
					FPLog.logger.fatal("[PANIC] Hologram item has no NBT after setting BlockStateData");
					pl.sendMessage(new TextComponent("An internal ERROR prevented saving the NBT."), Util.NIL_UUID);
				}
				else if(!it.getTag().contains("hologram"))
				{
					FPLog.logger.error("Cant add NBTTag 'BlockStateContainer' to Item");
					pl.sendMessage(new TextComponent("An internal ERROR prevented adding the BlockState to the Item. (Item has an NBT tag)"), Util.NIL_UUID);
				}
			}
			else
			{
				if(w.getBlockEntity(pos)==null)
				{
					pl.sendMessage(new TranslatableComponent("hologram.error.set.notileentity"), Util.NIL_UUID);
					return InteractionResult.FAIL;
				}
				
				state = HelperHologram.loadFormItem(it);
				
				if(state == null)
				{
					pl.sendMessage(new TranslatableComponent("hologram.error.set.nonbt"), Util.NIL_UUID);
					return InteractionResult.FAIL;
				}
				
				BlockEntity t = w.getBlockEntity(pos);
				if(t instanceof ITileHologramAble)
				{
					((ITileHologramAble)t).setHologram(state);
					t.setChanged();
					w.sendBlockUpdated(pos, state, state, 2); //TODO: change back if now working -> w.markBlockRangeForRenderUpdate(pos.add(-1, -1, -1), pos.add(1,1,1));
				}
				else
				{
					pl.sendMessage(new TranslatableComponent("hologram.error.set.notsupported"), Util.NIL_UUID);
					return InteractionResult.FAIL;
				}
			}
		}
		return InteractionResult.SUCCESS;
	}
	
	@Override
	public void fillItemCategory(CreativeModeTab group, NonNullList<ItemStack> items) 
	{
		super.fillItemCategory(group, items);
		if(allowdedIn(group))
		{
			//FIXME: confi sub holograms are nolonger available
//			for(String s : FPConfig.CLIENT.creativeQuickHologram.get())
//			{
//				try
//				{
//					ItemStack item = new ItemStack(this, 1);
//					NBTTagCompound nbt = new NBTTagCompound();
//					NBTTagCompound state = JsonToNBT.getTagFromJson(s);
//					nbt.put("hologram", state);
//					item.setTag(nbt);
//					items.add(item);
//				}
//				catch(CommandSyntaxException e)
//				{
//					ItemStack it = new ItemStack(Blocks.STONE);
//					it.setDisplayName(new TextComponentString(s));
//					NBTTagList list = new NBTTagList();
//					list.add(new NBTTagString(e.toString()));
//					it.getOrCreateChildTag("display").put("Lore", list);
//					items.add(it);
//				}
//			}
		}
	}
}
