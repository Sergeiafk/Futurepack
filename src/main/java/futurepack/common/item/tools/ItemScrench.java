package futurepack.common.item.tools;

import java.util.List;

import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.modification.TileEntityModificationBase;
import futurepack.common.sync.FPGuiHandler;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class ItemScrench extends Item 
{

	public ItemScrench(Item.Properties props) 
	{
		super(props);
//		setCreativeTab(FPMain.tab_items);
	}
	
	@Override
	public InteractionResult useOn(UseOnContext context)
	{
		ItemStack it = context.getItemInHand();
		Level w = context.getLevel();
		BlockPos pos = context.getClickedPos();
		
		if(it.getOrCreateTag().getBoolean("rotate"))
		{
			BlockEntity t = w.getBlockEntity(pos);
			CompoundTag nbt = new CompoundTag();
			if(t!=null)
			{
				nbt = t.saveWithFullMetadata();
			}
			BlockState state = w.getBlockState(pos);
//			Block bl = state.getBlock();
//			Random rand = new Random();

			
			BlockState changed	= state.rotate(w, pos, Rotation.COUNTERCLOCKWISE_90);
			if(state != changed)
			{
				w.setBlock(pos, changed, 3);
			}
			else if(state.hasProperty(BlockRotateableTile.FACING))
			{
				changed = state.setValue(BlockRotateableTile.FACING, state.getValue(BlockRotateableTile.FACING).getOpposite());
				if(state != changed)
				{
					w.setBlock(pos, changed, 3);
				}
			}

			t = w.getBlockEntity(pos);
			if(t!=null)
			{
				t.setBlockState(changed);
				t.load(nbt);
				t.setChanged();
			}
			return InteractionResult.SUCCESS;
		}
		else if(w.getBlockEntity(pos) instanceof TileEntityModificationBase)
		{
			if(!w.isClientSide)
				FPGuiHandler.CHIPSET.openGui(context.getPlayer(), pos);
			
			return InteractionResult.SUCCESS;
		}
		return InteractionResult.PASS;
	}
	
	@Override
	public InteractionResultHolder<ItemStack> use(Level w, Player pl, InteractionHand hand)
	{
		ItemStack it = pl.getItemInHand(hand);
		if(!pl.isShiftKeyDown())
		{
			if(it.getOrCreateTag().getBoolean("rotate"))
			{
				it.getOrCreateTag().putBoolean("rotate", false);
			}
			else
			{
				it.getOrCreateTag().putBoolean("rotate", true);
			}
		}
		return InteractionResultHolder.success(it);
	}
	
	@Override
	public Rarity getRarity(ItemStack stack)
	{
		return Rarity.UNCOMMON;
	}
	
	@Override
	public void appendHoverText(ItemStack stack, Level w, List<Component> tooltip, TooltipFlag advanced)
	{
		tooltip.add(new TranslatableComponent(stack.getOrCreateTag().getBoolean("rotate") ? "tooltip.items.scrench.rotate":"tooltip.items.scrench.modify"));
	}
	
}
