package futurepack.common.item.tools;

import futurepack.common.FuturepackTags;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Tier;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;

public class ItemDrillMK1 extends ItemPoweredMineToolBase
{

	public ItemDrillMK1(Properties props, Tier tier)
	{
		super(props, tier, BlockTags.MINEABLE_WITH_PICKAXE);
		max_speed = 50F;
	}
	
	@Override
	public boolean isMineable(BlockState bs)
	{
		return bs.getMaterial()==Material.STONE || bs.is(FuturepackTags.BLOCK_ORES);
	}

	@Override
	public boolean canPerformAction(ItemStack stack, net.minecraftforge.common.ToolAction toolAction) 
	{
		return net.minecraftforge.common.ToolActions.DEFAULT_PICKAXE_ACTIONS.contains(toolAction);
	}
}
