package futurepack.common.item;

import futurepack.api.interfaces.IGranadleLauncherAmmo;
import futurepack.common.block.plants.BlockGlowmelo;
import futurepack.common.block.plants.PlantBlocks;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.item.FallingBlockEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemGlowmelo extends BlockItem implements IGranadleLauncherAmmo
{

	public ItemGlowmelo(Properties builder) 
	{
		super(PlantBlocks.glowmelo, builder);
	}

	@Override
	public boolean isGranade(ItemStack it) 
	{
		return this == FoodItems.glowmelo;
	}

	@Override
	public Entity createGrenade(Level w, ItemStack it, Player pl, float strength)
	{
		Entity et = new FallingBlockEntity(w, pl.getX(), pl.getY()+pl.getEyeHeight(), pl.getZ(), PlantBlocks.glowmelo.defaultBlockState().setValue(BlockGlowmelo.AGE_0_6, 6));
		
		((FallingBlockEntity)et).setHurtsEntities(1.0F, 40);
		((FallingBlockEntity)et).time++;
		
		float f0 = -Mth.sin(pl.getYRot() * 0.017453292F) * Mth.cos(pl.getXRot() * 0.017453292F);
		float f1 = -Mth.sin((pl.getXRot()) * 0.017453292F);
		float f2 = Mth.cos(pl.getYRot() * 0.017453292F) * Mth.cos(pl.getXRot() * 0.017453292F);
		
		float ff = Mth.sqrt(f0 * f0 + f1 * f1 + f2 * f2);
		f0 = f0 / ff;
		f1 = f1 / ff;
		f2 = f2 / ff;
		f0 = f0 * strength;
		f1 = f1 * strength;
		f2 = f2 * strength;
		et.setDeltaMovement(f0, f1, f2);
		float fff = Mth.sqrt(f0 * f0 + f2 * f2);
		et.setYRot(et.yRotO = (float)(Mth.atan2(f0, f2) * (180D / Math.PI)));
		et.setXRot(et.xRotO = (float)(Mth.atan2(f1, fff) * (180D / Math.PI)));
		
		return et;
	}

}
