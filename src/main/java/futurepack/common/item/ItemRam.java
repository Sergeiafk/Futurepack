package futurepack.common.item;

import java.util.List;

import futurepack.api.interfaces.IItemWithRandom;
import futurepack.common.modification.IPartRam;
import futurepack.common.modification.thermodynamic.TemperatureManager;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class ItemRam extends Item implements IItemWithRandom
{
//	String[] iconname = new String[]{"Standart-ram","A-ram","P-ram","TCT-ram","Master-ram","Non-ram","Dungon-ram","Univ-ram","Zombie-ram","Entronium-ram"};
//	static Integer[] baseram = new Integer[]{1, 2, 3, 4, 5, 7, 7, 8, 0, 10};
	
	private final int baseram;
	
	public ItemRam(Properties props, int ram) 
	{
		super(props);
		this.baseram = ram;
//		this.setCreativeTab(FPMain.tab_items);
//		this.setHasSubtypes(true);
	}
	
	public static IPartRam getRam(ItemStack it)
	{
		if(it!=null && it.getItem() instanceof ItemRam)
		{
			ItemRam ram = (ItemRam) it.getItem();
			final int speed = ram.getRamSpeed(it);
			final int core = ram.getNeededCore(it);
			final float maxTemp = TemperatureManager.getTemp(it);
			
			return new IPartRam()
			{	
				@Override
				public float getRamSpeed()
				{
					return speed;
				}
				
				@Override
				public int getCorePower() 
				{
					return core;
				}

				@Override
				public float getMaximumTemperature()
				{
					return maxTemp;
				}
			};
		}
		return null;
	}
	
	public int getRamSpeed(ItemStack it)
	{
		int base = baseram;
		if(it.hasTag() && it.getTag().contains("ram"))
		{
			base += it.getTag().getInt("ram");
		}
		return base;
	}
	
	public int getNeededCore(ItemStack it)
	{
		int base = 1;
		if(it.hasTag() && it.getTag().contains("ram"))
		{
			base += it.getTag().getInt("ram");
		}
		return base;	
	}
	
	@Override
	public void appendHoverText(ItemStack it, Level w, List<Component> list, TooltipFlag par4) 
	{
		list.add(new TranslatableComponent("tooltip.futurepack.item.ram_speed", getRamSpeed(it)));
		list.add(new TranslatableComponent("tooltip.futurepack.item.core_power", getNeededCore(it)));
		list.add(new TranslatableComponent("tooltip.futurepack.item.max_temp", TemperatureManager.getTemp(it)));
		super.appendHoverText(it, w, list, par4);
	}

	@Override
	public void setRandomNBT(ItemStack it, int random) 
	{
		if(!it.hasTag())
		{
			it.setTag(new CompoundTag());
		}
			
		if(this == ComputerItems.torus_ram)
		{
			random += it.getTag().getInt("rambase");
		}
		
		it.getTag().putInt("ram",random);
	}
	
	public static ItemStack getFromToasted(ItemStack it)
	{
		if(!it.isEmpty() && it.getItem() == ComputerItems.toasted_ram && it.hasTag())
		{
			CompoundTag nbt = it.getTag();
			return ItemStack.of(nbt);
		}
		return null;
	}
}
