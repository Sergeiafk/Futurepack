package futurepack.common.modification;

public interface IPartRam extends IModificationPart
{
	@Override
	default boolean isRam()
	{
		return true;
	}
	
	@Override
	default boolean isChip()
	{
		return false;
	}
	
	@Override
	default boolean isCore() 
	{
		return false;
	}
	
	@Override
	default float getChipPower(EnumChipType type)
	{
		return 0;
	}
	
	@Override
	default int getCorePower(EnumCorePowerType type)
	{
		switch (type)
		{
		case NEEDED:
			return getCorePower();
		case BOTH:
			return - getCorePower();
		default:
			return 0;
		}
	}
	
	int getCorePower();
}
