package futurepack.common.dim.structures;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.UUID;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.Nameable;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.saveddata.SavedData;

public class TeleporterMap extends SavedData 
{
	private Map<UUID, Set<BlockPos>> player2teleporter = new TreeMap<UUID, Set<BlockPos>>();
	private Map<BlockPos, String> teleporterNames = new TreeMap<BlockPos, String>();
	
	private ServerLevel world;
	
	private TeleporterMap(ServerLevel world) 
	{
		this.world = world;
		setDirty(true);
	}
	
	public static TeleporterMap getTeleporterMap(ServerLevel world)
	{
		return world.getDataStorage().computeIfAbsent(nbt -> TeleporterMap.loadfromNBT(world, nbt), () -> new TeleporterMap(world), "futurepack_teleporter");
	}
	
	public static TeleporterMap loadfromNBT(ServerLevel world, CompoundTag nbt)
	{
		TeleporterMap map = new TeleporterMap(world);
		map.load(nbt);
		return map;
	}
	

	public void load(CompoundTag nbt) 
	{
		for(String key : nbt.getAllKeys())
		{
			int[] pos = nbt.getIntArray(key);
			UUID uid = UUID.fromString(key);
			Set<BlockPos> set = player2teleporter.computeIfAbsent(uid, u -> new TreeSet<>());
			int i=0;
			while(i<pos.length)
			{
				set.add(new BlockPos(pos[i], pos[i+1], pos[i+2]));
				i+=3;
			}
		}
	}

	@Override
	public CompoundTag save(CompoundTag nbt) 
	{
		for(Entry<UUID, Set<BlockPos>> entry : player2teleporter.entrySet())
		{
			int[] pos = new int[entry.getValue().size() * 3];
			int i=0;
			for(BlockPos p : entry.getValue())
			{
				pos[i] = p.getX();
				pos[i+1] = p.getY();
				pos[i+2] = p.getZ();
				i+=3;
			}
			nbt.putIntArray(entry.getKey().toString(), pos);
		}
		return nbt;
	}

	
	public List<TeleporterEntry> getTeleporter(Player player)
	{
		UUID uid = player.getGameProfile().getId();
		Set<BlockPos> poss = player2teleporter.computeIfAbsent(uid, u -> new TreeSet<>());
		List<TeleporterEntry> list =  new ArrayList<>(poss.size());
		List<BlockPos> removed = new ArrayList<BlockPos>();
		for(BlockPos p : poss)
		{
			if(world.getBlockEntity(p) != null)
			{
				list.add(new TeleporterEntry(p, teleporterNames.computeIfAbsent(p, this::getTeleporterName)));
			}
			else
			{
				removed.add(p);
			}
		}
		poss.removeAll(removed);
		return list;
	}
	
	public boolean addTeleporter(Player player, BlockPos pos)
	{
		pos = pos.immutable();
		setDirty();
		teleporterNames.put(pos, getTeleporterName(pos));
		UUID uid = player.getGameProfile().getId();
		return player2teleporter.computeIfAbsent(uid, u -> new TreeSet<>()).add(pos);
	}
	
	private String getTeleporterName(BlockPos pos)
	{
		Nameable name = (Nameable) world.getBlockEntity(pos);
		return name.getDisplayName().getString();
	}
	
	public static class TeleporterEntry
	{
		public final BlockPos pos;
		public final String name;
		
		public TeleporterEntry(BlockPos pos, String name) 
		{
			super();
			this.pos = pos;
			this.name = name;
		}
	}
	
}
