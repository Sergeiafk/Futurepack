package futurepack.common.filter;

import java.io.IOException;
import java.io.StringWriter;
import java.util.function.Consumer;

public class CallbackStringWriter extends StringWriter
{
	private final IOConsumer<CallbackStringWriter> callback;

	public CallbackStringWriter(IOConsumer<CallbackStringWriter> callback)
	{
		this.callback = callback;
	}

	@Override
	public void close() throws IOException
	{
		super.close();
		callback.accept(this);
	}

	@FunctionalInterface
	public static interface IOConsumer<T>
	{
		void accept(T t) throws IOException;
	}
}
