package futurepack.extensions.CuriousIntegration;

import java.lang.reflect.Method;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import futurepack.common.FPLog;
import futurepack.common.ManagerGleiter;
import futurepack.common.item.tools.ToolItems;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.InterModComms;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.items.IItemHandlerModifiable;

public class CuriosIntegration
{
	
	private static Function<Player, LazyOptional<Object>> f_getCuriosHandler;
	private static BiFunction<Object, String, Optional<Object>> f_getStacksHandler;
	private static Function<Object, IItemHandlerModifiable> f_getStacks;
	private static Supplier<Object> f_buildBack;
	
	private static final String BACK = "back";
	
	@SuppressWarnings("unchecked")
	public static void init(IEventBus modBus)
	{
		modBus.addListener(CuriosIntegration::enqueue);
		
		try
		{
			Class<?> c_curiosApi = Class.forName("top.theillusivec4.curios.api.CuriosApi");
//			Class<?> c_SlotContext = Class.forName("top.theillusivec4.curios.api.SlotContext");
			Class<?> c_SlotTypePreset = Class.forName("top.theillusivec4.curios.api.SlotTypePreset");
//			Class<?> c_ICurio = Class.forName("top.theillusivec4.curios.api.type.capability.ICurio");
			Class<?> c_ICuriosItemHandler = Class.forName("top.theillusivec4.curios.api.type.capability.ICuriosItemHandler");
			Class<?> c_ICurioStacksHandler = Class.forName("top.theillusivec4.curios.api.type.inventory.ICurioStacksHandler");
//			Class<?> c_CurioItemCapability = Class.forName("top.theillusivec4.curios.common.capability.CurioItemCapability");
			Class<?> c_ICuriosHelper = Class.forName("top.theillusivec4.curios.api.type.util.ICuriosHelper");
			Class<?> c_Builder = Class.forName("top.theillusivec4.curios.api.SlotTypeMessage$Builder");
			
			Method m_getCuriosHelper = c_curiosApi.getMethod("getCuriosHelper");
			Method m_getCuriosHandler = c_ICuriosHelper.getMethod("getCuriosHandler", LivingEntity.class);
			Method m_getStacksHandler = c_ICuriosItemHandler.getMethod("getStacksHandler", String.class);
			Method m_getStacks = c_ICurioStacksHandler.getMethod("getStacks");
			Method m_getMessageBuilder = c_SlotTypePreset.getMethod("getMessageBuilder");
			Method m_build = c_Builder.getMethod("build");
			
			//Object o_ICuriosHelper = m_getCuriosHelper.invoke(null);
			Object o_BACK = c_SlotTypePreset.getField("BACK").get(null);
			
			f_getCuriosHandler = pl -> {
				try
				{
					Object o_ICuriosHelper = m_getCuriosHelper.invoke(null);
					
					return (LazyOptional<Object>) m_getCuriosHandler.invoke(o_ICuriosHelper, pl); //LazyOptional<ICuriosItemHandler>
				} 
				catch (Exception e)
				{
					throw new RuntimeException(e);
				}
			};
			f_getStacksHandler = (inv, slot) -> {
				try
				{
					return (Optional<Object>) m_getStacksHandler.invoke(inv, slot); //Optionl<ICurioStacksHandler>
				} 
				catch (Exception e)
				{
					throw new RuntimeException(e);
				}
			};
			f_getStacks = stacksHandler -> {
				try
				{
					return (IItemHandlerModifiable) m_getStacks.invoke(stacksHandler); //IDynamicStackHandler
				} 
				catch (Exception e)
				{
					throw new RuntimeException(e);
				}
			};
			//			() -> SlotTypePreset.BACK.getMessageBuilder().build();
			f_buildBack = () -> {
				try
				{
					return m_build.invoke(m_getMessageBuilder.invoke(o_BACK));
				} 
				catch (Exception e)
				{
					throw new RuntimeException(e);
				}
			};			
		
			ManagerGleiter.gleiterProvider.add(CuriosIntegration::getGleiterBackStack);
		}
		catch(Throwable t)
		{
			FPLog.logger.catching(t);
		}
	}
	
	public static ItemStack getBackStack(Player player)
	{
		AtomicReference<ItemStack> back = new AtomicReference<>(ItemStack.EMPTY);
		LazyOptional<Object> optional = f_getCuriosHandler.apply(player);//LazyOptional<ICuriosItemHandler>
		optional.ifPresent(inv -> {
			Optional<Object> stacksOptional = f_getStacksHandler.apply(inv, BACK); //Optional<ICurioStacksHandler>
			stacksOptional.ifPresent(stacksHandler -> {
				ItemStack stack = f_getStacks.apply(stacksHandler).getStackInSlot(0);
				if(!stack.isEmpty())
				{
					back.set(stack);
				}
			});
		});
		return back.get();
	}

	public static ItemStack getGleiterBackStack(Player player)
	{
		ItemStack stack = getBackStack(player);
		if(stack.getItem() == ToolItems.gleiter)
		{
			return stack;
		}
		return ItemStack.EMPTY;
	}
	
	public static void enqueue(final InterModEnqueueEvent evt) 
	{
		InterModComms.sendTo("curios", "register_type", f_buildBack);
	}
	
	
//	/**
//     * Gets the stack in the back slot
//     */
//    public static ItemStack getBackStack(Player player)
//    {
//        AtomicReference<ItemStack> back = new AtomicReference<>(ItemStack.EMPTY);
//        LazyOptional<ICuriosItemHandler> optional = CuriosApi.getCuriosHelper().getCuriosHandler(player);
//        optional.ifPresent(itemHandler ->
//        {
//            Optional<ICurioStacksHandler> stacksOptional = itemHandler.getStacksHandler(SlotTypePreset.BACK.getIdentifier());
//            stacksOptional.ifPresent(stacksHandler ->
//            {
//                ItemStack stack = stacksHandler.getStacks().getStackInSlot(0);
//                if(!stack.isEmpty())
//                {
//                    back.set(stack);
//                }
//            });
//        });
//        return back.get();
//    }
	
}
