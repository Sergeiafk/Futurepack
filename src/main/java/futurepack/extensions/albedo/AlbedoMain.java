package futurepack.extensions.albedo;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.function.Consumer;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.EventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class AlbedoMain
{
	public static boolean isAvailable = false;
	
	public static final AlbedoMain INSTANCE = new AlbedoMain();
	
	private static Method c_addLights;
	
	public void init()
	{
		//MinecraftForge.EVENT_BUS.register(this);
		
		//I will hack the Forge Event Bus,this is terrible but I dont want hard dependencies...
		try
		{
			Method m = AlbedoMain.class.getDeclaredMethod("gatherLightEvent", Object.class);		
			Class<?> c_GatherLightEvent = Class.forName("elucent.albedo.event.GatherLightsEvent");
			c_addLights= c_GatherLightEvent.getMethod("add");
			
			Method m_register = EventBus.class.getDeclaredMethod("register", Class.class, Object.class, Method.class);
			m_register.setAccessible(true);
			m_register.invoke(MinecraftForge.EVENT_BUS, c_GatherLightEvent, this, m); //if any one will see this, them will kill me.
			
			isAvailable = true;
		}
		catch(ClassNotFoundException e)
		{
			//Albedo is not installed
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@SubscribeEvent
	public void gatherLightEvent(Object obj)
	{
		Consumer<Object> addLight = arg0 -> {
			try {
				c_addLights.invoke(arg0);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		};
		LightList.addToList(addLight);
	}
}
