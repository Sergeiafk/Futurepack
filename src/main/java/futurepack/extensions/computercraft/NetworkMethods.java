package futurepack.extensions.computercraft;

import javax.annotation.Nonnull;

import dan200.computercraft.api.peripheral.GenericPeripheral;
import dan200.computercraft.api.peripheral.PeripheralType;
import futurepack.api.Constants;
import net.minecraft.resources.ResourceLocation;

public class NetworkMethods implements GenericPeripheral
{
	@Nonnull
	@Override
	public PeripheralType getType() 
	{
		return PeripheralType.ofAdditional("network");
	}

	@Nonnull
	@Override
	public ResourceLocation id() 
	{
		return new ResourceLocation(Constants.MOD_ID, "network");
	}
	
	
}
