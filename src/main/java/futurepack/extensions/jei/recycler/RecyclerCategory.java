package futurepack.extensions.jei.recycler;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.api.interfaces.IRecyclerRecipe;
import futurepack.common.block.modification.ModifiableBlocks;
import futurepack.common.recipes.recycler.RecyclerTimeManipulatorRecipe;
import futurepack.extensions.jei.BaseRecipeCategory;
import futurepack.extensions.jei.FuturepackUids;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.builder.IRecipeSlotBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.drawable.IDrawableAnimated;
import mezz.jei.api.gui.drawable.IDrawableAnimated.StartDirection;
import mezz.jei.api.gui.drawable.IDrawableStatic;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import net.minecraft.client.Minecraft;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;

public class RecyclerCategory extends BaseRecipeCategory<IRecyclerRecipe>
{
	private IDrawableAnimated arrow;

	public RecyclerCategory(IGuiHelper gui)
	{
		super(gui, ModifiableBlocks.recycler_w, FuturepackUids.RECYCLER, 92-19, 32-10);


	}

	@SuppressWarnings("resource")
	@Override
	public void draw(IRecyclerRecipe recipe, PoseStack matrixStack, double mouseX, double mouseY)
	{
		arrow.draw(matrixStack, 68-24, 28-24);
		Minecraft.getInstance().font.draw(matrixStack, recipe.getJeiInfoText(), 0, 44, Color.gray.getRGB());
		super.draw(recipe, matrixStack, mouseX, mouseY);
	}


	@Override
	public Class<? extends IRecyclerRecipe> getRecipeClass()
	{
		return IRecyclerRecipe.class;
	}

//	@Override
//	public void setIngredients(IRecyclerRecipe rec, IIngredients ingredients)
//	{
//		List<List<ItemStack>> inputs = new ArrayList<List<ItemStack>>();
//		List<List<ItemStack>> outputs = new ArrayList<List<ItemStack>>();
//
//		if(rec instanceof RecyclerJeiFakeRecipe)
//		{
////			inputs.add(((RecyclerJeiFakeRecipe)rec).getInputs());
////			inputs.add(rec.getToolItemStacks());
//			outputs.add(((RecyclerJeiFakeRecipe)rec).getOutputs());
//
//			ingredients.setInputLists(VanillaTypes.ITEM, inputs);
//			ingredients.setOutputLists(VanillaTypes.ITEM, outputs);
//
//			return;
//		}
//		else if(rec instanceof RecyclerTimeManipulatorRecipe)
//		{
////			RecyclerTimeManipulatorRecipe rrr = (RecyclerTimeManipulatorRecipe) rec;
////
////			List<ItemStack> in = rec.getInput().collectAcceptedItems(new ArrayList<>());
////
////			for(ItemStack it : in)
////			{
////				if(it.isDamageableItem())
////					it.setDamageValue(Math.min(it.getMaxDamage() - 1, 25));
////			}
////			inputs.add(in);
////			inputs.add(rec.getToolItemStacks());
//
//			ingredients.setInputLists(VanillaTypes.ITEM, inputs);
//
//			for(ItemStack it : rec.getMaxOutput())
//			{
//				outputs.add(Collections.singletonList(it));
//			}
//
//			ingredients.setOutputLists(VanillaTypes.ITEM, outputs);
//
//			return;
//		}
//
//		inputs.add(rec.getInput().collectAcceptedItems(new ArrayList<>()));
//
////		inputs.add(rec.getToolItemStacks());
//
//		ingredients.setInputLists(VanillaTypes.ITEM, inputs);
//
//
//		for(ItemStack it : rec.getMaxOutput())
//		{
//			outputs.add(Collections.singletonList(it));
//		}
//
//		ingredients.setOutputLists(VanillaTypes.ITEM, outputs);
//
//	}
//
//	@Override
//	public void setRecipe(IRecipeLayout recipeLayout, IRecyclerRecipe recipe, IIngredients ingredients)
//	{
//		IGuiItemStackGroup group = recipeLayout.getItemStacks();
//
//		group.init(0, true, 26-25, 26-25);
//		group.init(1, true, 62-25, 44-25);
//
//		for(int i = 0; i < 9; i++)
//		{
//			group.init(i+2, false, 98-25 + (i%3)*18, 26-25 + (i/3)*18);
//		}
//
//		group.set(ingredients);
//
//		group.addTooltipCallback( (slotIndex,input,ingredient, tooltip) -> {
//			if(slotIndex >= 2 && recipe.getChances() != null)
//				tooltip.add(new TextComponent("Chance: " + recipe.getChances()[slotIndex - 2] * 100.0f + "%"));
//		});
//	}

	@Override
	public void setRecipe(IRecipeLayoutBuilder builder, IRecyclerRecipe recipe, IFocusGroup focuses)
	{
		IRecipeSlotBuilder in = builder.addSlot(RecipeIngredientRole.INPUT, 26-25+1, 26-25+1);
		builder.addSlot(RecipeIngredientRole.CATALYST, 62-25+1, 44-25+1).addItemStacks(recipe.getToolItemStacks());

		if(recipe instanceof RecyclerJeiFakeRecipe f)
		{
			in.addItemStacks(f.getInputs());
		}
		else if(recipe instanceof RecyclerTimeManipulatorRecipe t)
		{
			List<ItemStack> ins = t.getInput().collectAcceptedItems(new ArrayList<>());

			for(ItemStack it : ins)
			{
				if(it.isDamageableItem())
					it.setDamageValue(Math.min(it.getMaxDamage() - 1, 25));
			}
			in.addItemStacks(ins);
		}
		else
		{
			List<ItemStack> ins = recipe.getInput().collectAcceptedItems(new ArrayList<>());
			in.addItemStacks(ins);
		}


		ItemStack[] outs = recipe.getMaxOutput();
		for(int i = 0; i < 9; i++)
		{
			IRecipeSlotBuilder out = builder.addSlot(RecipeIngredientRole.OUTPUT, 98-25 + (i%3)*18 +1 , 26-25 + (i/3)*18 +1);
			if(recipe.getChances() != null)
			{
				final int slot = i;
				out.addTooltipCallback((view, tip) ->  tip.add(new TextComponent("Chance: " + recipe.getChances()[slot] * 100.0f + "%")));
			}

			if(outs!=null && i < outs.length)
			{
				out.addItemStack(outs[i]);
			}

		}



		// TODO Auto-generated method stub
		super.setRecipe(builder, recipe, focuses);
	}

	@Override
	protected IDrawable createBackground(IGuiHelper gui)
	{
		ResourceLocation res =new ResourceLocation(Constants.MOD_ID, "textures/gui/recycler_jei.png");
		IDrawableStatic base = gui.createDrawable(res, 176, 0, 4, 12);
		arrow = gui.createAnimatedDrawable(base, 100, StartDirection.BOTTOM, false);
		return gui.createDrawable(res, 24, 24, 128, 56);
	}

	@Override
	public boolean isResearched(IRecyclerRecipe rec)
	{
		return true;
	}

}
