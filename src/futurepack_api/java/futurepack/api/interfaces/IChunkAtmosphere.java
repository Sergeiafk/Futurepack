package futurepack.api.interfaces;

import net.minecraft.world.level.ChunkPos;

public interface IChunkAtmosphere
{
	public int getMaxAir();
	
	/**
	 * @param x the X position 0-15
	 * @param y the Y position 0-255 (min build to max build height of world)
	 * @param z the Z position 0-15
	 * @return the amount of breathable air
	 */
	public int getAirAt(int x, int y, int z);
	
	/**
	 * @param x the X position 0-15
	 * @param y the Y position (min build to max build height of world)
	 * @param z the Z position 0-15
	 * @param amount the amount to be added
	 * @return the actually added amount
	 */
	public int addAir(int x, int y, int z, int amount);
	
	/**
	 * @param x the X position 0-15
	 * @param y the Y position (min build to max build height of world)
	 * @param z the Z position 0-15
	 * @param amount the amount to be removed
	 * @return the actually removed amount
	 */
	public int removeAir(int x, int y, int z, int amount);
	
	public default boolean hasAir(int x, int y, int z)
	{
		return getAirAt(x,y,z) > 0;
	}
	
	/**
	 * Used for sync/reloding
	 */
	public int setAir(int x, int y, int z, int amount);
	
	/**
	 * 
	 * @param heightSection chekcs if that section needs an update or -1 if the whole chunk needs an update
	 * @return true if a update is needed
	 */
	public default boolean needsUpdate(int heightSection)
	{
		return true;
	}
	
	public int getHeightSections();
	
	
	
	default void setChunkPos(ChunkPos pos) {
		
	}
	
	default ChunkPos getChunkPos()
	{
		return null;
	}
}
