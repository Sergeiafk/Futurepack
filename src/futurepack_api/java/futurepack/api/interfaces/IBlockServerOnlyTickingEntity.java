package futurepack.api.interfaces;

import futurepack.api.interfaces.tilentity.ITileClientTickable;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public interface IBlockServerOnlyTickingEntity<T extends BlockEntity & ITileServerTickable> extends IBlockTickableEntity 
{
	@Override
	default BlockEntityType<? extends ITileClientTickable> getClientEnityType(Level pLevel, BlockState pState) 
	{
		return null;
	}
	
	@Override
	default T newBlockEntity(BlockPos pPos, BlockState pState) 
	{
		return getTileEntityType(pState).create(pPos, pState);
	}
	
	@Override
	default BlockEntityType<? extends ITileServerTickable> getServerEntityType(Level pLevel, BlockState pState) 
	{
		return getTileEntityType(pState);
	}
	
	public BlockEntityType<T> getTileEntityType(BlockState pState);
	
}
