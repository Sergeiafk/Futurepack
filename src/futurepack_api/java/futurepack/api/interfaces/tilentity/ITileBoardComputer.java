package futurepack.api.interfaces.tilentity;

import futurepack.api.EnumStateSpaceship;
import futurepack.api.interfaces.IPlanet;
import futurepack.api.interfaces.ISpaceshipUpgrade;
import net.minecraft.core.BlockPos;

/**
 * This is to determine if the given BoardComputer is good enough for a spaceship upgrade ({@link futurepack.api.interfaces.ISpaceshipUpgrade})
 */
public interface ITileBoardComputer
{
	/**
	 * Returns the status of the current spaceship.
	 */
	public boolean getState(EnumStateSpaceship state);
	
	/**
	 * Returns if this is the Advanced version of the BoardComputer
	 * for example the FTL drive need the advances version to work
	 */
	public boolean isAdvancedBoardComputer();
	
	public boolean isUpgradeinstalled(ISpaceshipUpgrade up);
	
	public BlockPos getJumpTargetPosition();
	public BlockPos getComputerPosition();
	
	
	public void prepareJump(IPlanet pl, BlockPos target);
	
	public boolean canJump();
	public void searchForShip();
	
	public void setHome();
	
	public int getThrusterCount();
	public int getFuelCount();
	public int getBlockCount();
	public int getTransportingBlockCount();
	
	default float getJumpProgress()
	{
		return (float)getTransportingBlockCount() / getBlockCount();
	}
}
