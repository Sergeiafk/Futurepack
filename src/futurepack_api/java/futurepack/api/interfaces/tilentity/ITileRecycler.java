package futurepack.api.interfaces.tilentity;

import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.CapabilitySupport;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;

public interface ITileRecycler
{

	default Level getTileLevel()
	{
		return ((BlockEntity) this).getLevel();
	}

	CapabilitySupport getSupport();

	CapabilityNeon getEnergy();

	int getDefaultPowerUsage();
	
}
