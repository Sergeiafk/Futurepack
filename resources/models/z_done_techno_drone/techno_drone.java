// Made with Blockbench 4.0.0-beta.3
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class techno_drone<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "techno_drone"), "main");
	private final ModelPart droneGround;

	public techno_drone(ModelPart root) {
		this.droneGround = root.getChild("droneGround");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition droneGround = partdefinition.addOrReplaceChild("droneGround", CubeListBuilder.create().texOffs(0, 4).addBox(-2.0F, -3.0F, -2.0F, 4.0F, 2.0F, 4.0F, new CubeDeformation(0.1F))
		.texOffs(0, 10).addBox(-1.5F, -4.0F, 1.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition nag = droneGround.addOrReplaceChild("nag", CubeListBuilder.create().texOffs(0, 0).addBox(-1.5F, -1.0F, -1.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -3.0F, -2.0F));

		PartDefinition lag1 = droneGround.addOrReplaceChild("lag1", CubeListBuilder.create().texOffs(0, 14).addBox(-0.5F, 0.0F, 0.0F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -3.0F, -2.0F, 0.0F, 0.7854F, 0.0F));

		PartDefinition servo01 = lag1.addOrReplaceChild("servo01", CubeListBuilder.create().texOffs(0, 4).addBox(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.1F))
		.texOffs(0, 6).addBox(0.0F, 1.0F, 0.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.05F)), PartPose.offsetAndRotation(2.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.4363F));

		PartDefinition servo02 = servo01.addOrReplaceChild("servo02", CubeListBuilder.create().texOffs(12, 5).addBox(-1.0F, 0.0F, 0.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.0F, 2.0F, 0.0F, 0.0F, 0.0F, 0.3491F));

		PartDefinition lag2 = droneGround.addOrReplaceChild("lag2", CubeListBuilder.create().texOffs(8, 14).addBox(-0.5F, 0.0F, -1.0F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -3.0F, 2.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition servo2 = lag2.addOrReplaceChild("servo2", CubeListBuilder.create().texOffs(0, 4).addBox(0.0F, 0.0F, -1.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.1F))
		.texOffs(0, 6).addBox(0.0F, 1.0F, -1.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.05F)), PartPose.offsetAndRotation(2.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.4363F));

		PartDefinition servo3 = servo2.addOrReplaceChild("servo3", CubeListBuilder.create().texOffs(12, 5).addBox(-1.0F, 0.0F, -1.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.0F, 2.0F, 0.0F, 0.0F, 0.0F, 0.3491F));

		PartDefinition lag3 = droneGround.addOrReplaceChild("lag3", CubeListBuilder.create().texOffs(0, 14).addBox(-0.5F, 0.0F, 0.0F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -3.0F, 2.0F, 0.0F, -2.3562F, 0.0F));

		PartDefinition servo4 = lag3.addOrReplaceChild("servo4", CubeListBuilder.create().texOffs(0, 4).addBox(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.1F))
		.texOffs(0, 6).addBox(0.0F, 1.0F, 0.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.05F)), PartPose.offsetAndRotation(2.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.4363F));

		PartDefinition servo5 = servo4.addOrReplaceChild("servo5", CubeListBuilder.create().texOffs(12, 5).addBox(-1.0F, 0.0F, 0.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.0F, 2.0F, 0.0F, 0.0F, 0.0F, 0.3491F));

		PartDefinition lag4 = droneGround.addOrReplaceChild("lag4", CubeListBuilder.create().texOffs(8, 14).addBox(-0.5F, 0.0F, -1.0F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -3.0F, -2.0F, 0.0F, 2.3562F, 0.0F));

		PartDefinition servo6 = lag4.addOrReplaceChild("servo6", CubeListBuilder.create().texOffs(0, 4).addBox(0.0F, 0.0F, -1.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.1F))
		.texOffs(0, 6).addBox(0.0F, 1.0F, -1.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.05F)), PartPose.offsetAndRotation(2.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.4363F));

		PartDefinition servo7 = servo6.addOrReplaceChild("servo7", CubeListBuilder.create().texOffs(12, 5).addBox(-1.0F, 0.0F, -1.0F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.0F, 2.0F, 0.0F, 0.0F, 0.0F, 0.3491F));

		return LayerDefinition.create(meshdefinition, 16, 16);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		droneGround.render(poseStack, buffer, packedLight, packedOverlay);
	}
}