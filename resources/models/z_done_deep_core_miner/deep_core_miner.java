// Made with Blockbench 4.0.0-beta.3
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class deep_core_miner<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "deep_core_miner"), "main");
	private final ModelPart root;

	public deep_core_miner(ModelPart root) {
		this.root = root.getChild("root");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition root = partdefinition.addOrReplaceChild("root", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition Arm = root.addOrReplaceChild("Arm", CubeListBuilder.create().texOffs(69, 39).addBox(-3.0F, -19.0F, -4.0F, 6.0F, 22.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -22.0F, 33.0F, 0.7436F, 0.0F, 0.0F));

		PartDefinition Kopf = root.addOrReplaceChild("Kopf", CubeListBuilder.create().texOffs(104, 28).addBox(-8.0F, 0.0F, -8.0F, 16.0F, 16.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -48.0F, 16.0F));

		PartDefinition Transformer = root.addOrReplaceChild("Transformer", CubeListBuilder.create().texOffs(0, 25).addBox(-8.0F, -24.0F, 8.0F, 16.0F, 24.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		PartDefinition Bodenplatte = root.addOrReplaceChild("Bodenplatte", CubeListBuilder.create().texOffs(106, 67).addBox(-9.0F, -3.0F, -24.0F, 18.0F, 3.0F, 32.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		PartDefinition Bohrloch = root.addOrReplaceChild("Bohrloch", CubeListBuilder.create().texOffs(78, 0).addBox(-8.0F, -4.0F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		PartDefinition Photonengenerator = root.addOrReplaceChild("Photonengenerator", CubeListBuilder.create().texOffs(148, 0).addBox(-5.0F, -32.0F, -5.0F, 10.0F, 8.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		PartDefinition Linse = root.addOrReplaceChild("Linse", CubeListBuilder.create().texOffs(67, 25).addBox(-4.0F, -22.0F, -4.0F, 8.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		PartDefinition Fokusmechanik = root.addOrReplaceChild("Fokusmechanik", CubeListBuilder.create().texOffs(172, 28).addBox(-3.0F, -18.0F, -3.0F, 6.0F, 16.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -20.0F, 16.0F));

		PartDefinition Sicherheitsgitter = root.addOrReplaceChild("Sicherheitsgitter", CubeListBuilder.create().texOffs(86, 106).addBox(-8.0F, -47.0F, -24.0F, 16.0F, 44.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		PartDefinition Verbindung4 = root.addOrReplaceChild("Verbindung4", CubeListBuilder.create().texOffs(65, 73).addBox(7.0F, -47.0F, 14.0F, 2.0F, 44.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 16.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition Verbindung3 = root.addOrReplaceChild("Verbindung3", CubeListBuilder.create().texOffs(65, 73).addBox(7.0F, -47.0F, -18.0F, 2.0F, 44.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		PartDefinition Verbindung2 = root.addOrReplaceChild("Verbindung2", CubeListBuilder.create().texOffs(65, 73).addBox(7.0F, -47.0F, -1.0F, 2.0F, 44.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 16.0F, 0.0F, -3.1416F, 0.0F));

		PartDefinition Verbindung1 = root.addOrReplaceChild("Verbindung1", CubeListBuilder.create().texOffs(65, 73).addBox(7.0F, -47.0F, -2.0F, 2.0F, 44.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		PartDefinition Lagereinheit3 = root.addOrReplaceChild("Lagereinheit3", CubeListBuilder.create().texOffs(0, 67).addBox(-7.0F, -48.0F, -23.0F, 14.0F, 14.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		PartDefinition Lagereinheit2 = root.addOrReplaceChild("Lagereinheit2", CubeListBuilder.create().texOffs(0, 67).addBox(-7.0F, -33.0F, -23.0F, 14.0F, 14.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		PartDefinition Lagereinheit1 = root.addOrReplaceChild("Lagereinheit1", CubeListBuilder.create().texOffs(0, 67).addBox(-7.0F, -18.0F, -23.0F, 14.0F, 14.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 16.0F));

		return LayerDefinition.create(meshdefinition, 256, 256);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		root.render(poseStack, buffer, packedLight, packedOverlay);
	}
}