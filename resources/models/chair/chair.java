// Made with Blockbench 4.0.0-beta.3
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class chair<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "chair"), "main");
	private final ModelPart chair;

	public chair(ModelPart root) {
		this.chair = root.getChild("chair");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition chair = partdefinition.addOrReplaceChild("chair", CubeListBuilder.create().texOffs(0, 0).addBox(-7.0F, -3.0F, -7.0F, 14.0F, 3.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition rotating = chair.addOrReplaceChild("rotating", CubeListBuilder.create().texOffs(0, 31).addBox(-8.0F, -32.0F, -8.0F, 16.0F, 3.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(0, 52).addBox(-8.0F, -46.0F, 6.0F, 16.0F, 14.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(68, 2).addBox(-12.0F, -37.0F, -5.0F, 4.0F, 2.0F, 13.0F, new CubeDeformation(0.0F))
		.texOffs(68, 19).addBox(8.0F, -37.0F, -5.0F, 4.0F, 2.0F, 13.0F, new CubeDeformation(0.0F))
		.texOffs(0, 21).addBox(-3.0F, -29.0F, -3.0F, 6.0F, 2.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		chair.render(poseStack, buffer, packedLight, packedOverlay);
	}
}