package futurepack.extensions.minetweaker.implementation;

import java.util.Arrays;

import crafttweaker.CraftTweakerAPI;
import crafttweaker.IAction;
import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import crafttweaker.api.oredict.IOreDictEntry;
import futurepack.api.ItemPredicates;
import futurepack.common.recipes.centrifuge.FPZentrifugeManager;
import futurepack.common.recipes.centrifuge.ZentrifugeRecipe;
import futurepack.extensions.jei.RecipeRuntimeEditor;
import futurepack.extensions.minetweaker.ClassRegistry;
import net.minecraft.item.ItemStack;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenRegister
@ZenClass("mods.futurepack.zentrifuge")
public class ZentrifugeBridge
{

	private static void addRecipe(IItemStack[] out, IIngredient in, int support)
	{
		ItemPredicates predicate = ClassRegistry.getPredicate(in);
		if (predicate == null)
		{
			CraftTweakerAPI.logError("Invalid items for centrifuge: " + Arrays.toString(in.getItemArray()));
			return;
		}

		ItemStack[] isO = new ItemStack[out.length];
		for(int i=0;i<isO.length;i++)
		{
			isO[i] = ClassRegistry.getItem(out[i]);
		}

		ClassRegistry.addRecipe(new Add(isO, predicate, support));
	}

	@ZenMethod
	public static void add(IItemStack[] out, IIngredient ins, int support)
	{
		addRecipe(out, ins, support);
	}

	private static class Add implements IAction
	{

		private final ItemStack[] output;
		private final ItemPredicates input;
		private final int support;

		public Add(ItemStack[] output, ItemPredicates input, int support)
		{
			this.output = output;
			this.input = input;
			this.support = support;
		}

		@Override
		public void apply()
		{
			ZentrifugeRecipe recipe = FPZentrifugeManager.addRecipe(input, output, support);
			RecipeRuntimeEditor.addRecipe(recipe);
		}

		@Override
		public String describe()
		{
			return "Adding Zentrifuge Recipe for " + Arrays.toString(output);
		}
	}

	@ZenMethod
	public static void remove(IIngredient iin)
	{
		ItemStack in;
		if (iin instanceof IItemStack)
		{
			in = ClassRegistry.getItem((IItemStack) iin);
		}
		else if (iin instanceof IOreDictEntry)
		{
			in = ClassRegistry.getItem(((IOreDictEntry) iin).getFirstItem());
		}
		else return;

		ClassRegistry.removeRecipe(new Remove(in));
	}

	private static class Remove implements IAction
	{

		private final ItemStack input;

		public Remove(ItemStack input)
		{
			this.input = input;
		}

		@Override
		public void apply()
		{
			ZentrifugeRecipe recipe = FPZentrifugeManager.instance.getMatchingRecipe(input, false);
			if(recipe != null)
			{
				if(FPZentrifugeManager.instance.recipes.remove(recipe))
				{
					RecipeRuntimeEditor.removeRecipe(recipe);
				}
			}
			else
			{
				ClassRegistry.onRecipeRemoveFail(input);
			}
		}

		@Override
		public String describe()
		{
			return String.format("Removing Zentrifuge Recipe with %s as input", input.toString());
		}
	}
}